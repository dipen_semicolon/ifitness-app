/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import { StatusBar } from 'react-native';
import Setup from "./src/boot/setup";
import BackgroundImage from './src/screens/Default/BackgroundImage';



export default class App extends Component {
  componentDidMount() {
    StatusBar.setHidden(false);
    StatusBar.setBarStyle("light-content", false);
 }
  render() {
    return <BackgroundImage><Setup /></BackgroundImage>;
    }
}
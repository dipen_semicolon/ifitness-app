import React from "react";
import { Root } from "native-base";
import { createStackNavigator, DrawerNavigator, TabNavigator } from "react-navigation";
import { SafeAreaView } from "react-native";
import constant from "./screens/Constants";

import splash from "./screens/home/splash";
import home from "./screens/home";
import Tabbar from "./screens/Tabbar/Tabbar";
import inapp from "./screens/inapp";

import BackgroundImage from "./screens/Default/BackgroundImage";
import HandleBack from "./screens/Default/HandleBack";

import Toastt from "./screens/Default/Toastt";
import APICall from './WebService/APICall';


global.BackgroundImage = BackgroundImage
global.HandleBack = HandleBack
global.Toastt = Toastt
global.APICall = APICall

const AppNavigator = createStackNavigator(
  {
    splash: {
      screen: splash,
      navigationOptions: () => ({
        header: null,
      })
    },
    Home: {
      screen: home,
      navigationOptions: () => ({
        header: null,
      })
    },
    inapp: {
      screen: inapp,
      navigationOptions: () => ({
        header: null,
      })
    },
    TabbarWithoutAnimation: {
      screen: Tabbar, navigationOptions: () => ({
        header: null
      })
    },
  },
  {
    initialRouteName: "splash",
    headerMode: 'screen',
    navigationOptions: {
      gesturesEnabled: false,
    },
    transitionConfig: (sceneProps) => ({
      transitionSpec: {
       duration: (sceneProps.scene.route.routeName == "TabbarWithoutAnimation" || sceneProps.scene.route.routeName == "Home") ? 0 : 260,
      },
    }),
    
  }
);

export default () =>
  <Root>
    <AppNavigator />
  </Root>;

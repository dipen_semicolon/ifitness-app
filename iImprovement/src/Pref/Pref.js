import { AsyncStorage, Alert } from "react-native"

export const kUserName = 'user_name';
export const kUserEmail = 'user_email';
export const kUserisLogin = 'UserisLogin';
export const kUserInfo = 'Userinfo';
export const kaccessToken = 'kaccessToken';
export const kUserId = 'kUserId';
export const kdeviceToken = 'kdeviceToken';
export const kTourVisit = 'kTourVisit';
export const kNewMessage = 'kNewMessage';
export const kIsInAppShow = 'kIsInAppShow';

export const setData = async (strKey, item) => {
    let value = JSON.stringify(item)
    if (value) {
        AsyncStorage.setItem(strKey, value);
    }
}

export const getData = (strKey, callback = (response1) => { }) => {
    AsyncStorage.getItem(strKey).then((value) => {
        callback(JSON.parse(value))
    });
}


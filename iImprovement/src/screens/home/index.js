import { createStackNavigator, TabNavigator } from 'react-navigation';
import React, { Component } from 'react';
import { Image, ImageBackground } from "react-native";

import Home from './home'
import Register from '../Registration'
import subRegistration from '../Registration/subRegistration'

import Tabbar from '../Tabbar/Tabbar'
import Login from '../Login'
import ForgotPassword from '../ForgotPassword'
import contentPage from '../contentPage'
import OTP from '../OTP'


import {
  Container, Header, Left, Body, Right, Button, Icon, Title, Text, View
} from "native-base";


const RootStack = createStackNavigator(
  {
    
    Home: {
      screen: Home, navigationOptions: () => ({
        header: null
      })
    },
    Login: {
      screen: Login, navigationOptions: () => ({
      })
    },
    Register: {
      screen: Register, navigationOptions: () => ({
      })
    },
    subRegistration: {
      screen: subRegistration, navigationOptions: () => ({
      })
    },
    OTP: {
      screen: OTP, navigationOptions: () => ({
      })
    },
    ForgotPassword: {
      screen: ForgotPassword, navigationOptions: () => ({
      })
    },
    Tabbar: {
      screen: Tabbar, navigationOptions: () => ({
        header: null
      })
    },
    TabbarWithoutAnimation: {
      screen: Tabbar, navigationOptions: () => ({
        header: null
      })
    },
    contentPage: {
      screen: contentPage, navigationOptions: () => ({
        header: null
      })
    },
    // Tabbar: { screen: Tabbar, navigationOptions: () => ({
    //   headerTitleStyle: { color: '#fff' },
    //   header: null,
    //   headerStyle: {
    //     backgroundColor: 'transparent'
    //   },
    //   gesturesEnabled: false,
    // }) }
  },
  {
    initialRouteName: 'Home',
    headerMode: 'screen',
    orientation: 'portrait',
    drawUnderNavBar: true,
    navBarTranslucent: true,
    navBarTransparent: true,
    navBarBackgroundColor: 'transparent',
    navigationOptions: {
      gesturesEnabled: false,
    },
    transitionConfig: (sceneProps) => ({
      transitionSpec: {
        duration: (sceneProps.scene.route.routeName == "TabbarWithoutAnimation") ? 0 : 260,
      },
    }),
    headerStyle: {
      borderBottomWidth: 0,
  }
  },

);

export default RootStack;

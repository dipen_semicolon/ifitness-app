import React, { Component } from "react";
import { SafeAreaView, Image, Dimensions, BackHandler } from "react-native";
import { Container, View, Text, Content, Button } from "native-base";
import styles from "./style";
import ValidationComponent from 'react-native-form-validator';
import Toast, { DURATION } from 'react-native-easy-toast';


let deviceWidth = Dimensions.get('window').width
let deviceheight = Dimensions.get('window').height

const topIcon = require("../../../assets/images/MainBG.jpg");

class Home extends ValidationComponent {

  constructor(props) {
    super(props);
  }

  componentWillMount() {
  }

  onSignUpClick = (page) => {
    this.props.navigation.push('Register')
  }

  onSignInClick = () => {
    this.props.navigation.push('Login')
  }

  onTabbarClick = () => {
    this.props.navigation.push('Tabbar')
  }


  onBack = () => {
    Alert.alert(
      "Are you sure you want to exit?",
      [
        { text: "Cancel", onPress: () => { }, style: "cancel" },
        { text: "Ok", onPress: () => BackHandler.exitApp() },
      ],
      { cancelable: false },
    );
    return true;
  };

  render() {

    return (
      <HandleBack onBack={this.onBack}>
        <Container flex-direction={"row"} style={{ flex: 1 }} >
          <Toast ref="toast"
            style={{ backgroundColor: 'red', width: '100%' }}
            position='top'
            positionValue={20}
            fadeInDuration={750}
            fadeOutDuration={1000}
            opacity={0.8}
            textStyle={{ color: 'white' }}
          />
          <BackgroundImage >
            <SafeAreaView />
            <View style={{ alignItems: 'center', justifyContent: 'center', flex: 1 }}>
              <Image source={topIcon} style={{ resizeMode: 'contain', width: deviceWidth, height: deviceheight }} />
            </View>
            <View style={{ flexDirection: 'row' }}>
              <Button style={[styles.bottomViewButton, { backgroundColor: '#091A2E', borderRadius: 0, flex: 0.5 }]}
                onPress={() => this.onSignInClick()}
              >
                <Text style={{ color: 'white' }}>{'LogIn'.toUpperCase()}</Text>
              </Button>
              <Button style={[styles.bottomViewButton, { backgroundColor: '#0D3058', borderRadius: 0, flex: 0.5 }]}
                onPress={() => this.onSignUpClick()}
              >
                <Text style={{ color: 'white' }}>{'Join Now'.toUpperCase()}</Text>
              </Button>
            </View>
          </BackgroundImage>
        </Container>
      </HandleBack>

    );
  }
}

export default Home;

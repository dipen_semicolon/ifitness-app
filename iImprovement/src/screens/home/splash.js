import React, { Component } from "react";
import { SafeAreaView, Dimensions, Image, ScrollView,Alert } from "react-native";
import { Container, Label, View, Text, Button } from "native-base";
import styles from "./style";
import ValidationComponent from 'react-native-form-validator';
import * as Pref from '../../Pref/Pref';
import SplashScreen from 'react-native-splash-screen'
import PageControl from 'react-native-page-control';
import firebase from 'react-native-firebase';
import Orientation from 'react-native-orientation';
let deviceWidth = Dimensions.get('window').width
let deviceHeight = Dimensions.get('window').height


const splash = require("../../../assets/images/topIcon.png");

class Splash extends ValidationComponent {

  constructor(props) {
    super(props);
    this.state = { page: 0 };
  }

  handlePageChange = (e) => {
    var offset = e.nativeEvent.contentOffset;
    if (offset) {
      var page = Math.round(offset.x / deviceWidth);
      if (this.state.page != page) {
        this.setState({ page: page });
      }
    }
  }
  componentWillMount() {
     SplashScreen.hide();
    console.log('componentWillMount');
    var navigation = this.props.navigation
    Pref.getData(Pref.kUserId, (value) => {
      if (value >= 1) {
        userid = value;
        Pref.getData(Pref.kUserInfo, (userinfo) => {
          var obj = userinfo;
          token = obj.token
          is_premium = obj.is_premium
          navigation.push('TabbarWithoutAnimation')
          //call myinfo ws in background here.
          this.wsUserInfocall()
        })
      } else {
        //navigation.push('Home')
        Pref.getData(Pref.kTourVisit, (value) => {
          if (value == 1) {
            navigation.push('Home')
          }
        })
      }
    });
  }

  onGetStartedClick = (page) => {
    // this.refs.scrollView.scrollTo({ x: page * deviceWidth, y: 0, animated: true });
    Pref.setData(Pref.kTourVisit, 1)
    this.props.navigation.navigate('Home')
  }


  async componentDidMount() {
    // SplashScreen.hide();
    this.checkPermission();
    Orientation.lockToPortrait()
  }

  //1
  async checkPermission() {
    const enabled = await firebase.messaging().hasPermission();
    if (enabled) {
      this.getToken();
      this.createNotificationListeners(); //add this line
    } else {
      this.requestPermission();
    }
  }

  //3
  async getToken() {
    Pref.getData(Pref.kdeviceToken, (value) => {
      console.log('value push', value);
      if (value == null) {
        firebase.messaging().getToken().then(token => {
          deviceToken = token
          Pref.setData(Pref.kdeviceToken, token)
        })
      }else{
        deviceToken = value
      }
    })

  }

  //2
  async requestPermission() {
    try {
      await firebase.messaging().requestPermission();
      // User has authorised
      this.getToken();
    } catch (error) {
      // User has rejected permissions
      console.log('permission rejected');
    }
  }


  wsUserInfocall = async () => {
    let httpMethod = 'POST'
    let strURL = userinfo
    let headers = {
      'Content-Type': 'application/json',
      'token': token,
      'userid': userid,
    }
    let params = {
    }

    APICall.callWebServiceBackground(httpMethod, strURL, headers, params, (resJSON) => {
      console.log('wsFavcall Screen -> ');
      console.log(resJSON)
      if (resJSON.status == 200) {
        let data = resJSON.data
        is_premium = data.is_premium
        console.log('userinfocall',data)
        Pref.setData(Pref.kUserInfo, data)
        return
      } else {
        this.refs.toast.show(resJSON.message);
      }

    })
  }


   createNotificationListeners = async () => {
     console.log('createNotificationListeners');
  //   /*
  //   * Triggered when a particular notification has been received in foreground
  //   * */
    // this.notificationListener = firebase.notifications().onNotification((notification) => {
    //   console.log('onNotification');
    //   const { title, body } = notification;
    //   this.showAlert(title, body);
    // });

 

  //   /*
  //   * If your app is in background, you can listen for when a notification is clicked / tapped / opened as follows:
  //   * */
    // this.notificationOpenedListener = firebase.notifications().onNotificationOpened((notificationOpen) => {
    //   console.log('onNotificationOpened');
    //   const { title, body } = notificationOpen.notification;
    //   this.showAlert(title, body);
    // });

  //   /*
  //   * If your app is closed, you can check if it was opened by a notification being clicked / tapped / opened as follows:
  //   * */
  //  firebase.notifications().getInitialNotification().then(notificationOpen => {
  //   console.log('getInitialNotification');

  //     const { title, body } = notificationOpen.notification;
  //     this.showAlert(title, body);
  //   })
    /*
    * Triggered for data only payload in foreground
    * */
    // this.messageListener = firebase.messaging().onMessage((message) => {
    //   //process data message
    //   console.log(JSON.stringify(message));
    //   const { title, body } = message.data;
    //   this.showAlert(title, body);
    //   this.props.navigation.setParams({ newConversations: true })
    // });
  }

  showAlert(title, body) {
    console.log('alert call')
    Alert.alert(
      title, body,
      [
        { text: 'OK', onPress: () => console.log('OK Pressed') },
      ],
      { cancelable: false },
    );
  }
  //Remove listeners allocated in createNotificationListeners()
  componentWillUnmount() {
    this.notificationListener();
    this.notificationOpenedListener();
  }

  render() {
    return (
      <Container flex-direction={"row"} style={{ flex: 1 }} >

        <BackgroundImage >
          <SafeAreaView />
          <ScrollView ref="scrollView" horizontal={true} pagingEnabled={true} bounces={false} showsHorizontalScrollIndicator={false} scrollEventThrottle={160} onMomentumScrollEnd={(e) => this.handlePageChange(e)}>
            <View style={{ flex: 1, width: deviceWidth }}>
              <View style={{ marginTop: 44, alignItems: 'center', justifyContent: 'center' }} flexDirection={"row"}>
                <Image source={splash} style={styles.appicon}></Image>
              </View>
            </View>
            <View style={{ flex: 1, width: deviceWidth }}>
              <View style={{ marginTop: 44, alignItems: 'center', justifyContent: 'center' }} flexDirection={"row"}>
                <Image source={splash} style={styles.appicon}></Image>
              </View>
              <View style={{ flex: 1, padding: 60, justifyContent: 'flex-end', marginBottom: 0, }}>
                <Button light style={styles.bottomViewButton} onPress={() => this.onGetStartedClick()}>
                  <Text style={styles.txtBtnTitle}> GET STARTED </Text>
                </Button>
              </View>
            </View>
          </ScrollView>
          <PageControl
            style={{ position: 'absolute', left: 0, right: 0, bottom: 25 }}
            numberOfPages={2}
            currentPage={this.state.page}
            hidesForSinglePage
            pageIndicatorTintColor='#69D4D3'
            currentPageIndicatorTintColor='#FFFFFF'
            indicatorStyle={{ borderRadius: 3 }}
            currentIndicatorStyle={{ borderRadius: 3 }}
            indicatorSize={{ width: 6, height: 6 }}
            onPageIndicatorPress={this.onItemTap}
          />
        </BackgroundImage>
      </Container>
    );
  }
}

export default Splash;

const React = require("react-native");
const { Dimensions, Platform } = React;
const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;

export default {

  welcome: {
    fontSize: 34.5,
    color: 'white',
    fontFamily: font_SFProDisplay_Bold,
    marginTop:83,
  },
  appName: {
    fontSize: 34.5,
    color: '#ff9b00',
    fontFamily: font_SFProDisplay_Bold,
  },
  bottomView: {
    paddingTop: 0,
    backgroundColor: 'transparent',
  },

  bottomViewButton: {
    height: 50,
    width: '100%',
    justifyContent: "center",
    alignSelf: "stretch",
    textAlignVertical: "center",
    fontFamily: font_SFProDisplay_Medium,
    fontSize: 20,
    },

  textInput: {
    color: '#95959a',
    height:40,
    fontSize: 20,
    fontFamily: font_SFProDisplay_Regular,
    paddingLeft:10,
    borderWidth:1,
    borderColor:'#95959a',
  },

};

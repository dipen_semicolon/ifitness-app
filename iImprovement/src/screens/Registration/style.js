const React = require("react-native");
const { Dimensions, Platform } = React;
const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;

export default {
  headerAndroidnav: {
    backgroundColor: '#2265B3',
    shadowOpacity: 0,
    borderBottomWidth: 0,
  },
  headerTitle: {
    fontFamily: font_SFProDisplay_Bold,
    marginTop: 15,
    color: 'white',
    textAlign: 'center'
  },

  //message listing
  Title: {
    fontFamily: font_SFProDisplay_Bold,
    fontSize: 13,
    alignSelf: 'flex-start',
    flex: 1,
    color: '#1A1918'
  },
  bottomView: {
    paddingTop: 0,
    backgroundColor: 'transparent',
  },

  bottomViewButton: {
    height: 50,
    width: '100%',
    justifyContent: "center",
    alignSelf: "stretch",
    textAlignVertical: "center",
    fontFamily: font_SFProDisplay_Medium,
    fontSize: 20,
  },

  imgView: {
    width: deviceWidth * 0.26,
    height: deviceWidth * 0.26,
    borderRadius: deviceWidth * 0.13,
  },

  textInputView: {
    color: '#95959a',
    height: 45,
    borderWidth: 1,
    borderColor: '#95959a',
  },

  textInput: {
    color: 'white',
    fontSize: 16,
    fontFamily: font_SFProDisplay_Regular,
  },
  textOR: {
    color: 'white',
    fontSize: 20,
    fontFamily: font_SFProDisplay_Regular,
  },
  fotertext: {
    color: 'white',
    fontSize: 18,
    fontFamily: font_SFProDisplay_Regular,
  },
};

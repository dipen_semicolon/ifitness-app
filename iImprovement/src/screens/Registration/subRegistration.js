import React, { Component } from "react";
import { SafeAreaView, TextInput, Image, Dimensions, StatusBar, Platform, Alert, TouchableOpacity } from "react-native";
import { Container, View, Text, Content, Button, Header, Left, Right } from "native-base";
import styles from "./style";
import ValidationComponent from 'react-native-form-validator';
import * as Pref from '../../Pref/Pref';
import ImagePicker from 'react-native-image-picker';


let deviceWidth = Dimensions.get('window').width
const email = require("../../../assets/images/email.png");
const lock = require("../../../assets/images/lock.png");
const photo = require("../../../assets/images/photo.png");
const backWhite = require("../../../assets/images/back.png");



class Registration extends ValidationComponent {
  constructor(props) {
    super(props);
    this.state = { name: "", email: "", photo: "", loading: true, selectedImageURI: "", fbid: "", refercode: "" };
  }

  static navigationOptions = ({ navigation }) => {
    theNavigation = navigation;

    return {
      header: (
        <Header style={styles.headerAndroidnav}>
          <StatusBar barStyle="light-content" backgroundColor="#051f3a" />

          <Left style={{ alignSelf: 'flex-start', flex: 0.1 }} >
            <Button transparent onPress={() => navigation.goBack()} style={{ alignSelf: 'baseline', width: 44, height: 44 }} >
              <Image source={backWhite} style={{ width: 22, height: 22, tintColor: "white" }} />
            </Button>
          </Left>
          <View style={{ alignSelf: 'flex-start', flex: 1 }}>
            <Text style={styles.headerTitle} >{'Register'.toUpperCase()}</Text>
          </View>
          <Right style={{ alignSelf: 'flex-start', flex: 0.1 }}>
            {/* <Button transparent style={{ alignSelf: 'baseline', width: 44, height: 44 }} ><Image source={search} style={{ width: 22, height: 22, tintColor: 'black' }} /></Button> */}
          </Right>
        </Header>
      )
    }
  }

  componentWillMount() {
  }

  componentDidMount() {
    this.setState({
      email: this.props.navigation.getParam('email') ? this.props.navigation.getParam('email') : '',
      fbid: this.props.navigation.getParam('facebook_id'),
      name: this.props.navigation.getParam('name') ? this.props.navigation.getParam('name') : '',
      photo: this.props.navigation.getParam('photo') ? this.props.navigation.getParam('photo') : '',
      code: this.props.navigation.getParam('code') ? this.props.navigation.getParam('code') : '',
    })
  }

  onSignInClick = () => {
    if (this.validtion()) {
      this.verifyFbIdws(this.state.fbid, this.state.email, this.state.name, this.state.photo); //2 for student
    }
  }




  verifyFbIdws = async (facebook_id, email, name, photo, code) => {
    let httpMethod = 'POST'
    let strURL = checkfb
    let headers = {
      'Content-Type': 'application/json',
      'device_token': deviceToken,
      'device_type': Platform.OS === 'ios' ? 1 : 2,
      'build_version': '1.0'
    }
    let params = {
      'fbid': facebook_id,
      'name': name,
      'email': email,
      'photo': photo,
      'code': code
    }

    APICall.callWebService(httpMethod, strURL, headers, params, (resJSON) => {
      console.log('Login Screen -> ');
      console.log(resJSON)
      if (resJSON.status == 200) {
        let data = resJSON.data
        console.log(data);
        Pref.setData(Pref.kUserInfo, data)
        Pref.setData(Pref.kUserId, data.id)
        Pref.setData(Pref.kaccessToken, data.token)
        userid = data.id;
        token = data.token;
        this.props.navigation.push('Tabbar')
        return
      } else {
        this.refs.toast.show(resJSON.message);
      }
    })
  }

  validtion = () => {
    this.validate({
      email: { email: true },
    });
    console.log(this.state);
    if (this.isFieldInError('email')) {
      this.refs.toast.show(this.getErrorsInField('email'), 500, () => {
      });
      return false
    }
    return true
  }




  onClickSelectPhoto = () => {
    var options = {
      title: 'Select Avatar',
      allowsEditing: true,
      storageOptions: {
        skipBackup: true,
        path: 'images'
      }
    };

    ImagePicker.showImagePicker(options, (response) => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled image picker');
      }
      else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      }
      else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      }
      else {
        let source = { uri: response.uri };
        console.log('Response = ', source);
        this.setState({
          selectedImageURI: response.uri
        })
      }
    });
  }


  render() {
    console.log(this.state)
    return (
      <Container flex-direction={"row"} style={{ flex: 1 }} >
        <Toastt ref="toast"></Toastt>
        <BackgroundImage >
          <SafeAreaView />
          <Content style={{ paddingLeft: 33, paddingRight: 33 }}>
            <View style={{ marginTop: 25 }}>
              <Text style={[styles.fotertext, { textAlign: 'center', fontSize: 20 }]}>
                we are not able to get your below data from facebook.
              </Text>
            </View>
            {/* <View style={{ alignItems: 'center', justifyContent: 'center', flex: 1, marginTop: 25 }}>
              <Button style={{ alignItems: 'center', justifyContent: 'center', width: '100%', height: 100 }} transparent onPress={() => this.onClickSelectPhoto()}>
                {
                  this.state.selectedImageURI == '' ?
                    <Image source={photo}
                      style={styles.imgView} />
                    :
                    <Image
                      source={{ uri: this.state.selectedImageURI }}
                      style={styles.imgView}
                    />
                }
              </Button>
            </View> */}
            <View style={{ flex: 0.5 }}>
              <View style={[styles.textInputView, { marginTop: 15, flexDirection: 'row', alignItems: 'center' }]}>
                <Image source={email} style={{ height: 22, width: 22, marginLeft: 15 }} />
                <TextInput
                  ref="email"
                  onChangeText={(email) => this.setState({ email })} value={this.state.email}
                  style={[styles.textInput, { marginLeft: 10, width: '80%', }]}
                  placeholder="Email Address"
                  placeholderTextColor="#95959a"
                  keyboardType="email-address"
                  autoCorrect={false}
                />
              </View>

              <View style={{ marginTop: 5 }}>
                <Button style={[styles.bottomViewButton, { backgroundColor: '#0F66C7', marginTop: 5, borderRadius: 0, }]}
                  onPress={() => this.onSignInClick()}
                >
                  <Text style={{ color: 'white' }}>Join Now</Text>
                </Button>
              </View>

            </View>
          </Content>
        </BackgroundImage>

      </Container>
    );
  }
}

export default Registration;

import React, { Component } from "react";
import { SafeAreaView, TextInput, Image, Dimensions, StatusBar, Platform, Alert, TouchableOpacity } from "react-native";
import { Container, View, Text, Content, Button, Header, Left, Right } from "native-base";
import styles from "./style";
import ValidationComponent from 'react-native-form-validator';
import * as Pref from '../../Pref/Pref';
import ImagePicker from 'react-native-image-picker';
import Modal from "react-native-modal";


let deviceWidth = Dimensions.get('window').width
const user_icon = require("../../../assets/images/user-icon.png");
const email = require("../../../assets/images/email.png");
const lock = require("../../../assets/images/lock.png");
const photo = require("../../../assets/images/photo.png");
const backWhite = require("../../../assets/images/back.png");
const fbIcon = require("../../../assets/images/facebook.png");
const close = require("../../../assets/images/close.png");

const Checkbox_Unchecked = require("../../../assets/images/unchecked_sqaure.png");
const Checkbox_Checked = require("../../../assets/images/checked_sqaure.png");


const FBSDK = require('react-native-fbsdk');
const {
  LoginManager,
  GraphRequest,
  GraphRequestManager,
  AccessToken,
} = FBSDK;

// const infoRequest = new GraphRequest(
//   '/me',
//   null,
//   this._responseInfoCallback,
// );


class Registration extends ValidationComponent {

  constructor(props) {
    super(props);
    this.state = { name: "", email: "", password: "", confirmpassword: "", loading: true, selectedImageURI: "", fbid: "", isFacebookRegister: "", isModelVisible: false, refercode: "", validCode: "", isSelected: false };
  }

  static navigationOptions = ({ navigation }) => {
    theNavigation = navigation;

    return {
      header: (
        <Header style={styles.headerAndroidnav}>
          <StatusBar barStyle="light-content" backgroundColor="#051f3a" />

          <Left style={{ alignSelf: 'flex-start', flex: 0.1 }} >
            <Button transparent onPress={() => navigation.goBack()} style={{ alignSelf: 'baseline', width: 44, height: 44 }} >
              <Image source={backWhite} style={{ width: 22, height: 22, tintColor: "white" }} />
            </Button>
          </Left>
          <View style={{ alignSelf: 'flex-start', flex: 1 }}>
            <Text style={styles.headerTitle} >{'Register'.toUpperCase()}</Text>
          </View>
          <Right style={{ alignSelf: 'flex-start', flex: 0.1 }}>
            {/* <Button transparent style={{ alignSelf: 'baseline', width: 44, height: 44 }} ><Image source={search} style={{ width: 22, height: 22, tintColor: 'black' }} /></Button> */}
          </Right>
        </Header>
      )
    }
  }

  componentWillMount() {
  }

  componentDidMount() {
    this.setState({
      email: this.props.navigation.getParam('email') ? this.props.navigation.getParam('email') : '',
      fbid: this.props.navigation.getParam('facebook_id'),
      isFacebookRegister: this.props.navigation.getParam('isFacebookRegister'),
      name: this.props.navigation.getParam('name') ? this.props.navigation.getParam('name') : '',

    })
  }

  onSignInClick = () => {
    if (this.validtion()) {
      this.wscall(); //2 for student
    }

  }


  FbLoginButton = () => {
    LoginManager
      .logInWithReadPermissions(['public_profile', 'email'])
      .then(function (result) {
        if (result.isCancelled) {
          console.log('Login cancelled');
        } else {
          AccessToken
            .getCurrentAccessToken()
            .then((data) => {
              let accessToken = data.accessToken
              console.log(accessToken.toString())

              const responseInfoCallback = (error, result) => {
                if (error) {
                  console.log(error)
                } else {
                  console.log(result)
                  checkfbFunc(result.id, result.email, result.name, result.picture.data.url)
                }
              }

              const infoRequest = new GraphRequest(
                '/me?fields=email,name,picture',
                null,
                responseInfoCallback,
              );
              new GraphRequestManager().addRequest(infoRequest).start();

              // const infoRequest = new GraphRequest('/me', {
              //   accessToken: accessToken,
              //   parameters: {
              //     fields: {
              //       string: 'email,name,picture'
              //     }
              //   }
              // }, responseInfoCallback);

              // // Start the graph request.
              // new GraphRequestManager()
              //   .addRequest(infoRequest)
              //   .start()

            })
        }
      }, function (error) {
        console.log('Login fail with error: ' + error);
      });

    checkfbFunc = (facebook_id, email, name, photo) => {
      if (email.length >= 1) {
        this.verifyFbIdws(facebook_id, email, name, photo)
      } else {
        this.props.navigation.push("subRegistration", {
          'facebook_id': facebook_id,
          'email': email,
          'name': name,
          'photo': photo,
          'code': this.state.validCode
        })
      }
    }
  }

  verifyFbIdws = async (facebook_id, email, name, photo) => {
    let httpMethod = 'POST'
    let strURL = checkfb
    let headers = {
      'Content-Type': 'application/json',
      'device_token': deviceToken,
      'device_type': Platform.OS === 'ios' ? 1 : 2,
      'build_version': '1.0'
    }
    let params = {
      'fbid': facebook_id,
      'name': name,
      'email': email,
      'photo': photo,
    }

    APICall.callWebService(httpMethod, strURL, headers, params, (resJSON) => {
      console.log('Login Screen -> ');
      console.log(resJSON)
      if (resJSON.status == 200) {
        let data = resJSON.data
        console.log(data);
        Pref.setData(Pref.kUserInfo, data)
        Pref.setData(Pref.kUserId, data.id)
        Pref.setData(Pref.kaccessToken, data.token)
        userid = data.id;
        token = data.token;
        this.props.navigation.push('Tabbar')
        return
      } else {
        this.refs.toast.show(resJSON.message);
      }
    })
  }

  onPrivacyClick = () => {
    this.props.navigation.push('contentPage', {
      id: '4'
    });
  }

  onTermsAndConditionsClick = () => {
    this.props.navigation.push('contentPage', {
      id: '5'
    });
  }

  validtion = () => {
    this.validate({
      name: { required: true },
      password: { minlength: 5, required: true },
      confirmpassword: { minlength: 5, required: true },
      email: { email: true },

    });
    console.log(this.state);
    if (this.isFieldInError('name')) {
      this.refs.toast.show(this.getErrorsInField('name'), 500, () => {
      });
      return false
    } else if (this.isFieldInError('email')) {
      this.refs.toast.show(this.getErrorsInField('email'), 500, () => {
      });
      return false
    } else if (this.isFieldInError('password')) {
      this.refs.toast.show(this.getErrorsInField('password'), 500, () => {
      });
      return false
    } else if (this.state.confirmpassword != this.state.password) {
      this.refs.toast.show("Confirm Password not match", 500, () => {
      });
      return false
    } else if (this.state.isSelected == false) {
      this.refs.toast.show("Select agreement.", 500, () => {
      });
      return false
    }
    return true
  }

  wscall = async () => {
    let httpMethod = 'POST'
    let strURL = userRegister
    let headers = {
      'Content-Type': 'application/json',
      'device_token': deviceToken,
      'device_type': Platform.OS === 'ios' ? 1 : 2,
      'build_version': '1.0',
    }
    let params = {
      'email': this.state.email,
      'password': this.state.password,
      'name': this.state.name,
      'fbid': '',
      'code': this.state.validCode,
    }
    console.log('call api');
    if (this.state.selectedImageURI.length != 0) {
      console.log('uploadPhoto');
      headers = {
        'Content-Type': 'multipart/form-data',
        'device_token': deviceToken,
        'device_type': Platform.OS === 'ios' ? 1 : 2,
        'build_version': '1.0',
      }
      APICall.uploadPhoto('photo', this.state.selectedImageURI, 'image/jpg', 'userPhoto', strURL, headers, params, (resJSON) => {
        console.log('uploadPhoto -> ');
        console.log(resJSON)
        if (resJSON.status == 200) {
          let data = resJSON.data
          console.log(data);
          // Pref.setData(Pref.kUserInfo, data)
          // Pref.setData(Pref.kUserId, data.id)
          // Pref.setData(Pref.kaccessToken, data.token)
          this.props.navigation.push('OTP', {
            data: data,
          })
          return
        } else {
          this.refs.toast.show(resJSON.message);
        }
      })
    } else {
      console.log('callWebService');
      APICall.callWebService(httpMethod, strURL, headers, params, (resJSON) => {
        console.log(resJSON)
        if (resJSON.status == 200) {
          let data = resJSON.data
          console.log(data);
          // Pref.setData(Pref.kUserInfo, data)
          // Pref.setData(Pref.kUserId, data.id)
          // Pref.setData(Pref.kaccessToken, data.token)
          this.props.navigation.push('OTP', {
            data: data,
          })
          return
        } else {
          this.refs.toast.show(resJSON.message);
        }

      })
    }

  }


  wsCodeCheckcall = async () => {
    let httpMethod = 'POST'
    let strURL = checkCode
    let headers = {
      'Content-Type': 'application/json',
    }
    let params = {
      'code': this.state.refercode,
    }

    console.log('wsCodeCheckcall');
    APICall.callWebService(httpMethod, strURL, headers, params, (resJSON) => {
      console.log(resJSON)
      if (resJSON.status == 200) {
        this.setState({ validCode: this.state.refercode, isModelVisible: false })
        return
      } else {
        Alert.alert(
          "Referal code", "Not valid code.",
          [
            { text: 'OK', onPress: () => console.log('OK Pressed') },
          ],
          { cancelable: false },
        );
        //this.refs.toast.show(resJSON.message);
      }
    })

  }


  onClickSelectPhoto = () => {
    var options = {
      title: 'Select Avatar',
      allowsEditing: true,
      storageOptions: {
        skipBackup: true,
        path: 'images'
      }
    };

    ImagePicker.showImagePicker(options, (response) => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled image picker');
      }
      else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      }
      else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      }
      else {
        let source = { uri: response.uri };
        console.log('Response = ', source);
        this.setState({
          selectedImageURI: response.uri
        })
      }
    });
  }

  _onPressButton = () => {
    return this.setState({ isSelected: !this.state.isSelected });
  }

  render() {

    return (
      <Container flex-direction={"row"} style={{ flex: 1 }} >
        <Toastt ref="toast"></Toastt>
        <BackgroundImage >
          <SafeAreaView />
          <Content style={{ paddingLeft: 33, paddingRight: 33 }}>
            <View style={{ alignItems: 'center', justifyContent: 'center', flex: 1, marginTop: 10 }}>
              <Button style={{ alignItems: 'center', justifyContent: 'center', width: '100%', height: 100 }} transparent onPress={() => this.onClickSelectPhoto()}>
                {
                  this.state.selectedImageURI == '' ?
                    <Image source={photo}
                      style={styles.imgView} />
                    :
                    <Image
                      source={{ uri: this.state.selectedImageURI }}
                      style={styles.imgView}
                    />
                }

              </Button>
            </View>
            <View style={{ flex: 0.5 }}>
              <View style={[styles.textInputView, { marginTop: 15, flexDirection: 'row', alignItems: 'center', marginRight: 0 }]}>
                <Image source={user_icon} style={{ height: 22, width: 22, marginLeft: 15 }} />
                <TextInput
                  ref="name"
                  onChangeText={(name) => this.setState({ name })} value={this.state.name}
                  style={[styles.textInput, { marginLeft: 10, width: '80%', }]}
                  placeholder="Full Name"
                  placeholderTextColor="#95959a"
                  autoCorrect={false}
                />
              </View>
              <View style={[styles.textInputView, { marginTop: 15, flexDirection: 'row', alignItems: 'center' }]}>
                <Image source={email} style={{ height: 22, width: 22, marginLeft: 15 }} />
                <TextInput
                  ref="email"
                  onChangeText={(email) => this.setState({ email })} value={this.state.email}
                  style={[styles.textInput, { marginLeft: 10, width: '80%', }]}
                  placeholder="Email Address"
                  placeholderTextColor="#95959a"
                  keyboardType="email-address"
                  autoCorrect={false}
                />
              </View>
              <View style={[styles.textInputView, { marginTop: 15, flexDirection: 'row', alignItems: 'center' }]}>
                <Image source={lock} style={{ height: 22, width: 16, marginLeft: 20 }} />
                <TextInput
                  ref="password"
                  onChangeText={(password) => this.setState({ password })} value={this.state.password}
                  style={[styles.textInput, { marginLeft: 13, width: '80%', }]}
                  placeholder="Password"
                  placeholderTextColor="#95959a"
                  secureTextEntry={true}
                />
              </View>
              <View style={[styles.textInputView, { marginTop: 15, flexDirection: 'row', alignItems: 'center' }]}>
                <Image source={lock} style={{ height: 22, width: 16, marginLeft: 20 }} />
                <TextInput
                  ref="confirmpassword"
                  onChangeText={(confirmpassword) => this.setState({ confirmpassword })} value={this.state.confirmpassword}
                  style={[styles.textInput, { marginLeft: 13, width: '80%', }]}
                  placeholder="Confirm Password"
                  placeholderTextColor="#95959a"
                  secureTextEntry={true}
                />
              </View>
              <View style={{ marginTop: 5, }}>
                <Button transparent style={[styles.textInput, { height: 30, justifyContent: "center", alignSelf: "stretch", textAlignVertical: "center", }]}
                  onPress={() => { this.setState({ isModelVisible: true }) }} >
                  {
                    this.state.validCode.length >= 1 ?
                      <Text style={{ color: 'white', textAlign: 'center' }}>{this.state.validCode}</Text>
                      :
                      <Text style={{ color: 'white', textAlign: 'center' }}>Have a refer code?</Text>

                  }
                </Button>
              </View>
              <View style={{ flex: 1, flexDirection: 'row', marginTop: 10, marginBottom: 8 }}>
                <TouchableOpacity
                  onPress={() => this._onPressButton()}
                  style={{ flex: 1, flexDirection: 'row', marginRight: 5 }}>
                  <Image
                    style={{ marginLeft: 5, marginRight: 10 }}
                    source={this.state.isSelected ? Checkbox_Checked : Checkbox_Unchecked}
                  />
                </TouchableOpacity>
                <View>
                  <Text style={[styles.fotertext]}>By joining iImprove, You agree to our<Text style={[styles.fotertext, { textDecorationLine: 'underline' }]} onPress={() => this.onPrivacyClick()}> privacy policy</Text>
                    <Text style={[styles.fotertext]} > and </Text>
                    <Text style={[styles.fotertext, { textDecorationLine: 'underline' }]} onPress={() => this.onTermsAndConditionsClick()}> Terms.</Text></Text>
                </View>
              </View>
              <View style={{ marginTop: 5 }}>
                <Button style={[styles.bottomViewButton, { backgroundColor: '#0F66C7', marginTop: 5, borderRadius: 0, }]}
                  onPress={() => this.onSignInClick()}
                >
                  <Text style={{ color: 'white' }}>Join Now</Text>
                </Button>
              </View>
              <View style={{ alignItems: 'center', justifyContent: 'center', height: 40 }}>
                <Text style={[styles.textOR, { color: 'white' }]}>OR</Text>
              </View>
              <View style={{}}>
                <Button style={[styles.bottomViewButton, { backgroundColor: '#3B66C4', borderRadius: 0, }]}
                  onPress={() => this.FbLoginButton()}
                >
                  <Image source={fbIcon} style={{ marginRight: 10, marginTop: -2 }} />

                  <Text style={{ color: 'white' }}>Join with facebook</Text>
                </Button>
              </View>

            </View>
          </Content>
        </BackgroundImage>
        <Modal isVisible={this.state.isModelVisible}>
          <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', borderRadius: 10, }}>
            <View style={{ backgroundColor: 'white', width: '100%', borderRadius: 5, minHeight: 160 }}>
              <View style={{ flexDirection: 'row' }}>
                <Text style={{ color: 'black', marginLeft: 10, marginTop: 5, flex: 1 }}>Referal Code.</Text>
                <Button transparent style={[styles.textInput, { height: 30, width: 30, alignSelf: 'flex-end', margin: 5 }]}
                  onPress={() => { this.setState({ isModelVisible: false }) }}>
                  <Image source={close} style={{ tintColor: '#95959a' }} />
                </Button>
              </View>
              <View style={[styles.textInputView, { margin: 5, flexDirection: 'row', alignItems: 'center' }]}>
                <TextInput
                  ref="refercode"
                  onChangeText={(refercode) => this.setState({ refercode })} value={this.state.refercode}
                  style={[styles.textInput, { marginLeft: 10, width: '80%', color: '#051F3A' }]}
                  placeholder="Refer Code"
                  placeholderTextColor="#95959a"
                />
              </View>
              <View style={{ justifyContent: 'center', alignItems: 'center', flex: 1, marginTop: 10, marginBottom: 10 }}>
                <Button style={{ height: 50, borderRadius: 30, backgroundColor: '#2265B3', paddingLeft: 30, paddingRight: 30, alignSelf: 'center' }} onPress={() => this.wsCodeCheckcall()}><Text style={{ fontSize: 20 }}>APPLY</Text>
                </Button>
              </View>
            </View>
          </View>
        </Modal>
      </Container>
    );
  }
}

export default Registration;

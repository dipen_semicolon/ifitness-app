import React, { Component } from "react";
import { SafeAreaView, TextInput, alert, Dimensions, StatusBar, Image, WebView } from "react-native";
import { Container, View, Text, Content, Button, Header, Left, Right } from "native-base";
import styles from "./style";
import ValidationComponent from 'react-native-form-validator';
const backWhite = require("../../../assets/images/back.png");

class ContentPage extends ValidationComponent {

  constructor(props) {
    super(props);
    this.state = { description: "", loading: true, title: '', id: '4' };
  }

  componentDidMount() {
    if (theNavigation.getParam('id') == undefined) {
      this.wscall(4)
    } else {
      this.wscall(theNavigation.getParam('id'))
    }
  }

  static navigationOptions = ({ navigation }) => {
    theNavigation = navigation;
    return {
    }
  }


  wscall = async (id) => {
    let httpMethod = 'POST'
    let strURL = pages
    let headers = {
      'Content-Type': 'application/json',
    }
    let params = {
      'id': id,
    }

    APICall.callWebService(httpMethod, strURL, headers, params, (resJSON) => {
      console.log(resJSON)
      if (resJSON.status == 200) {
        let data = resJSON.data
        console.log(data);
        this.setState({ description: data.description, title: data.title })
        return
      } else {
        this.refs.toast.show(resJSON.message);
      }
    })
  }


  render() {
    if (this.state.isLoading) {
      return (<View></View>)
    } else {
      return (
        <Container flex-direction={"row"} style={{ flex: 1 }} >
          <BackgroundImage >
            <SafeAreaView />
            <Header style={styles.headerAndroidnav}>
            <StatusBar barStyle="light-content" backgroundColor="#051f3a" />
            <Left style={{ alignSelf: 'center', flex: 0.1, justifyContent:'center' }} >
              <Button transparent onPress={() => this.props.navigation.goBack()} style={{ alignSelf: 'baseline', width: 44, height: 44 }} >
                <Image source={backWhite} style={{ width: 22, height: 22, tintColor: "white" }} />
              </Button>
            </Left>
            <View style={{ alignSelf:'center', alignItems: 'center',justifyContent:'center', flex: 1 }}>
              <Text style={[styles.headerTitle, { marginTop:0}]} >{this.state.title.toUpperCase()}</Text>
            </View>
          </Header>
          
            <WebView
              source={{ html: this.state.description }}
            />
          </BackgroundImage>
        </Container>
      );
    }
  }

}

export default ContentPage;

const React = require("react-native");
const { Dimensions, Platform } = React;
const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;

export default {
  headerAndroidnav: {
    backgroundColor: '#2265b3',
    shadowOpacity: 0,
    borderBottomWidth: 0,
  },
  headerTitle: {
    fontFamily: font_SFProDisplay_Bold,
    marginTop: 15,
    color: 'white',
    textAlign: 'center'
  },

};

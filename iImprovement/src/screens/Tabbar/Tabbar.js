
import { createStackNavigator, createBottomTabNavigator, IconBadge } from 'react-navigation';
import React, { Component } from 'react';
import { Image, ImageBackground, View, Text, Alert } from "react-native";
import firebase from 'react-native-firebase';

import Favorite from './Favorite'
import Food from './Food'
import Workout from './Workout'
import Message from './Message'
import Profile from './Profile'
import * as Pref from '../../Pref/Pref';


const fav = require('../../../assets/Tabbar/fav.png')
const food = require('../../../assets/Tabbar/food.png')
const workout = require('../../../assets/Tabbar/workout.png')
const msg = require('../../../assets/Tabbar/msg.png')
const profile = require('../../../assets/Tabbar/profile.png')

var newConversations = false
var thenavigation;
Pref.getData(Pref.kNewMessage, (value) => {
  if (value) {
    if (value === '"1"') {
      console.log('check new message', value)
      newConversations = true
      navigation.setParams({ newConversations: true })
    }
  }
})


const Tabbar = createBottomTabNavigator({
  Favorite: { screen: Favorite },
  Food: {
    screen: Food, navigationOptions: () => ({
      header: null,
    })
  },
  Workout: { screen: Workout },
  Message: { screen: Message },
  Profile: { screen: Profile },

},
  {
    initialRouteName: 'Workout',
    navigationOptions: ({ navigation }) => ({
      tabBarIcon: ({ focused, tintColor }) => {
        const { routeName } = navigation.state;
        var iconName = fav;
        if (routeName === 'Favorite') {
          iconName = `${focused ? fav : fav}`;
          return <Image source={iconName} size={25} />;
        } else if (routeName === 'Food') {
          iconName = `${focused ? food : food}`;
          return <Image source={iconName} size={25} />;
        } else if (routeName === 'Workout') {
          iconName = `${focused ? workout : workout}`;
          return <Image source={iconName} size={25} />;
        } else if (routeName === 'Message') {
          iconName = `${focused ? msg : msg}`;
          return <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'center'
            }}
          >
            <Image source={iconName} size={25} />
            {newConversations
              ? <View style={{ backgroundColor: 'red', width: 10, height: 10, borderRadius: 5, position: 'absolute', top: 0, right: -5 }}></View>
              : null
            }
          </View>
        } else if (routeName === 'Profile') {
          iconName = `${focused ? profile : profile}`;
          return <Image source={iconName} size={25} />;
        }
      },
    }),
    tabBarOptions: {
      activeTintColor: 'white',
      inactiveTintColor: 'white',
      activeBackgroundColor: '#0b2139',
      gesturesEnabled: false,
      style: {
        backgroundColor: '#0b2d53',
      },
      labelStyle: {
        fontSize: 12,
        fontFamily: font_SFProDisplay_Medium,
        textAlign: 'center',
      },

    },
  },
  {
    order: ['Favorite', 'Food', 'Workout', 'Message', 'Profile'],
    animationEnabled: false,
  });


export default Tabbar;

Tabbar.navigationOptions = ({ navigation }) => {
  thenavigation = navigation

  if (navigation.state.index == 3) {
    if (newConversations) {
      newConversations = false
      Pref.setData(Pref.kNewMessage, "0")
      navigation.setParams({ newConversations: false })
    }
  }
};


this.messageListener = firebase.notifications().onNotification((notification) => {
  //process data message
  console.log(notification);
  newConversations = true
  thenavigation.setParams({ newConversations: true })
  Pref.setData(Pref.kNewMessage, "1")
  const { title, body } = notification;
  //this.showAlert(title, body);
});


this.notificationOpenedListener = firebase.notifications().onNotificationOpened((notificationOpen) => {
  console.log('onNotificationOpened');
  //process data message
  console.log(notificationOpen.notification);
  newConversations = true
  thenavigation.setParams({ newConversations: true })
  Pref.setData(Pref.kNewMessage, "1")
  const { title, body } = notificationOpen.notification;
  // this.showAlert(title, body);
});

showAlert = (title, body) => {
  console.log('alert call')
  Alert.alert(
    title, body,
    [
      { text: 'OK', onPress: () => thenavigation.navigate('Message') },
    ],
    { cancelable: false },
  );
}


global.logoutForcefully = () => {
  
  Pref.setData(Pref.kUserId, 0)
  is_premium = '0'
  thenavigation.push('Home')
}
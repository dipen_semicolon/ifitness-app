import React, { Component } from "react";
import { SafeAreaView, Image, FlatList, StatusBar, TouchableHighlight, ImageBackground, ActivityIndicator, BackHandler } from "react-native";
import { Container, View, Text, Content, Button, Header, Left, Right } from "native-base";
import styles from "./style";
import ValidationComponent from 'react-native-form-validator';
import imageCacheHoc from 'react-native-image-cache-hoc';

import Modal from "react-native-modal";
import VideoPlayer from "../Food/videoPlayer";


const topIcon = require("../../../../assets/images/topIcon.png");
const play_small = require("../../../../assets/images/play_button_small.png");
const fav = require("../../../../assets/images/fav.png");
const fav_selected = require("../../../../assets/images/fav_selected.png");
const star = require("../../../../assets/images/star.png");
const food_placeholder = require("../../../../assets/images/food_placeholder.png");

var theNavigation;
var subthis;


const CacheableImage = imageCacheHoc(ImageBackground, {
  fileHostWhitelist: ['100.25.250.56'],
  validProtocols: ['http', 'https'],
  defaultPlaceholder: {
    component: Image,
    props: {
      style: styles.activityIndicatorStyle
    }

  }
});


const CacheableReceipeImage = imageCacheHoc(ImageBackground, {
  fileHostWhitelist: ['100.25.250.56'],
  validProtocols: ['http', 'https'],
  defaultPlaceholder: {
    component: Image,
    props: {
      style: styles.placeholderIcon,
      source: food_placeholder
    }
  }
});

class favList extends ValidationComponent {

  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      page: 0,
      isFilterVisible: false,
      videourl: '',
      image: [],
    }
    subthis = this
  }

  static navigationOptions = ({ navigation }) => {
    theNavigation = navigation;

    return {
      header: (
        <Header style={[styles.headerAndroidnav, { borderBottomWidth: 1, borderBottomColor: '#092441' }]}>
          <StatusBar barStyle="light-content" backgroundColor="#051f3a" />

          <Left style={{ alignSelf: 'flex-start', flex: 0.1 }} >
          </Left>
          <View style={{ alignSelf: 'flex-start', flex: 1 }}>
            <View style={{ alignItems: 'center', justifyContent: 'center', flex: 1, marginTop: 15, marginBottom: 15 }}>
              <Image source={topIcon} style={[styles.topIcon]} />
            </View>
          </View>
          <Right style={{ alignSelf: 'flex-start', flex: 0.1 }}>
            {/* <Button transparent style={{ alignSelf: 'baseline', width: 44, height: 44 }} ><Image source={search} style={{ width: 22, height: 22, tintColor: 'black' }} /></Button> */}
          </Right>
        </Header>
      )
    }
  }


  componentDidMount() {
    this.props.navigation.addListener('willFocus', (route) => {
      this.wscall(this.state.page)
    });
  }

  onWorkcoutClick = () => {
    this.setState({ page: 0 })
    this.wscall(0)
  }
  onFoodClick = () => {
    this.setState({ page: 1 })
    this.wscall(1)
  }


  wscall = async (page) => {
    let httpMethod = 'POST'
    let strURL = page == 0 ? favVideoList : favReceipeList
    let headers = {
      'Content-Type': 'application/json',
      'token': token,
      'userid': userid,
    }
    let params = {
    }

    APICall.callWebService(httpMethod, strURL, headers, params, (resJSON) => {
      console.log('receipeList Screen -> ');
      console.log(resJSON)
      if (resJSON.status == 200) {
        if (page == 0) {
          this.setState({
            isLoading: false,
            videoData: resJSON.data,
            page: page,
          })

        } else {
          this.setState({
            isLoading: false,
            ReceipeData: resJSON.data,
            page: page,
          })

        }
        return
      } else {
        this.refs.toast.show(resJSON.message);
      }

    })
  }

  wsFavcall = async (receipe_id, status, index) => {
    let httpMethod = 'POST'
    let strURL = favReceipe
    let headers = {
      'Content-Type': 'application/json',
      'token': token,
      'userid': userid,
    }
    let params = {
      'receipe_id': receipe_id,
      'status': status,
    }

    newData = this.state.ReceipeData
    newData.splice(index, 1)
    console.log('newdata', newData)
    subthis.setState({ ReceipeData: newData })

    APICall.callWebServiceBackground(httpMethod, strURL, headers, params, (resJSON) => {
      console.log('wsFavcall Screen -> ');
      console.log(resJSON)
      if (resJSON.status == 200) {
        if (status == 0) {
          //this.refs.toast.show('Unfavorite sucessfull.');
        } else {
          //this.refs.toast.show('favorite sucessfull.');
        }
        // newData = this.state.Data
        // receipeData = newData[index]
        // receipeData.receipeInfo.fav_status = status
        // console.log('newdata', receipeData)
        // subthis.setState({ Data: newData })
        return
      } else {
        this.refs.toast.show(resJSON.message);
      }

    })
  }

  toggleFilterModal = (video, photo) => {
    // this.setState({ videourl: video, image: photo, isFilterVisible: !this.state.isFilterVisible, });
    // console.log(this.state.isFilterVisible);
  }

  toggleHideModal = (filterOptions) => {
    this.setState({ isFilterVisible: false });
    console.log('filterOptions', filterOptions);
  }


  onBack = () => {
    Alert.alert(
      "Are you sure you want to exit?",
      [
        { text: "Cancel", onPress: () => { }, style: "cancel" },
        { text: "Ok", onPress: () => BackHandler.exitApp() },
      ],
      { cancelable: false },
    );
    return true;
  };
  
  render() {
    console.log('render', this.state)
    if (this.state.isLoading) {
      return (<BackgroundImage></BackgroundImage>)
    } else {
      return (
        <HandleBack onBack={this.onBack}>
          <Container flex-direction={"row"} style={{ flex: 1, }} >
            <Toastt ref="toast"></Toastt>
            <BackgroundImage  >
              <SafeAreaView />
              <View style={[styles.topButtons, { flexDirection: 'row' }]} >
                <Button transparent style={[styles.button, this.state.page == 0 ? styles.buttonSelected : null]} onPress={() => this.onWorkcoutClick()}><Text style={styles.buttonText}>Workout</Text></Button>
                <Button transparent style={[styles.button, this.state.page == 1 ? styles.buttonSelected : null]} onPress={() => this.onFoodClick()}><Text style={styles.buttonText}>Food</Text></Button>
              </View>
              {this.setupdata()}
            </BackgroundImage>
          </Container>
        </HandleBack>
      );
    }
  }

  setupdata = () => {
    var data
    if (this.state.page == 0) {
      data = this.state.videoData
    } else {
      data = this.state.ReceipeData
    }
    if (data == undefined) {
      return (<Text></Text>)
    } else if (data.length >= 1) {
      return (<FlatList
        style={{ flex: 1 }}
        data={data}
        renderItem={
          ({ item, index }) => this.loadTableCells(item, index)
        }
        keyExtractor={(index) => index.toString()}
      />)

    } else {
      return (<View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <Image source={star} style={{ width: 25, height: 25 }} />
        <Text style={{ color: 'white', fontSize: 20, fontFamily: font_SFProDisplay_Bold, marginTop: 10 }}>{'Add Favorites'.toUpperCase()}</Text>
        <Text style={{ color: 'white', fontSize: 16, fontFamily: font_SFProDisplay_Regular, marginTop: 10, marginLeft: 20, marginRight: 20, textAlign: 'center' }}>{'Start saving your top'} {this.state.page == 0 ? 'workouts' : 'receipe'} {'for \n quick access,any time'}</Text>
      </View>)

    }

  }

  loadTableCells(item, index) {
    if (this.state.page == 0) {
      return (this.cellVideoList(item.videoInfo, index));
    } else if (this.state.page == 1) {
      return (this.cellReceipeList(item.receipeInfo, index));
    }
  }

  refresh(index) {
    console.log("refresh",index)
    newData = this.state.Data
    newData.splice(index, 1)
    console.log('newdata', newData)
    subthis.setState({ Data: newData })
  }

  cellVideoList = (facet, rowID) => {
    sectionID = 0;
    return (
      <TouchableHighlight style={{ flexDirection: 'row' }}
        onPress={() => {
          theNavigation.push('workoutDetail', {
            title: facet.title,
            data: facet,
            FromFavorite: true,
            onGoBack: (index) => subthis.refresh(index),
            index: rowID,

          })
        }}
        underlayColor="transparent"
        activeOpacity={1}
      >
        <View style={{ flexDirection: 'row', borderBottomColor: '#193654', borderBottomWidth: 2, paddingBottom: 10, flex: 1, paddingLeft: 10, paddingRight: 10, marginTop: 10, }}>
          <View style={{ justifyContent: 'center', alignItems: 'center', width: 80, height: 60 }}>
            <CacheableImage style={{ width: 80, height: 60 }} source={{ uri: facet.photo }} permanent={true} >
              <View style={{ backgroundColor: 'rgba(0,0,0,0.5)', alignItems: 'center', justifyContent: 'center', flex: 1 }}>
                <Text style={{ color: 'white', fontSize: 25, fontFamily: font_MyriadPro_Regular, textAlign: 'center', marginTop: 15, height: 22 }}>{Math.floor(facet.duration / 60)}</Text>
                <Text style={{ color: 'white', }}>min</Text>
              </View>
            </CacheableImage>
          </View>
          <View style={{ flex: 1, paddingLeft: 10, }}>
            <Text style={styles.videoTitle}>{facet.title}</Text>
            <View style={{ justifyContent: 'center', flex: 1 }}>
              <Text style={styles.videoDuration} numberOfLines={1} >{facet.level} {facet.level.length >= 1 ? '-' : null} {facet.equipment} </Text>
            </View>
            {/*<View style={{ alignItems: 'flex-start', justifyContent: 'flex-end', flex: 1 }}>
                <Text style={styles.videoDuration}>Duration: {facet.duration}</Text>
              </View> */}
          </View>
        </View>
      </TouchableHighlight>
    );
  }

  cellReceipeList = (facet, rowID) => {
    sectionID = 0;
    console.log('cellReceipeList', facet)
    return (
      <TouchableHighlight style={{ flexDirection: 'row' }}
        onPress={() => {
          theNavigation.push('FoodDetail', {
            title: facet.title,
            data: facet,
            FromFavorite: true,
            onGoBack: (index) => this.refresh(index),
            index: rowID,
          })
        }}
        underlayColor="transparent"
        activeOpacity={1}
      >
        <View style={{ flex: 1, padding: 10 }}>
          <CacheableReceipeImage source={{ uri: facet.images.length >= 1 ? facet.images[0].photo : 'http://100.25.250.56:8081/images/food_placeholder.png' }} resizeMode='cover' style={{ width: '100%', height: 180 }} permanent={true}>
            <View style={{
              paddingLeft: 10, paddingBottom: 10, flexDirection: 'row',
              justifyContent: 'flex-end',
              alignItems: 'center',
            }}>
              <Button transparent style={{ justifyContent: 'flex-end', paddingRight: 5, width: 44, height: 44 }} onPress={() => subthis.wsFavcall(facet.id, facet.fav_status == 1 ? 0 : 1, rowID)} ><Image source={facet.fav_status == 1 ? fav_selected : fav} style={{ width: 22, height: 22, tintColor: 'white' }} /></Button>
            </View>
            <View style={{ paddingLeft: 10, paddingBottom: 10, alignItems: 'flex-start', justifyContent: 'flex-end', flex: 1 }}>
              <View style={[styles.videoTitleBG]}>
                <Text style={styles.recepieTitle}>{facet.title}</Text>
              </View>
            </View>
          </CacheableReceipeImage>

        </View>
      </TouchableHighlight>

    );
  }
}
export default favList;

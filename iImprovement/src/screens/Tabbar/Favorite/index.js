import React, { Component } from 'react';
import { Image, ImageBackground } from "react-native";
import { createStackNavigator, TabNavigator } from 'react-navigation';
import FavList from './FavList'
import workoutDetail from '../Workout/workoutDetail'
import FoodDetail from '../Food/FoodDetail'
import videoPlayer from "../Food/videoPlayer";

import {
  Container, Header, Left, Body, Right, Button, Icon, Title, Text, View
} from "native-base";


const RootStack = createStackNavigator(
  {
    FavList: {
      screen: FavList, navigationOptions: () => ({
      })
    },
    workoutDetail: {
      screen: workoutDetail, navigationOptions: () => ({
      })
    },
    FoodDetail:{
      screen: FoodDetail, navigationOptions: () => ({
      })
    },
    videoPlayer: {
      screen: videoPlayer, navigationOptions: () => ({
        header:null,
      })
    },
  },
  {
    initialRouteName: 'FavList',
    headerMode: 'screen',
    orientation: 'portrait',
    navBarBackgroundColor: 'transparent',
    headerStyle: {

    },
  },

);

RootStack.navigationOptions = ({ navigation }) => {

  let tabBarVisible = true;
  if (navigation.state.index > 0) {
      tabBarVisible = false
  }
  return {
      tabBarVisible,
  };
};
export default RootStack;

const React = require("react-native");
const { Platform, Dimensions } = React;

const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;

export default {
    headerAndroidnav: {
        // ...Platform.select({
        //   ios: {
        //   },
        //   android: {
        //     height: (deviceWidth * 110) / 320,
        //     paddingTop: 5,
        //   },
        // }),
        backgroundColor: '#051F3A',
        shadowOpacity: 0,
        borderBottomWidth: 0,
        height: (deviceWidth * 100) / 320,
    },
    topIcon: {

        ...Platform.select({
            ios: {
                height: (deviceWidth * 70) / 320,
                width: (deviceWidth * 119) / 320,
            },
            android: {
                height: 88, //(deviceWidth * 42) / 320,
                width: 150
            },
        }),

        resizeMode: 'cover',

    },
    facetTitleWrapper: {
        width: '100%',
        height: 150,
        backgroundColor:'#051f3a',
    },
    facetMenuWrapper: {
        height: 50,
        borderBottomWidth:1,
        borderBottomColor:'#092545',
    },

    facetOptionContainer: {
        flex: 1,
        backgroundColor: '#051f3a',
    },

    scrollView: {
        flex: 1,
        backgroundColor: '#F0F1F2',
        marginTop: 20,
    },
    facetTitle: {
        marginLeft: 20,
        color: 'white',
        fontSize: 18,
        fontFamily: font_MyriadPro_Regular,

    },
    titleContainer:{
        backgroundColor:'#051f3a',
    },
    facetsContainer:{
        backgroundColor:'#051f3a',
    }
   
};




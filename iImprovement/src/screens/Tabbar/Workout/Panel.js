import React, { Component } from "react";
import { StyleSheet, Text, View, Image, TouchableHighlight, TouchableOpacity, Animated, ImageBackground,ActivityIndicator } from 'react-native';
import styles from "./style";
import imageCacheHoc from 'react-native-image-cache-hoc';

const resizeMode = 'center';

const CacheableImage = imageCacheHoc(ImageBackground, {
    fileHostWhitelist: ['100.25.250.56'],
    validProtocols: ['http', 'https'],
    defaultPlaceholder: {
        component: ActivityIndicator,
        props: {
          style: styles.activityIndicatorStyle
        }
      }
  });

class Panel extends Component {
    constructor(props) {
        super(props);
        this.state = {
            title: props.title,
            photo: props.photo,
            expanded: false,
            animation: new Animated.Value()
        };
        this.hideSubMenu()
    }

    hideSubMenu = () => {
        console.log('hideSubMenu');
        this.state.animation.setValue(150);
    }

    toggle() {
        let initialValue = this.state.expanded ? this.state.maxHeight + this.state.minHeight : this.state.minHeight,
            finalValue = this.state.expanded ? this.state.minHeight : this.state.maxHeight + this.state.minHeight;
        console.log(initialValue, finalValue);
        this.setState({
            expanded: !this.state.expanded
        });

        this.state.animation.setValue(initialValue);
        Animated.spring(
            this.state.animation,
            {
                toValue: finalValue
            }
        ).start();
    }

    _setMaxHeight(event) {
        this.setState({
            maxHeight: event.nativeEvent.layout.height
        });
    }

    _setMinHeight(event) {
        this.setState({
            minHeight: event.nativeEvent.layout.height
        });
    }

    render() {


        return (
            <Animated.View
                style={[styles.container, { height: this.state.animation }]}>
                <View style={styles.titleContainer} onLayout={this._setMinHeight.bind(this)}>
                    <TouchableHighlight
                        style={styles.button}
                        onPress={this.toggle.bind(this)}
                        underlayColor="transparent"
                        activeOpacity={1}
                    >
                        <View style={styles.facetsContainer}>
                            <View style={styles.facetTitleWrapper}>
                                <CacheableImage
                                    style={{
                                        backgroundColor: '#ccc',
                                        flex: 1,
                                        resizeMode,
                                        width: '100%',
                                        height: '100%',
                                        justifyContent: 'center',
                                    }}
                                    source={{ uri: this.state.photo,cache: 'only-if-cached', }} permanent={true} >
                                    <View style={{
                                        backgroundColor: 'black', opacity: .5, flex: 1, position: 'absolute', width: '100%', height: '100%',
                                    }}>
                                    </View>
                                    <View style={{ flex: 1, alignItems: 'flex-start', justifyContent: 'flex-end' }}>
                                        <Text
                                            style={{
                                                backgroundColor: 'transparent',
                                                fontSize: 20,
                                                color: 'white',
                                                paddingLeft: 10,
                                                paddingBottom: 10,

                                            }}
                                        >
                                            {this.state.title}
                                        </Text>
                                    </View>
                                    <View style={{height:5,backgroundColor:'#051f3a'}}></View>
                                </CacheableImage>
                            </View>
                        </View>
                    </TouchableHighlight>
                </View>

                <View style={[styles.body, { backgroundColor: '#051F3A', }]} onLayout={this._setMaxHeight.bind(this)}>
                    {this.props.children}
                </View>

            </Animated.View>
        );
    }
}



export default Panel;
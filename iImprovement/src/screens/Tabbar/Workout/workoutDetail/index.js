import React, { Component } from "react";
import { SafeAreaView, Image, ScrollView, TouchableHighlight, ImageBackground, Animated, StatusBar } from "react-native";
import { Container, View, Text, Content, Button, Header, Left, Right } from "native-base";
import styles from "./style";
import ValidationComponent from 'react-native-form-validator';
//import VideoPlayer from 'react-native-video-player';
import Video from 'react-native-af-video-player'
import Orientation from 'react-native-orientation';
import HTMLView from 'react-native-htmlview';


const backWhite = require("../../../../../assets/images/back.png");
const fav = require("../../../../../assets/images/fav.png");
const fav_selected = require("../../../../../assets/images/fav_selected.png");
const close = require("../../../../../assets/images/close.png");

var theNavigation;
var subthis;
class WorkoutDetail extends ValidationComponent {

    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            FromFavorite: false,
            index: -1,
            fullscreen: false

        };
        subthis = this
    }

    static navigationOptions = ({ navigation }) => {
        theNavigation = navigation;
        data = theNavigation.getParam('data')
        const { state } = navigation
        if (state.params && state.params.fullscreen) {
            return {
                header: null
            }
        } else {
            return {
                header: (
                    <Header style={styles.headerAndroidnav}>
                        <StatusBar barStyle="light-content" backgroundColor="#051f3a" />
                        <Left style={{ flex: 0.1, alignItems: 'center', justifyContent: 'center' }} >
                            <Button transparent onPress={() => {
                         navigation.goBack()
                         navigation.state.params.onGoBack()
                        }} style={{ alignSelf: 'center', width: 44, height: 44 }} >
                                <Image source={backWhite} style={{ width: 22, height: 22, tintColor: "white" }} />
                            </Button>
                        </Left>
                        <View style={{ alignSelf: 'center', justifyContent: 'center', flexDirection: 'row', flex: 1 }}>
                            {/* <Text style={styles.headerTitle} >{theNavigation.getParam('title').toUpperCase()}</Text> */}
                        </View>
                        <Right style={{ alignItems: 'center', justifyContent: 'center', flex: 0.1 }}>
                            <Button transparent style={{ alignSelf: 'baseline', width: 44, height: 44 }} onPress={() => { subthis.wscall(data.id, data.fav_status == 1 ? 0 : 1) }}><Image source={data.fav_status == 1 ? fav_selected : fav} style={{ width: 22, height: 22, tintColor: 'white' }} /></Button>
                        </Right>
                    </Header>
                )
            }
        }


    }


    componentWillUnmount() {
        Orientation.lockToPortrait();
    }


    componentDidMount() {
       
        this.setState({
            data: theNavigation.getParam('data'),
            title: theNavigation.getParam('title'),
            FromFavorite: theNavigation.getParam('FromFavorite') != undefined ? theNavigation.getParam('FromFavorite') : false,
            index: theNavigation.getParam('index') != undefined ? theNavigation.getParam('index') : -1,
            isLoading: false,
        })
        //this.wscall(theNavigation.getParam('selectedId'))
    }


    wscall = async (video_id, status) => {
        let httpMethod = 'POST'
        let strURL = favVideo
        let headers = {
            'Content-Type': 'application/json',
            'token': token,
            'userid': userid,
        }
        let params = {
            'video_id': video_id,
            'status': status,
        }
        newdata = this.state.data
        newdata.fav_status = status
        theNavigation.setParams({ data: newdata })

        APICall.callWebServiceBackground(httpMethod, strURL, headers, params, (resJSON) => {
            console.log('Favorite video Screen -> ');
            console.log(resJSON)
            if (resJSON.status == 200) {
                if (status == 0) {
                    //this.refs.toast.show('Unfavorite sucessfull.');
                } else {
                    //this.refs.toast.show('favorite sucessfull.');
                }
                // newdata = this.state.data
                // newdata.fav_status = status
                // console.log('newdata', newdata)
                // theNavigation.setParams({ data: newdata })
                if (this.state.FromFavorite) {
                    console.log(theNavigation.state.params);
                    theNavigation.goBack()
                    //theNavigation.state.params.onGoBack(this.state.index)
                }
                return
            } else {
                this.refs.toast.show(resJSON.message);
            }

        })
    }

    onFullScreen(status) {
        // Set the params to pass in fullscreen status to navigationOptions
        this.setState({ fullscreen: status })
        this.props.navigation.setParams({
            fullscreen: status
        })
        if(status){
            Orientation.unlockAllOrientations();
            Orientation.lockToLandscape();    
        }else{
            Orientation.lockToPortrait();
        }

    }

    onEnd = () => {
        console.log('onEnd call')
        this.setState({ fullscreen: false })
        this.props.navigation.setParams({
            fullscreen: false
        })
        this.video.toggleFS()
        Orientation.lockToPortrait();
    }

    render() {
        if (this.state.isLoading) {
            return (<BackgroundImage></BackgroundImage>)
        } else {
            // if (this.state.fullscreen) {
            //     return (
            //         <Container flex-direction={"row"} style={{ flex: 1, }} >
            //                 <View style={{flex:1}}>
            //                     <Video url={this.state.data.video}
            //                         onFullScreen={status => this.onFullScreen(status)}
            //                         fullScreenOnly
            //                         autoPlay
            //                         onEnd={() => { this.onEnd() }}
            //                     />
            //                 </View>
            //                 <Button transparent style={{ position:'absolute', top: 0, left: 0, width: 44, height: 44 }} onPress={() =>this.onEnd()}><Image source={close} /></Button>
            //         </Container>
            //     )
            // } else {
            return (
                <Container flex-direction={"row"} style={{ flex: 1, }} >
                    <Toastt ref="toast"></Toastt>
                    <BackgroundImage  >
                        <SafeAreaView />
                        <Content style={this.state.fullscreen == true ? {} : { paddingLeft: 15, paddingRight: 15, paddingTop: 10 }} scrollEnabled={!this.state.fullscreen}>
                            <View >
                                {/* <Image source={{ uri: this.state.data.photo }} style={{ width: '100%', height: 200 }} /> */}
                                {/* <VideoPlayer
                                        endWithThumbnail
                                        thumbnail={{ uri: this.state.data.photo }}
                                        video={{ uri: this.state.data.video }}
                                        duration={0}
                                        disableControlsAutoHide={true}
                                        ref={r => this.player = r}
                                    /> */}
                                <Video url={this.state.data.video}
                                    onFullScreen={status => this.onFullScreen(status)}
                                    ref={(ref) => { this.video = ref }}
                                />
                            </View>
                            <View style={{ paddingTop: 10 }}>
                                <Text style={[styles.videoTitle, { marginTop: 0, marginBottom: 5 }]}>{this.state.data.title}</Text>
                                <HTMLView
                                    value={this.state.data.descriptions}
                                    stylesheet={styles}
                                    addLineBreaks={false}
                                />
                            </View>
                            {
                                this.state.fullscreen == true ?
                                    <Button transparent style={{ position: 'absolute', top: 0, left: 0, width: 44, height: 44 }} onPress={() => this.onEnd()}><Image source={close} /></Button>
                                    : null
                            }
                        </Content>
                    </BackgroundImage>
                </Container>
            );
            //}

        }
    }
}
export default WorkoutDetail;

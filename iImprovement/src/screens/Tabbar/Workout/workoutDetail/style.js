const React = require("react-native");
const { Platform, Dimensions } = React;
let deviceWidth = Dimensions.get('window').width

export default {
    headerAndroidnav: {
        backgroundColor: '#2265B3',
        shadowOpacity: 0,
        borderBottomWidth: 0,
        height: (deviceWidth * 64) / 320,
    },
    headerTitle: {
        fontFamily: font_SFProDisplay_Bold,
        color: 'white',
        textAlign: 'center'
    },
    videoTitle:{
        color: 'white',
        fontSize: 20,
      },
      videoDescription:{
        color: 'white',
        fontSize: 18,
      },
      videoDuration:{
        color: 'white',
        fontSize: 16,
        marginTop:3,
      },
      p: {
        color: 'white', // make links coloured pink
      },
      li: {
        color: 'white', // make links coloured pink
        fontSize: 16,
      },
      ul: {
        color: 'white', // make links coloured pink
        fontSize: 18,
        padding: 5,
      },
      ol: {
        color: 'white', // make links coloured pink
        fontSize: 18,
        padding: 5,
      },
      headersubTitle: {
        fontFamily: font_SFProDisplay_Regular,
        color: 'white',
        fontSize: 18,
      },
};




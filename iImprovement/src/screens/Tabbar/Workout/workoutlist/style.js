const React = require("react-native");
const { Platform, Dimensions } = React;
let deviceWidth = Dimensions.get('window').width

export default {
  headerAndroidnav: {
    backgroundColor: '#2265B3',
    shadowOpacity: 0,
    borderBottomWidth: 0,
    height: (deviceWidth * 64) / 320,
  },
  headerTitle: {
    fontFamily: font_SFProDisplay_Bold,
    color: 'white',
    textAlign: 'center'
  },
  container: {
    flex: 1,
    alignItems: 'center',
    marginTop: 20,
  },
  noVideo: {
    textAlign: 'center',
    color: 'white',
    fontSize: 28,
  },
  videoTitle: {
    color: '#8CA9C7',
    fontSize: 18,
  },
  videoDescription: {
    color: '#8CA9C7',
    fontSize: 18,
  },
  videoDuration: {
    color: '#8CA9C7',
    fontSize: 16,
  },
  activityIndicatorStyle: {
    width: 80,
    height: 60,
    backgroundColor: 'transparent'
  }
};




import React, { Component } from "react";
import { SafeAreaView, Image, ScrollView, TouchableHighlight, ImageBackground, Animated, StatusBar, TouchableOpacity, ActivityIndicator, Dimensions } from "react-native";
import { Container, View, Text, Content, Button, Header, Left, Right } from "native-base";
import styles from "./style";
import ValidationComponent from 'react-native-form-validator';
import ScrollableTabView from 'react-native-scrollable-tab-view';
import TabBar from "react-native-underline-tabbar";
import imageCacheHoc from 'react-native-image-cache-hoc';

import Modal from "react-native-modal";
import FilterPopup from "../filterpopup";
import InAppPurchase from "../../../inapp"

const backWhite = require("../../../../../assets/images/back.png");
const sort = require("../../../../../assets/images/sort.png");
const play_small = require("../../../../../assets/images/play_button_small.png");
const star = require("../../../../../assets/images/video_placeholder.png");
const padlock = require("../../../../../assets/images/padlock.png");
const workout_placeholder = require("../../../../../assets/images/workout_placeholder.png");

let deviceHeight = Dimensions.get('window').height - 250


var theNavigation;
var subthis;

const CacheableImage = imageCacheHoc(ImageBackground, {
  fileHostWhitelist: ['100.25.250.56'],
  validProtocols: ['http', 'https'],
  defaultPlaceholder: {
    component: Image,
    props: {
      style: styles.activityIndicatorStyle,
      source: workout_placeholder
    }
  }
});

const Page = ({ label, data, videoLoad }) => (
  <Content>
    <View style={[styles.container]}>
      {videoLoad == true ? renderVideo(data) : null}
    </View>
  </Content>
);

const Tab = ({ tab, page, isTabActive, onPressHandler, onTabLayout, styles }) => {
  const { label, icon } = tab;
  const style = {
    borderRightWidth: (subthis.state.catData.subcategories.length - 1) == page ? 0 : 2,
    borderRightColor: '#092441',
  };
  const containerStyle = {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: styles.backgroundColor,
    opacity: styles.opacity,
    transform: [{ scale: styles.opacity }],
    paddingHorizontal: 20,
    paddingVertical: 10,
  };
  const textStyle = {
    color: styles.textColor,
    fontSize: 18,
    fontFamily: font_MyriadPro_Regular,
  };

  return (
    <TouchableOpacity style={style} onPress={onPressHandler} onLayout={onTabLayout} key={page}>
      <Animated.View style={containerStyle}>
        <Animated.Text style={textStyle}>{label}</Animated.Text>
      </Animated.View>
    </TouchableOpacity>
  );
};



const renderVideo = (videData) => {
  console.log(videData);
  if (videData != null) {
    var facetsArray = videData;
    var facetListViewArray = [];
    facetsArray = facetsArray.map((facet, i) => {
      facetListViewArray.push(
        <TouchableHighlight style={{ flexDirection: 'row' }}
          onPress={() => {
            if (facet.paid == '1' && is_premium == '0') {
              //theNavigation.push('inapp')
               subthis.setState({isIAPVisible:true}) 
            } else {
              subthis.setState({DetailScreen:true})
              theNavigation.push('workoutDetail', {
                title: facet.title,
                data: facet,
                onGoBack: () => subthis.refresh()
              })
            }
          }}
          underlayColor="transparent"
          activeOpacity={1}
        >
          <View style={{ flexDirection: 'row', borderBottomColor: '#193654', borderBottomWidth: 2, paddingBottom: 10, flex: 1, paddingLeft: 10, paddingRight: 10, paddingTop: 10 }}>
            <View style={{ justifyContent: 'center', alignItems: 'center', width: 80, height: 60 }}>
              <CacheableImage source={{ uri: facet.photo, cache: 'only-if-cached', }} style={{ width: 80, height: 60 }} permanent={true}>
                <View style={{ backgroundColor: 'rgba(0,0,0,0.5)', alignItems: 'center', justifyContent: 'center', flex: 1 }}>
                  {
                    facet.paid == '1' && is_premium == '0' ?
                      <Image source={padlock} style={{ width: 25, height: 25 }} />
                      :
                      <Text style={{ color: 'white', fontSize: 25, fontFamily: font_MyriadPro_Regular, textAlign: 'center' }}>{Math.floor(facet.duration / 60)}{'\n'}
                        <Text style={{ fontSize: 14, color: 'white' }}>min</Text></Text>
                  }
                  {/* {
                    facet.paid == '0' ?
                      <Text style={{ fontSize: 14, color: 'white', textAlign:'center' }}>min</Text>
                      : null
                  } */}

                </View>
              </CacheableImage>
            </View>
            <View style={{ flex: 1, paddingLeft: 10, }}>
              <Text style={styles.videoTitle}>{facet.title}</Text>
              <View style={{ justifyContent: 'center', flex: 1 }}>
                <Text style={styles.videoDuration} numberOfLines={1} >{facet.level} {facet.level.length >= 1 ? '-' : null} {facet.equipment} </Text>
              </View>
              {/*<View style={{ alignItems: 'flex-start', justifyContent: 'flex-end', flex: 1 }}>
                <Text style={styles.videoDuration}>Duration: {facet.duration}</Text>
              </View> */}
            </View>
          </View>
        </TouchableHighlight >
      )
    })
    if (facetListViewArray.length == 0) {
      return (<View style={{ flex: 1, height: deviceHeight, alignItems: 'center', justifyContent: 'center' }}>
        <Image source={star} style={{ width: 36, height: 36 }} />
        <Text style={{ color: 'white', fontSize: 20, fontFamily: font_SFProDisplay_Bold, marginTop: 10 }}>{'Workout Videos'.toUpperCase()}</Text>
        <Text style={{ color: 'white', fontSize: 16, fontFamily: font_SFProDisplay_Regular, marginTop: 10, marginLeft: 20, marginRight: 20, textAlign: 'center' }}> No videos for this categories.</Text>
      </View>)
    }
    return (facetListViewArray)

  }
  return (<View style={{ flex: 1, height: deviceHeight, alignItems: 'center', justifyContent: 'center' }}>
    <Image source={star} style={{ width: 36, height: 36 }} />
    <Text style={{ color: 'white', fontSize: 20, fontFamily: font_SFProDisplay_Bold, marginTop: 10 }}>{'Workout Videos'.toUpperCase()}</Text>
    <Text style={{ color: 'white', fontSize: 16, fontFamily: font_SFProDisplay_Regular, marginTop: 10, marginLeft: 20, marginRight: 20, textAlign: 'center' }}> No videos for this categories.</Text>
  </View>)

}

class Workoutlist extends ValidationComponent {

  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      catData: null,
      page: 0,
      isvideoDataLoad: false,
      isFilterVisible: false,
      titlesort: 0,
      durationsort: 0,
      sortName: 'SORT',
      isIAPVisible: false,
      DetailScreen:false

    };
    subthis = this
    console.log('constructor call')
    this.wscall(theNavigation.getParam('selectedId'), this.state.titlesort, this.state.durationsort)
  
  }

  refresh = () => {
    this.setState({DetailScreen:false})
}

  static navigationOptions = ({ navigation }) => {
    theNavigation = navigation;
    console.log('navigation option call')
    return {
      header: (
        <Header style={styles.headerAndroidnav}>
          <StatusBar barStyle="light-content" backgroundColor="#051f3a" />
          <Left style={{ flex: 0.1, alignItems: 'center', justifyContent: 'center' }} >
            <Button transparent onPress={() => navigation.goBack()} style={{ alignSelf: 'center', width: 44, height: 44 }} >
              <Image source={backWhite} style={{ width: 22, height: 22, tintColor: "white" }} />
            </Button>
          </Left>
          <View style={{ alignSelf: 'center', justifyContent: 'center', flexDirection: 'row', flex: 1 }}>
            {/* <Text style={styles.headerTitle} >{theNavigation.getParam('title').toUpperCase()}</Text> */}
            <Text style={styles.headerTitle} >{'browse by'.toUpperCase()}</Text>
          </View>
          <Right style={{ alignSelf: 'center', justifyContent: 'center', flex: 0.1 }}>
            {/* <Button transparent style={{ alignSelf: 'baseline', width: 44, height: 44 }} onPress={() => { subthis.toggleFilterModal() }}><Image source={filter} style={{ width: 22, height: 22, }} /></Button> */}
          </Right>
        </Header>
      )
    }
  }
  
  componentDidMount() {
    console.log("componentDidMount")
    this.setState({
      catData: theNavigation.getParam('catData'),
      page: theNavigation.getParam('currentIndex'),
      selectedId: theNavigation.getParam('selectedId'),
      title: theNavigation.getParam('title'),
    }) 
  }
  oncatChange = (value) => {
    if(this.state.DetailScreen == false){
      console.log('onCat Change')
      this.wscall(this.state.catData.subcategories[value.i].id, this.state.titlesort, this.state.durationsort)
    }else{

    }  
  }

  wscall = async (selectedId, titlesort, durationsort) => {
    let httpMethod = 'POST'
    let strURL = workoutVideos
    let headers = {
      'Content-Type': 'application/json',
      'token': token,
      'userid': userid,
    }
    let params = {
      'cat_id': selectedId,
      'title': titlesort,
      'duration': durationsort,
    }
 console.log('WorkoutList API Call')
    APICall.callWebService(httpMethod, strURL, headers, params, (resJSON) => {
      
      if (resJSON.status == 200) {

        var newobj = this.state.videData
        if (newobj == undefined) {
          newobj = {}
        }
        newobj[selectedId] = resJSON.data

        this.setState({
          isLoading: false,
          videData: newobj,
          isvideoDataLoad: true,
        })

        return
      } else {
        this.refs.toast.show(resJSON.message);
      }

    })
  }
  renderPage() {
    if (this.state.catData != null) {
      console.log(this.state.videData);
      var facetsArray = this.state.catData.subcategories;
      var facetListViewArray = [];
      facetsArray = facetsArray.map((facet, i) => {
        facetListViewArray.push(
          <Page tabLabel={{ label: facet.title }} data={this.state.videData[facet.id]} videoLoad={this.state.isvideoDataLoad} />
        )
      })
      return (facetListViewArray)
    }

    return (<Page tabLabel={{ label: "" }} />)
  }

  renderVideo() {
    if (this.state.videData != null) {
      var facetsArray = this.state.videData;
      var facetListViewArray = [];
      facetsArray = facetsArray.map((facet, i) => {
        facetListViewArray.push(
          <Text>
            {facet.title}
          </Text>
        )
      })
      return (facetListViewArray)
    }
    return (<Page tabLabel={{ label: "" }} />)
  }

  toggleFilterModal = () => {
    this.setState({ isFilterVisible: !this.state.isFilterVisible });
    console.log(this.state.isFilterVisible);
  }

  toggleHideModal = (filterOptions) => {
    console.log('filterOptions', filterOptions);
    var title = 0
    var duration = 0
    var sortName = 'SORT'
    if (filterOptions.dictSearchSetting.duration[0].isSelected) {
      //sortest video
      duration = 1
      sortName = 'Shortest'
    } else if (filterOptions.dictSearchSetting.duration[1].isSelected) {
      //longest video
      duration = 2
      sortName = 'Longest'
    } else if (filterOptions.dictSearchSetting.duration[2].isSelected) {
      //longest video
      title = 1
      sortName = 'A-Z'
    }

    this.setState({
      isFilterVisible: false,
      titlesort: title,
      durationsort: duration,
      sortName: sortName
    });
    this.wscall(this.state.selectedId, title, duration)
  }

  _scrollX = new Animated.Value(theNavigation.getParam('currentIndex'));
  // 6 is a quantity of tabs
  interpolators = Array.from({ length: 6 }, (_, i) => i).map(idx => ({
    // scale: this._scrollX.interpolate({
    //   inputRange: [idx - 1, idx, idx + 1],
    //   outputRange: [1, 1.2, 1],
    //   extrapolate: 'clamp',
    // }),
    opacity: this._scrollX.interpolate({
      inputRange: [idx - 1, idx, idx + 1],
      outputRange: [0.9, 1, 0.9],
      extrapolate: 'clamp',
    }),
    textColor: '#ffffff',
    // textColor: this._scrollX.interpolate({
    //   inputRange: [idx - 1, idx, idx + 1],
    //   outputRange: ['#000', '#ffffff', '#000'],
    // }),
    backgroundColor: this._scrollX.interpolate({
      inputRange: [idx - 1, idx, idx + 1],
      outputRange: ['#0F2A47', '#092441', '#0F2A47'],
      extrapolate: 'clamp',
    }),
  }));


  togglePopupPromocode = () => {
    this.setState({ isIAPVisible: !this.state.isIAPVisible });
  }

  render() {
    if (this.state.isLoading) {
      return (<BackgroundImage></BackgroundImage>)
    } else {
      return (
        <Container flex-direction={"row"} style={{ flex: 1, }} >
          <BackgroundImage  >
            <SafeAreaView />

            <ScrollableTabView
              tabBarActiveTextColor="white"
              tabBarInactiveTextColor='white'
              tabBarBackgroundColor='#0F2A47'
              //renderTabBar={() => <TabBar underlineColor="transparent" tabBarTextStyle={{ fontSize: 18, fontFamily: font_MyriadPro_Regular, }} tabBarStyle={{ borderBottomWidth: 0, marginTop: 0,height:50,alignItems:'center'}} />}
              initialPage={this.state.page}
              Page={this.state.page}
              onChangeTab={(value) => this.oncatChange(value)}
              renderTabBar={() => (
                <TabBar
                  underlineColor="#0F2A47"
                  underlineHeight={0}
                  tabBarStyle={{
                    backgroundColor: "#0F2A47",
                    borderBottomWidth: 0, marginTop: 0
                  }}
                  renderTab={(tab, page, isTabActive, onPressHandler, onTabLayout) => (
                    <Tab
                      key={this.state.page}
                      tab={tab}
                      page={page}
                      isTabActive={isTabActive}
                      onPressHandler={onPressHandler}
                      onTabLayout={onTabLayout}
                      styles={this.interpolators[page]}
                    />
                  )}
                />
              )}
              onScroll={(x) => this._scrollX.setValue(x)}
            >
              {this.renderPage()}

            </ScrollableTabView>
            <View style={{ backgroundColor: '#2265B3', height: 50, alignItems: 'center', justifyContent: 'center' }}>
              <Button transparent style={{ alignSelf: 'center', justifyContent: 'center', alignItems: 'center', flex: 1, width: '100%' }}
                onPress={() => { subthis.toggleFilterModal() }}
              >
                <Image source={sort} style={{ width: 24, height: 24 }} />
                <Text style={{ color: 'white', fontSize: 20, fontFamily: font_MyriadPro_Bold, marginTop: 10 }}>{this.state.sortName}</Text>
              </Button>
            </View>
            <Modal isVisible={this.state.isFilterVisible} style={{ margin: 0 }}>
              <FilterPopup
                hidePopup={(filter) => this.toggleHideModal(filter)}
                closePopup={() => this.toggleFilterModal()}
                nameTitle='Workout Name'
                titlesort={this.state.titlesort}
                durationsort={this.state.durationsort}
              />
            </Modal>
            <Modal isVisible={this.state.isIAPVisible} style={{ margin: 0, padding: 0 }} >
              <InAppPurchase
                hidePopup={() => this.togglePopupPromocode()}
                closeBtnPressed={() => this.togglePopupPromocode()}
              />
            </Modal>
          </BackgroundImage>
        </Container>
      );
    }
  }
}
export default Workoutlist;

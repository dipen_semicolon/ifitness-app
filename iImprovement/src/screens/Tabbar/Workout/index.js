import React, { Component } from 'react';
import { Image, ImageBackground } from "react-native";
import { createStackNavigator, TabNavigator } from 'react-navigation';
import Workout from './Workout'
import workoutlist from './workoutlist/'
import workoutDetail from './workoutDetail'

import {
  Container, Header, Left, Body, Right, Button, Icon, Title, Text, View
} from "native-base";


const RootStack = createStackNavigator(
  {
    Workout: {
      screen: Workout, navigationOptions: () => ({
      })
    },
    workoutlist: {
      screen: workoutlist, navigationOptions: () => ({
      })
    },
    workoutDetail: {
      screen: workoutDetail, navigationOptions: () => ({
      })
    },
  },
  {
    initialRouteName: 'Workout',
    headerMode: 'screen',
    orientation: 'portrait',
    navBarBackgroundColor: 'transparent',
    headerStyle: {

    },
  },

);

RootStack.navigationOptions = ({ navigation }) => {

  let tabBarVisible = true;
  if (navigation.state.index > 0) {
      tabBarVisible = false
  }
  return {
      tabBarVisible,
  };
};
export default RootStack;

import React, { Component } from "react";
import { Image, ScrollView, SafeAreaView, TouchableOpacity, StatusBar } from "react-native";
import { Container, View, Text, Button, Header, Left, Right, ListItem, } from "native-base";
import styles from "./style";


const Checkbox_Unchecked = require("../../../../../assets/images/Checkbox_Unchecked.png");
const Checkbox_Checked = require("../../../../../assets/images/Checkbox_Checked.png");

class FilterPopup extends Component {

    constructor(props) {
        super(props);
        this.state = {
            dictSearchSetting: {
                duration: [
                    { name: 'Shortest', isSelected: this.props.durationsort == 1 ? true : false },
                    { name: 'Longest', isSelected: this.props.durationsort == 2 ? true : false },
                    { name: 'A-Z', isSelected: this.props.titlesort == 1 ? true : false },
                ],
            }
        }
    }


    render() {
        return (
            <Container>
                <BackgroundImage  >
                    <Header style={styles.headerAndroidnav}>
                        <StatusBar barStyle="light-content" backgroundColor="#051f3a" />
                        <Left style={{ }} >
                        </Left>
                        <View style={{ alignSelf: 'center',}}>
                            <Text style={styles.headerTitle} >{'sort'.toUpperCase()}</Text>
                        </View>
                        <Right style={{ }}>
                            <Button style={{ backgroundColor: '#164883', height: 44 }} onPress={() => { this.props.hidePopup(this.state) }}><Text style={{ color: 'white' }}>Done</Text></Button>
                        </Right>
                    </Header>

                    <View style={{ backgroundColor: 'tranparent' }}>
                        <ListItem style={{ backgroundColor: '#0C2A4C', height: 60, paddingLeft: 10, marginLeft: 0, borderBottomWidth: 0 }}>
                            <View style={{ flex: 1, justifyContent: 'center', alignSelf: 'center' }}>
                                <Text style={styles.sectionHeadingText}>
                                    DURATION
                                </Text>
                            </View>
                        </ListItem>
                        <ListItem style={{ borderBottomWidth: 0,height:60 }}>
                            <TouchableOpacity
                                onPress={() => this._onPressButton(this.state.dictSearchSetting.duration, 0)}
                                style={{ flex: 1, flexDirection: 'row' }}>
                                <Text style={styles.contentText}>
                                    {this.state.dictSearchSetting.duration[0].name}
                                </Text>
                                <Image
                                    style={styles.Cell_Image}
                                    source={this.state.dictSearchSetting.duration[0].isSelected ? Checkbox_Checked : Checkbox_Unchecked}
                                />
                            </TouchableOpacity>
                        </ListItem>
                        <ListItem style={{ borderBottomWidth: 0,height:60}}>
                            <TouchableOpacity
                                onPress={() => this._onPressButton(this.state.dictSearchSetting.duration, 1)}
                                style={{ flex: 1, flexDirection: 'row' }}>
                                <Text style={styles.contentText}>
                                    {this.state.dictSearchSetting.duration[1].name}
                                </Text>
                                <Image
                                    style={styles.Cell_Image}
                                    source={this.state.dictSearchSetting.duration[1].isSelected ? Checkbox_Checked : Checkbox_Unchecked}
                                />
                            </TouchableOpacity>
                        </ListItem>
                        <ListItem style={{ backgroundColor: '#0C2A4C', height: 60, paddingLeft: 10, marginLeft: 0, borderBottomWidth: 0 }}>
                            <View style={{ flex: 1, justifyContent: 'center', alignSelf: 'center' }}>
                                <Text style={styles.sectionHeadingText}>
                                    {this.props.nameTitle.toUpperCase()}
                                </Text>
                            </View>

                        </ListItem>
                        <ListItem style={{ borderBottomWidth: 0,height:60 }}>
                            <TouchableOpacity
                                onPress={() => this._onPressButton(this.state.dictSearchSetting.duration, 2)}
                                style={{ flex: 1, flexDirection: 'row' }}>
                                <Text style={styles.contentText}>
                                    {this.state.dictSearchSetting.duration[2].name}
                                </Text>
                                <Image
                                    style={styles.Cell_Image}
                                    source={this.state.dictSearchSetting.duration[2].isSelected ? Checkbox_Checked : Checkbox_Unchecked}
                                />
                            </TouchableOpacity>
                        </ListItem>
                    </View>
                </BackgroundImage>
            </Container>
        );
    }


    _onPressButton = (itemObj, index) => {
        let data = Object.assign({}, this.state.dictSearchSetting);
        for (var i = 0; i < data.duration.length; i++) {
            data.duration[i].isSelected = false
            if (i == index) {
                data.duration[index].isSelected = !data.duration[index].isSelected
            }
        }
        return this.setState({ data });
    }

    _onPressNameButton = (itemObj, index) => {
        let data = Object.assign({}, this.state.dictSearchSetting);
        data.workoutName[0].isSelected = !data.workoutName[0].isSelected
        return this.setState({ data });
    }
}

export default FilterPopup;

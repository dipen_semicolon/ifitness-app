export default function() {
	return (
		[
			{
				name: 'iImprove Basics',
				url:'https://www.mtlblog.com/u/2017/10/25/b9c739ff370328ffe20dc0cd29a9ca8a9e6ac656.png_1200x630.png',
				options: [
					{name: 'Example 1.1'},
					{name: 'Example 1.2'},
					{name: 'Example 1.3'},
					{name: 'Example 1.4'},
					{name: 'Example 1.5'},
					{name: 'Example 1.6'},
					{name: 'Example 1.7'},
				]
			},
			{
				name: 'iImprove Advanced',
				url:'https://www.mtlblog.com/u/2017/10/25/b9c739ff370328ffe20dc0cd29a9ca8a9e6ac656.png_1200x630.png',
				options: [
					{name: 'Example 2.1'},
					{name: 'Example 2.2'}
				]
			},
			{
				name: 'iImprove Calisthenics',
				url:'https://www.mtlblog.com/u/2017/10/25/b9c739ff370328ffe20dc0cd29a9ca8a9e6ac656.png_1200x630.png',
				options: [
					{name: 'Example 3.1'},
					{name: 'Example 3.2'},
					{name: 'Example 3.3'},
					{name: 'Example 3.4'},
					{name: 'Example 3.5'},
					{name: 'Example 3.6'},
					{name: 'Example 3.7'},
				]
			},
			{
				name: 'Athlete Workout Collection',
				url:'https://www.mtlblog.com/u/2017/10/25/b9c739ff370328ffe20dc0cd29a9ca8a9e6ac656.png_1200x630.png',
				options: [
					{name: 'Example 4.1'},
					{name: 'Example 4.2'},
					{name: 'Example 4.3'},
					{name: 'Example 4.4'},
					{name: 'Example 4.5'},
					{name: 'Example 4.6'},
					{name: 'Example 4.7'},
					{name: 'Example 4.8'},
					{name: 'Example 4.9'},
				]
			},
		]
	)
}
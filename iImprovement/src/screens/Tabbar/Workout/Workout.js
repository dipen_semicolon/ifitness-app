import React, { Component } from "react";
import { SafeAreaView, Image, ScrollView, TouchableHighlight, ImageBackground, Animated, StatusBar, BackHandler, Alert } from "react-native";
import { Container, View, Text, Content, Button, Header, Left, Right } from "native-base";
import styles from "./style";
import ValidationComponent from 'react-native-form-validator';
import HandleBack from "../../Default/HandleBack";//../../screens/Default/HandleBack
import Modal from "react-native-modal";
import InAppPurchase from "../../inapp"

const topIcon = require("../../../../assets/images/topIcon.png");
const right_arrow = require("../../../../assets/images/right-arrow.png");

import DummyData from './DummyData';
const resizeMode = 'center';
import Panel from './Panel';
import * as Pref from '../../../Pref/Pref';

class Workout extends ValidationComponent {

  constructor(props) {
    super(props);
    this.state = {
      parentCheckedOptionsArray: [],
      data: [],
      isLoading: true,
      animation: new Animated.Value(),
      expanded: true,
      isModelVisible: false,
    };
  }


  static navigationOptions = ({ navigation }) => {
    theNavigation = navigation;

    return {
      header: (
        <Header style={styles.headerAndroidnav}>
          <StatusBar barStyle="light-content" backgroundColor="#051f3a" />

          <Left style={{ alignSelf: 'flex-start', flex: 0.1 }} >
          </Left>
          <View style={{ alignSelf: 'flex-start', flex: 1 }}>
            <View style={{ alignItems: 'center', justifyContent: 'center', flex: 1, marginTop: 15, marginBottom: 15 }}>
              <Image source={topIcon} style={[styles.topIcon]} />
            </View>
          </View>
          <Right style={{ alignSelf: 'flex-start', flex: 0.1 }}>
            {/* <Button transparent style={{ alignSelf: 'baseline', width: 44, height: 44 }} ><Image source={search} style={{ width: 22, height: 22, tintColor: 'black' }} /></Button> */}
          </Right>
        </Header>
      )
    }
  }

  wscall = async () => {
    let httpMethod = 'POST'
    let strURL = workoutCategories
    let headers = {
    }
    let params = {
    }

    APICall.callWebService(httpMethod, strURL, headers, params, (resJSON) => {
      console.log('workout Screen -> ');
      console.log(resJSON)
      if (resJSON.status == 200) {
        let data = resJSON.data
        Pref.getData(Pref.kIsInAppShow, (userinfo) => {
          console.log('kIsInAppShow',userinfo)
          if(userinfo == null){
            this.setState({isModelVisible:true})
          }else{
            //this.setState({isModelVisible:true})
          }

          this.setState({ data: data })
          this.loadData();
        })
        return
      } else {
        this.refs.toast.show(resJSON.message);
      }

    })
  }

  componentDidMount() {
    this._data = [];
    this.facetsContainerVisibilityArray = [];
    //this.loadData();
    this.wscall();

  }

  loadData() {
    for (var item in this.state.data) {
      this._data.push(this.state.data[item]);
      this.facetsContainerVisibilityArray.push(false);
    }
    console.log(this.facetsContainerVisibilityArray)
    this.setState({
      isLoading: false,
      facetsContainerVisibility: this.facetsContainerVisibilityArray
    });
  }


  onCatClick = (cat_id, subcatIndex, index) => {
    this.props.navigation.push('workoutlist', {
      catData: this.state.data[index],
      currentIndex: subcatIndex,
      selectedId: cat_id,
      title: this.state.data[index].title
    })
  }



  toggleOptionsView(parentIndex) {
    var oldstatus = this.facetsContainerVisibilityArray[parentIndex];
    for (let i = 0; i < this.facetsContainerVisibilityArray.length; i++) {
      this.facetsContainerVisibilityArray[i] = false;
    }

    if (oldstatus === true) {
      this.facetsContainerVisibilityArray[parentIndex] = false;
    } else {
      this.facetsContainerVisibilityArray[parentIndex] = true;
    }
    this.setState({
      facetsContainerVisibility: this.facetsContainerVisibilityArray
    });

    this.setState({
      expanded: !this.state.expanded
    });

    if (this.state.expanded) {
      initialValue = 0,
        finalValue = 100;
    } else {
      initialValue = 0,
        finalValue = 0;
    }
    console.log("initialValue", initialValue, finalValue);
    this.state.animation.setValue(initialValue);
    Animated.spring(
      this.state.animation,
      {
        toValue: finalValue
      }
    ).start();

  }


  renderOption() {
    var facetsArray = this.state.data;
    var facetListViewArray = [];
    var state = this.state;

    facetsArray = facetsArray.map((facet, i) => {
      facetListViewArray.push(
        <Panel title={facet.title} photo={facet.photo} >
          {this.rendersubOption(facet.subcategories, i)}
        </Panel>
      )

    })
    return (facetListViewArray)
  }

  rendersubOption(facetsArray, index) {
    var facetListViewArray = [];
    var state = this.state;
    facetsArray = facetsArray.map((facet, i) => {
      facetListViewArray.push(
        <TouchableHighlight
          style={styles.button}
          onPress={() => { this.onCatClick(facet.id, i, index) }}
          underlayColor="transparent"
          activeOpacity={1}
        >
          <View style={[styles.facetMenuWrapper, { flexDirection: 'row' }]} >
            <View style={{ alignItems: 'flex-start', justifyContent: 'center', width: '90%' }}>
              <Text style={[styles.facetTitle]}>
                {facet.title}
              </Text>
            </View>
            <View style={{ flex: 1, alignItems: 'flex-end', justifyContent: 'center' }}>
              <Image source={right_arrow} style={{ width: 22, height: 22, marginRight: 16 }} />
            </View>
          </View>

        </TouchableHighlight>

      )

    })
    return (facetListViewArray)
  }

  onBack = () => {
    Alert.alert(
      "Exit",
      "Are you sure you want to exit?",
      [
        { text: "Cancel", style: "cancel" },
        { text: "Ok", onPress: () => BackHandler.exitApp() },
      ],
      { cancelable: false },
    );
    return true;
  };

  togglePopupPromocode = () => {
    this.setState({ isModelVisible: !this.state.isModelVisible });
    Pref.setData(Pref.kIsInAppShow,"1")
  }

  render() {
    if (this.state.isLoading) {
      return (<BackgroundImage></BackgroundImage>)
    } else {
      return (
        <HandleBack onBack={() => this.onBack()}>
          <Container flex-direction={"row"} style={{ flex: 1 }} >
            <BackgroundImage >
              <SafeAreaView />
              <Content showsHorizontalScrollIndicator={false} showsVerticalScrollIndicator={false}>
                <ScrollView style={styles.container} showsHorizontalScrollIndicator={false} showsVerticalScrollIndicator={false}>
                  {this.renderOption()}

                </ScrollView>
              </Content>
            </BackgroundImage>
            <Modal isVisible={this.state.isModelVisible} style={{ margin: 0, padding: 0 }} >
              <InAppPurchase
                hidePopup={() => this.togglePopupPromocode()}
                closeBtnPressed={() => this.togglePopupPromocode()}
              />
            </Modal>
          </Container>
        </HandleBack>
      );
    }
  }

}

export default Workout;

const React = require("react-native");
const { Dimensions, Platform } = React;
let deviceWidth = Dimensions.get('window').width

export default {
    headerAndroidnav: {
        backgroundColor: '#051F3A',
        shadowOpacity: 0,
        borderBottomWidth: 0,
        height: (deviceWidth * 100) / 320,
    },
    topIcon: {

        ...Platform.select({
            ios: {
                height: (deviceWidth * 70) / 320,
                width: (deviceWidth * 119) / 320,
            },
            android: {
                height: 88, //(deviceWidth * 42) / 320,
                width: 150
            },
        }),

        resizeMode: 'cover',

    },
    videoTitle:{
        color: '#8CA9C7',
        fontSize: 18,
      },
      recepieTitle:{
        color: 'white',
        fontSize: 22,
        backgroundColor:'rgba(0,0,0,0.5)',
        paddingHorizontal: 20,
        paddingVertical: 10,
    
      },
      videoDescription:{
        color: '#8CA9C7',
        fontSize: 18,
      },
      videoDuration:{
        color: '#8CA9C7',
        fontSize: 16,
        marginTop:3,
      },
      activityIndicatorStyle: {
        width: 80,
        height: 60,
        backgroundColor: 'transparent'
      }
};

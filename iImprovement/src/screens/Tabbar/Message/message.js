import React, { Component } from "react";
import { SafeAreaView, Image, FlatList, StatusBar, TouchableHighlight, ActivityIndicator, BackHandler } from "react-native";
import { Container, View, Text, Content, Header, Left, Right } from "native-base";
import styles from "./style";
import ValidationComponent from 'react-native-form-validator';
const topIcon = require("../../../../assets/images/topIcon.png");
import imageCacheHoc from 'react-native-image-cache-hoc';
import Swipeout from 'react-native-swipeout';

import * as Pref from '../../../Pref/Pref';

const star = require("../../../../assets/images/email.png");
const food_placeholder = require("../../../../assets/images/food_placeholder.png");

var subthis;
const CacheableImage = imageCacheHoc(Image, {
  fileHostWhitelist: ['100.25.250.56'],
  validProtocols: ['http', 'https'],
  defaultPlaceholder: {
    component: ActivityIndicator,
    props: {
      style: styles.activityIndicatorStyle
    }
  }
});

// Buttons
var swipeoutBtns = [
  {
    text: 'Archive',
    backgroundColor: '#ff0000',
    component: <View flexDirection={'row'} style={{ alignItems: 'center', justifyContent: 'center', flex: 1 }} >
      <Text style={{ fontSize: 12, color: 'white', alignSelf: 'center' }}> DELETE </Text>
    </View>,
    onPress: swipeBtnClick = () => {
      var arr = subthis.state.Data
      var obj = arr[subthis.state.rowID]
      subthis.wscallBackgroundDeleteMessage(obj.id)
      delete arr.splice(subthis.state.rowID, 1);
      subthis.setState({
        Data: arr, sectionID: null,
        rowID: null,
      })
    },
  }
]

class Message extends ValidationComponent {

  constructor(props) {
    super(props);
    subthis = this
    this.state = {
      isLoading: true,
      page: 0,
      Data: [],
      sectionID: null,
      rowID: null,

    }
  }

  static navigationOptions = ({ navigation }) => {
    theNavigation = navigation;

    return {
      header: (
        <Header style={[styles.headerAndroidnav, { borderBottomWidth: 1, borderBottomColor: '#092441' }]}>
          <StatusBar barStyle="light-content" backgroundColor="#051f3a" />

          <Left style={{ alignSelf: 'flex-start', flex: 0.1 }} >
          </Left>
          <View style={{ alignSelf: 'flex-start', flex: 1 }}>
            <View style={{ alignItems: 'center', justifyContent: 'center', flex: 1, marginTop: 15, marginBottom: 15 }}>
              <Image source={topIcon} style={[styles.topIcon]} />
            </View>
          </View>
          <Right style={{ alignSelf: 'flex-start', flex: 0.1 }}>
            {/* <Button transparent style={{ alignSelf: 'baseline', width: 44, height: 44 }} ><Image source={search} style={{ width: 22, height: 22, tintColor: 'black' }} /></Button> */}
          </Right>
        </Header>
      )
    }
  }
  componentDidMount() {
    this.props.navigation.addListener('willFocus', (route) => {
      Pref.setData(Pref.kNewMessage, "0")
      this.wscall()
    });
  }

  wscallBackground = async () => {
    let httpMethod = 'POST'
    let strURL = messageList
    let headers = {
      'Content-Type': 'application/json',
      'token': token,
      'userid': userid,
    }
    let params = {
    }

    APICall.callWebServiceBackground(httpMethod, strURL, headers, params, (resJSON) => {
      console.log('workoutList Screen -> ');
      console.log(resJSON)
      if (resJSON.status == 200) {
        this.setState({
          isLoading: false,
          Data: resJSON.data,
          isRefreshing: false
        })
        return
      } else {
        this.refs.toast.show(resJSON.message);
      }

    })
  }

  wscallBackgroundDeleteMessage = async (id) => {
    let httpMethod = 'POST'
    let strURL = deletemessage
    let headers = {
      'Content-Type': 'application/json',
      'token': token,
      'userid': userid,
    }
    let params = {
      'id': id,
    }

    APICall.callWebServiceBackground(httpMethod, strURL, headers, params, (resJSON) => {
      console.log('Message delete Screen -> ');
      console.log(resJSON)
      if (resJSON.status == 200) {
        return
      } else {
        this.refs.toast.show(resJSON.message);
      }
    })
  }

  wscall = async () => {
    let httpMethod = 'POST'
    let strURL = messageList
    let headers = {
      'Content-Type': 'application/json',
      'token': token,
      'userid': userid,
    }
    let params = {
    }

    APICall.callWebService(httpMethod, strURL, headers, params, (resJSON) => {
      console.log('workoutList Screen -> ');
      console.log(resJSON)
      if (resJSON.status == 200) {
        this.setState({
          isLoading: false,
          Data: resJSON.data,
          isRefreshing: false
        })
        return
      } else {
        this.refs.toast.show(resJSON.message);
      }

    })
  }

  loadTableCells(item, index) {
    return (this.cellVideoList(item, index));
  }
  onPressDetail = (facet, rowID) => {
    console.log(facet);
    if (facet.type == 1) {
      //video
      this.wscallSingleVideo(facet.video)
    } else if (facet.type == 2) {
      this.wscallSingleReceipe(facet.receipe)
      //Receipe
    } else if (facet.type == 3) {
      //Webpage
      console.log(facet.link)
      theNavigation.push('webpage', {
        url: facet.link
      })
    }
  }

  wscallSingleVideo = async (id) => {
    let httpMethod = 'POST'
    let strURL = singleVideo
    let headers = {
      'Content-Type': 'application/json',
      'token': token,
      'userid': userid,
    }
    let params = {
      'id': id
    }

    APICall.callWebService(httpMethod, strURL, headers, params, (resJSON) => {
      console.log('workoutList Screen -> ');
      console.log(resJSON)
      if (resJSON.status == 200) {
        var data = resJSON.data
        theNavigation.push('workoutDetail', {
          title: data.title,
          data: data
        })
        return
      } else {
        this.refs.toast.show(resJSON.message);
      }

    })
  }

  wscallSingleReceipe = async (id) => {
    let httpMethod = 'POST'
    let strURL = singleReceipe
    let headers = {
      'Content-Type': 'application/json',
      'token': token,
      'userid': userid,
    }
    let params = {
      'id': id
    }

    APICall.callWebService(httpMethod, strURL, headers, params, (resJSON) => {
      console.log('workoutList Screen -> ');
      console.log(resJSON)
      if (resJSON.status == 200) {
        var data = resJSON.data
        theNavigation.push('FoodDetail', {
          title: data.title,
          data: data
        })

        return
      } else {
        this.refs.toast.show(resJSON.message);
      }

    })
  }


  cellVideoList = (facet, rowID) => {
    sectionID = 0;

    return (
      <Swipeout right={swipeoutBtns} style={{ backgroundColor: 'transparent' }}
        close={!(this.state.sectionID === sectionID && this.state.rowID === rowID)}
        onOpen={() => {
          this.setState({
            sectionID,
            rowID,
          })
        }}
        onClose={() => console.log('===close')}
      >

        <TouchableHighlight style={{ flexDirection: 'row' }}
          onPress={() => {
            this.onPressDetail(facet, rowID)
          }}
          underlayColor="transparent"
          activeOpacity={1}
        >
          <View style={{ flexDirection: 'row', borderBottomColor: '#193654', borderBottomWidth: 2, paddingBottom: 10, flex: 1, paddingLeft: 20, paddingRight: 20, marginTop: 10, }}>
            <View style={{ justifyContent: 'center', alignItems: 'center', width: 80, height: 60 }}>
              {
                this.setupImage(facet.photo,facet.type)
              }
            </View>
            <View style={{ flex: 1, paddingLeft: 10, }}>
              <Text style={styles.videoTitle}>{facet.title}</Text>
            </View>
          </View>
        </TouchableHighlight>
      </Swipeout>

    );
  }

  setupImage = (photo,type) => {
    console.log('setupImage',photo,type)
    if(type == 1){
      //workout video
        return (<CacheableImage source={{ uri: photo, cache: 'only-if-cached', }} style={{ width: 80, height: 60 }} permanent={true}>
        </CacheableImage>)
    }else if(type == '2'){
      //receipe
      if(photo.length >= 1){
        return (<CacheableImage source={{ uri: photo, cache: 'only-if-cached', }} style={{ width: 80, height: 60 }} permanent={true}>
        </CacheableImage>)
      }else{
        return (<Image source={food_placeholder} style={{ width: 80, height: 60, resizeMode: 'contain' }}/>)
      }
    }else if(type == 3){
      //weburl
      return (
        <CacheableImage source={{ uri: 'http://100.25.250.56:8081/images/internet.png', cache: 'only-if-cached', }} style={{ width: 80, height: 60, resizeMode: 'contain' }} permanent={true}>
        </CacheableImage>
      )
    }
  }

  onBack = () => {
    Alert.alert(
        "Are you sure you want to exit?",
        [
          { text: "Cancel", onPress: () => {}, style: "cancel" },
          { text: "Ok", onPress: () => BackHandler.exitApp() },
        ],
        { cancelable: false },
      );
     return true;
  };
  
  render() {
    if (this.state.isLoading) {
      return (<BackgroundImage></BackgroundImage>)
    } else {
      return (
        <HandleBack onBack={this.onBack}>
          <Container flex-direction={"row"} style={{ flex: 1 }} >
            <BackgroundImage >
              <SafeAreaView />
              {
                this.state.Data.length == 0 ?
                  <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                    <Image source={star} style={{ width: 25, height: 25 }} />
                    <Text style={{ color: 'white', fontSize: 20, fontFamily: font_SFProDisplay_Bold, marginTop: 10 }}>{'Message'.toUpperCase()}</Text>
                    <Text style={{ color: 'white', fontSize: 16, fontFamily: font_SFProDisplay_Regular, marginTop: 10, marginLeft: 20, marginRight: 20, textAlign: 'center' }}>{'No new messages.'}</Text>
                  </View>
                  :
                  <Content>
                    <FlatList
                      style={{ flex: 1, borderTopColor: '#193654', borderTopWidth: 2, }}
                      data={this.state.Data}
                      renderItem={
                        ({ item, index }) => this.loadTableCells(item, index)
                      }
                      keyExtractor={(index) => index.toString()}
                    />
                  </Content>

              }
            </BackgroundImage>
          </Container>
        </HandleBack>
      );
    }
  }

}

export default Message;

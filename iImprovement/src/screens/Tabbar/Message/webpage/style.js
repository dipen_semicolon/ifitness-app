const React = require("react-native");
const { Dimensions, Platform } = React;
let deviceWidth = Dimensions.get('window').width

export default {
  headerAndroidnav: {
    backgroundColor: '#2265B3',
    shadowOpacity: 0,
    borderBottomWidth: 0,
    height: (deviceWidth * 64) / 320,
  },
  headerTitle: {
    fontFamily: font_SFProDisplay_Bold,
    color: 'white',
    textAlign: 'center'
  },
};

import React, { Component } from "react";
import { SafeAreaView, TextInput, alert, Dimensions, StatusBar, Image, WebView } from "react-native";
import { Container, View, Text, Content, Button, Header, Left, Right } from "native-base";
import styles from "./style";
import ValidationComponent from 'react-native-form-validator';
const backWhite = require("../../../../../assets/images/back.png");

class ContentPage extends ValidationComponent {

  constructor(props) {
    super(props);
    this.state = { description: "", loading: true, title: '' };
  }

  static navigationOptions = ({ navigation }) => {
    theNavigation = navigation;
    console.log(theNavigation.getParam('url'))
    return {
      header: (
        <Header style={styles.headerAndroidnav}>
          <StatusBar barStyle="light-content" backgroundColor="#051f3a" />
          <Left style={{ flex: 0.1, alignItems: 'center', justifyContent: 'center' }} >
            <Button transparent onPress={() => navigation.goBack()} style={{ alignSelf: 'center', width: 44, height: 44 }} >
              <Image source={backWhite} style={{ width: 22, height: 22, tintColor: "white" }} />
            </Button>
          </Left>
          <View style={{ alignSelf: 'center', justifyContent: 'center', flexDirection: 'row', flex: 1 }}>
            <Text style={styles.headerTitle} >{'Web Link'.toUpperCase()}</Text>
          </View>
          <Right style={{ alignSelf: 'center', justifyContent: 'center', flex: 0.1 }}>
          </Right>
        </Header>
      )
    }
  }



  render() {
    if (this.state.isLoading) {
      return (<View></View>)
    } else {
      return (
        <Container flex-direction={"row"} style={{ flex: 1 }} >
          <BackgroundImage >
            <SafeAreaView />
            <WebView
              source={{ uri: theNavigation.getParam('url') }}
            />
          </BackgroundImage>
        </Container>
      );
    }
  }

}

export default ContentPage;

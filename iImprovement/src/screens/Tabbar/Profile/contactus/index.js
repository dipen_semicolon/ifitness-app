import React, { Component } from "react";
import { SafeAreaView, TextInput, Image, Dimensions, StatusBar, Platform, Alert } from "react-native";
import { Container, View, Text, Content, Button, Header, Left, Right, Textarea } from "native-base";
import styles from "./style";
import ValidationComponent from 'react-native-form-validator';
import * as Pref from '../../../../Pref/Pref';
import ToastSucess from "../../../Default/ToastSucess"


const email = require("../../../../../assets/images/lock.png");
const backWhite = require("../../../../../assets/images/back.png");
const topIcon = require("../../../../../assets/images/topIcon.png");

class ContactUs extends ValidationComponent {

  constructor(props) {
    super(props);
    this.state = { message: "", };
  }

  static navigationOptions = ({ navigation }) => {
    theNavigation = navigation;

    return {
      header: (
        <Header style={styles.headerAndroidnav}>
          <StatusBar barStyle="light-content" backgroundColor="#051f3a" />
          <Left style={{ flex: 0.1, alignItems: 'center', justifyContent: 'center' }} >
            <Button transparent onPress={() => navigation.goBack()} style={{ alignSelf: 'center', width: 44, height: 44 }} >
              <Image source={backWhite} style={{ width: 22, height: 22, tintColor: "white" }} />
            </Button>
          </Left>
          <View style={{ alignSelf: 'center', justifyContent: 'center', flexDirection: 'row', flex: 1 }}>
            {/* <Text style={styles.headerTitle} >{theNavigation.getParam('title').toUpperCase()}</Text> */}
            <Text style={styles.headerTitle} >{'Let’s Communicate'.toUpperCase()}</Text>
          </View>
          <Right style={{ alignSelf: 'center', justifyContent: 'center', flex: 0.1 }}>
            {/* <Button transparent style={{ alignSelf: 'baseline', width: 44, height: 44 }} onPress={() => { subthis.toggleFilterModal() }}><Image source={filter} style={{ width: 22, height: 22, }} /></Button> */}
          </Right>
        </Header>
      )
    }

  }

  componentWillMount() {

  }

  onSendMessage = () => {

    if (this.validtion()) {
      this.wscall(this.state.message);
    }
  }

  validtion = () => {
    this.validate({
      message: { minlength: 5, required: true },
    });
    if (this.isFieldInError('message')) {
      this.refs.toast.show(this.getErrorsInField('message'), 500, () => {
      });
      return false
    }
    return true
  }

  wscall = async (message) => {
    let httpMethod = 'POST'
    let strURL = contact_us
    let headers = {
      'Content-Type': 'application/json',
      'token': token,
      'userid': userid,
    }
    let params = {
      'message': message,
    }

    APICall.callWebService(httpMethod, strURL, headers, params, (resJSON) => {
      console.log('Login Screen -> ');
      console.log(resJSON)
      if (resJSON.status == 200) {
        Alert.alert(
          'Feedback',
          'your feedback is submitted sucessfully.',
          [
            { text: 'Ok', onPress: () => this.props.navigation.goBack() },
          ],
          { cancelable: false }
        )
        return
      } else {
        this.refs.toast.show(resJSON.message);
      }
    })
  }

  render() {
    return (
      <Container flex-direction={"row"} style={{ flex: 1 }} >
        <Toastt ref="toast"></Toastt>
        <ToastSucess ref="toastsucess"></ToastSucess>
        <BackgroundImage >
          <SafeAreaView />
          <Content style={{ paddingLeft: 33, paddingRight: 33 }}>
            <View>
              <Text style={{ color: 'white', marginTop: 40, marginBottom: 5,textAlign:'center',fontSize:25,fontFamily:font_SFProDisplay_Bold}} >Have A Fitness Question or
Concern?</Text>
              <Text style={{ color: 'white', marginTop: 20, marginBottom: 25,textAlign:'center'}}>We would love to hear from you. Tell us how we can further help you in your
fitness journey</Text>

              <View style={[styles.textInputView, { flexDirection: 'row', alignItems: 'center' }]}>
                <Textarea
                  rowSpan={5} placeholder="Comment*"
                  onChangeText={(message) => this.setState({ message })} value={this.state.message}
                  style={[styles.textInput, { marginLeft: 5 }]}
                  placeholderTextColor="#95959a"
                  autoCorrect={false}
                />
              </View>
            </View>
            <View style={{ marginTop: 20 }}>
              <View style={{ justifyContent: 'center', alignItems: 'center', flex: 1, marginTop: 40 }}>
                <Button style={{ height: 50, borderRadius: 30, backgroundColor: '#2265B3', paddingLeft: 30, paddingRight: 30, alignSelf: 'center' }} onPress={() => this.onSendMessage()} ><Text style={{ fontSize: 20 }}>SUBMIT</Text>
                </Button>
              </View>
            </View>
          </Content>
        </BackgroundImage>
      </Container>
    );
  }
}

export default ContactUs;

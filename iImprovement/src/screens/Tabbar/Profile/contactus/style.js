const React = require("react-native");
const { Dimensions, Platform } = React;
const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;

export default {

  headerAndroidnav: {
    backgroundColor: '#2265B3',
    shadowOpacity: 0,
    borderBottomWidth: 0,
    height: (deviceWidth * 64) / 320,
  },
  headerTitle: {
    fontFamily: font_SFProDisplay_Bold,
    color: 'white',
    textAlign: 'center'
  },
  //message listing
  
  textInputView: {
    color: '#95959a',
    borderWidth: 1,
    borderColor: '#95959a',
  },

  textInput: {
    color: 'white',
    fontSize: 16,
    fontFamily: font_SFProDisplay_Regular,
  },

};

import React, { Component } from "react";
import { SafeAreaView, TextInput, alert, Dimensions, StatusBar, Image, TouchableOpacity, BackHandler } from "react-native";
import { Container, View, Text, Content, Button, Header, Left, Right, ListItem, Body } from "native-base";
import styles from "./style";
import ValidationComponent from 'react-native-form-validator';
import imageCacheHoc from 'react-native-image-cache-hoc';
import ImagePicker from 'react-native-image-picker';
import DateTimePicker from 'react-native-modal-datetime-picker';
import Picker from 'react-native-picker';

var dateFormat = require('dateformat');
import moment from 'moment';

import * as Pref from '../../../Pref/Pref';
import ToastSucess from "../../Default/ToastSucess";
const user = require("../../../../assets/images/user.png");
const user_placeholder = require("../../../../assets/images/user_placeholder.png");
const checkbox = require("../../../../assets/images/Checkbox_Unchecked.png");
const checkbox_checked = require("../../../../assets/images/Checkbox_Checked.png");
const premium = require("../../../../assets/images/protection.png");
const info = require("../../../../assets/images/info.png");

var defaultdate = new Date();
defaultdate.setFullYear(defaultdate.getFullYear() - 18);

const CacheableImage = imageCacheHoc(Image, {
    fileHostWhitelist: ['100.25.250.56', 'fbsbx'],
    validProtocols: ['http', 'https'],
    defaultPlaceholder: {
        component: Image,
        props: {
            style: styles.activityIndicatorStyle,
            source: user_placeholder
        }
    }
});
class Profile extends ValidationComponent {

    constructor(props) {
        super(props);
        this.state = {
            name: "", email: "", isLoading: true, selectedImageURI: "", isPhotochange: false, isEditable: false, weight: 0, height: 0, DOB: '', mobile: '', isDateTimePickerVisible: false, selectedDate: '',
            selectedServerDate: '',
            points: 0,
        };

    }

    componentDidMount() {
        //CacheableImage.flush()
        Pref.getData(Pref.kUserInfo, (userinfo) => {
            var obj = userinfo;
            console.log(obj)
            if (obj.DOB.length >= 1) {
                defaultdate = moment(obj.DOB).toDate();
            }
            this.setState({
                selectedImageURI: obj.photo,
                photo: obj.photo,
                fbid: obj.fbid,
                name: obj.name,
                email: obj.email,
                mobile: obj.mobile,
                DOB: obj.DOB,
                gender: obj.Gender,
                height: obj.Height.length == 0 ? "0'0\"" : obj.Height,
                weight: obj.weight.toFixed(2).toString(),
                points: obj.points.toString(),
                isLoading: false,
                isPhotochange: false,
            })

        })
    }


    static navigationOptions = ({ navigation }) => {
        theNavigation = navigation;


    }

    onEditClick = () => {
        if (this.state.isEditable) {
            if (this.validtion()) {
                this.wscall()
            }
        } else {
            this.setState({ isEditable: !this.state.isEditable })
        }
    }

    onLogoutClick = () => {
        Pref.setData(Pref.kUserId, 0)
        this.props.navigation.push('Home')
    }

    onSettingClick = () => {

        theNavigation.push('Setting', {
            onGoBack: () => this.refresh()
        })
    }

    refresh = () => {
        this.setState({ "reload": "1" })
    }

    onClickSelectPhoto = () => {
        var options = {
            title: 'Select Avatar',
            allowsEditing: true,
            storageOptions: {
                skipBackup: true,
                path: 'images'
            }
        };

        ImagePicker.showImagePicker(options, (response) => {
            console.log('Response = ', response);

            if (response.didCancel) {
                console.log('User cancelled image picker');
            }
            else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            }
            else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            }
            else {
                let source = { uri: response.uri };
                console.log('Response = ', source);
                this.setState({
                    selectedImageURI: response.uri,
                    isPhotochange: true
                })
            }
        });
    }

    _showDateTimePicker = () => this.setState({ isDateTimePickerVisible: true });

    _hideDateTimePicker = () => this.setState({ isDateTimePickerVisible: false });

    _handleDatePicked = (date) => {
        console.log(date)
        if (date != undefined) {
            console.log('A date has been picked: ', dateFormat(date, "dd/mm/yyyy"));
            this.setState({ DOB: dateFormat(date, "dd-mmm-yyyy") })
        }
        this._hideDateTimePicker();
    };

    pad = (n) => {
        return (n < 10) ? ("0" + n) : n;
    }

    onHeightClick = () => {
        var selectedHeight = this.state.height.split('\'')
        console.log('selectedHeight', selectedHeight)
        var height1 = []
        for (i = 3; i <= 12; i++) {
            height1.push(i + "'")
        }

        var height2 = []
        for (i = 0; i <= 11; i += 1) {
            height2.push(i + "\"")
        }
        pickerData = [
            height1,
            height2
        ];
        console.log(selectedHeight[0] + "'", selectedHeight[1] + "\"")

        Picker.init({
            pickerData: pickerData,
            selectedValue: [selectedHeight[0] + "'", selectedHeight[1]],
            pickerTitleText: 'Height',
            pickerCancelBtnText: 'Cancel',
            pickerConfirmBtnText: 'Confirm',
            onPickerConfirm: data => {
                console.log(data);
                this.setState({ height: data.join('') })

            },
            onPickerCancel: data => {
                console.log(data);
            },
            onPickerSelect: data => {
                console.log(data);
            }
        });
        Picker.show();
    }


    onWeightClick = () => {
        var selectedWeight = this.state.weight.split('.')

        var weight1 = []
        for (i = 10; i <= 300; i++) {
            weight1.push(i)
        }

        var weight2 = []
        for (i = 0; i <= 90; i += 10) {
            weight2.push("." + this.pad(i))
        }
        pickerData = [
            weight1,
            weight2
        ];

        Picker.init({
            pickerData: pickerData,
            selectedValue: [selectedWeight[0], "." + selectedWeight[1]],
            pickerTitleText: 'Weight(LBS)',
            onPickerConfirm: data => {
                console.log(data.join(''));
                this.setState({ weight: data.join('') })
            },
            onPickerCancel: data => {
                console.log(data);
            },
            onPickerSelect: data => {
                console.log(data);
            }
        });
        Picker.show();
    }

    onGenderMaleClick = () => {
        this.setState({ gender: 1 })
    }

    onGenderFemaleClick = () => {
        this.setState({ gender: 2 })
    }

    validtion = () => {
        this.validate({
            name: { required: true },
            password: { minlength: 5, required: true },
            confirmpassword: { minlength: 5, required: true },
            email: { email: true },

        });
        console.log(this.state);
        if (this.isFieldInError('name')) {
            this.refs.toast.show(this.getErrorsInField('name'), 500, () => {
            });
            return false
        } else if (this.isFieldInError('email')) {
            this.refs.toast.show(this.getErrorsInField('email'), 500, () => {
            });
            return false
        } else if (this.isFieldInError('password')) {
            this.refs.toast.show(this.getErrorsInField('password'), 500, () => {
            });
            return false
        } else if (this.state.confirmpassword != this.state.password) {
            this.refs.toast.show("Confirm Password not match", 500, () => {
            });
            return false
        }
        return true
    }

    wscall = async () => {
        let httpMethod = 'POST'
        let strURL = editUserProfile
        let headers = {
            'Content-Type': 'application/json',
            'token': token,
            'userid': userid,
        }
        let params = {
            'name': this.state.name,
            'mobile': this.state.mobile,
            'DOB': this.state.DOB,
            'gender': this.state.gender,
            'height': this.state.height,
            'weight': this.state.weight,
        }
        console.log('call api');
        if (this.state.isPhotochange) {
            console.log('uploadPhoto');
            headers = {
                'Content-Type': 'multipart/form-data',
                'token': token,
                'userid': userid,
            }
            APICall.uploadPhoto('photo', this.state.selectedImageURI, 'image/jpg', 'userPhoto', strURL, headers, params, (resJSON) => {
                console.log('uploadPhoto -> ');
                console.log(resJSON)
                if (resJSON.status == 200) {
                    let data = resJSON.data
                    console.log(data);
                    Pref.setData(Pref.kUserInfo, data)
                    // Pref.setData(Pref.kUserId, data.id)
                    // Pref.setData(Pref.kaccessToken, data.token)
                    this.setState({ isEditable: !this.state.isEditable })
                    this.refs.toast.show("Save data sucessfully.", 500, () => {
                    });
                    return
                } else {
                    this.refs.toast.show(resJSON.message);
                }
            })
        } else {
            console.log('callWebService');
            APICall.callWebService(httpMethod, strURL, headers, params, (resJSON) => {
                console.log(resJSON)
                if (resJSON.status == 200) {
                    let data = resJSON.data
                    console.log(data);
                    Pref.setData(Pref.kUserInfo, data)
                    // Pref.setData(Pref.kUserId, data.id)
                    // Pref.setData(Pref.kaccessToken, data.token)

                    this.setState({ isEditable: !this.state.isEditable })
                    this.refs.toast.show("Save data sucessfully.", 500, () => {
                    });

                    return
                } else {
                    this.refs.toast.show(resJSON.message);
                }

            })
        }


    }

    onRedeem = () => {
        this.props.navigation.push('loyalityPoints', {
            page: 1
        })
    }

    onBack = () => {
        Alert.alert(
            "Are you sure you want to exit?",
            [
                { text: "Cancel", onPress: () => { }, style: "cancel" },
                { text: "Ok", onPress: () => BackHandler.exitApp() },
            ],
            { cancelable: false },
        );
        return true;
    };

    onloyalityClick = () => {
        console.log('onChangepasswordClick')
        this.props.navigation.push('loyalityPoints', {
            page: 1
        })
    }

    oninfoClick = () => {
        this.props.navigation.push("contentPage", {
            id: '6'
        })
    }

    render() {
        console.log(this.state.isLoading)
        if (this.state.isLoading) {
            return (<BackgroundImage></BackgroundImage>)
        } else {
            console.log('render', this.state)
            return (
                <HandleBack onBack={this.onBack}>
                    <Container flex-direction={"row"} style={{ flex: 1 }} >
                        <Header style={styles.headerAndroidnav}>
                            <StatusBar barStyle="light-content" backgroundColor="#051f3a" />
                            <Left style={{ alignSelf: 'center', justifyContent: 'center', flex: 0.5 }} >
                                {this.state.isEditable ?
                                    // <Button transparent style={{ height: 44 }} onPress={() => { this.onEditClick() }}><Text style={{ color: 'white' }}>Save</Text></Button>
                                    <Text style={{ color: 'white' }} onPress={() => { this.onEditClick() }}>Save</Text>
                                    :
                                    <Text style={{ color: 'white' }} onPress={() => { this.onEditClick() }}>Edit</Text>
                                }

                            </Left>
                            <Body style={{ flex: 1 }}>
                                <View style={{ alignSelf: 'center', justifyContent: 'center' }}>
                                    <Text style={styles.headerTitle}>Profile</Text>
                                </View>
                            </Body>
                            <Right style={{ alignSelf: 'center', justifyContent: 'flex-end', flex: 0.5 }}>
                                {/* <Button transparent  onPress={() => { this.onSettingClick() }}><Text style={{ color: 'white' }}>Settings</Text></Button> */}
                                <Text style={{ color: 'white' }} onPress={() => { this.onSettingClick() }}>Settings</Text>
                            </Right>
                        </Header>
                        <BackgroundImage >
                            <SafeAreaView />
                            <Content style={{}}>
                                <ToastSucess ref="toast"></ToastSucess>
                                <View style={{ alignItems: 'center', justifyContent: 'center', flex: 1, marginTop: 30, paddingBottom: 40, borderColor: '#193654', borderBottomWidth: 1 }}>
                                    <Button style={{ alignItems: 'center', justifyContent: 'center', width: '100%', height: 100, borderRadius: 50 }} disabled={!this.state.isEditable} transparent onPress={() => this.onClickSelectPhoto()}>
                                        {
                                            this.state.selectedImageURI == '' ?
                                                <Image source={user} style={{ width: 100, height: 100, borderRadius: 50 }} />
                                                :

                                                this.state.isPhotochange == true ?
                                                    <Image source={{ uri: this.state.selectedImageURI }} style={{ width: 100, height: 100, borderRadius: 50 }} />
                                                    :
                                                    <CacheableImage source={{ uri: this.state.selectedImageURI, cache: 'only-if-cached', }} style={{ width: 100, height: 100, borderRadius: 50 }} permanent={true}></CacheableImage>

                                        }
                                    </Button>
                                    <TextInput
                                        ref="name"
                                        onChangeText={(name) => this.setState({ name })} value={this.state.name}
                                        style={[styles.username, { marginLeft: 10, width: '95%', }]}
                                        placeholder="Full Name"
                                        placeholderTextColor="#95959a"
                                        editable={this.state.isEditable}
                                    />
                                    {
                                        is_premium == "1" ?
                                            <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center', marginTop: 5 }}>
                                                <Image source={premium} style={{ width: 24, height: 24, tintColor: "white", marginRight: 5 }} />
                                                <Text style={{ color: 'white' }}>Premium User</Text>
                                            </View>
                                            : null
                                    }
                                </View>
                                <ListItem style={{ paddingLeft: 0, marginLeft: 0, borderColor: '#193654' }}>
                                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', paddingLeft: 15, flexDirection: 'row', height: 45 }}>
                                        <Text style={styles.sectionHeadingWhiteText}>
                                            {this.state.points} Points
                                    </Text>
                                        <View style={{ justifyContent: 'flex-end', alignItems: 'flex-end', flex: 1,flexDirection:'row' }}>
                                            <Button style={{marginRight:10, borderRadius: 5, height: 30, borderRadius: 15, backgroundColor: '#2265B3', alignSelf: 'flex-end' }} onPress={() => { this.onloyalityClick() }}><Text style={{ fontSize: 12 }}>REDEEM</Text></Button>
                                            <Button transparent style={{borderRadius: 5, height: 30,  alignSelf: 'flex-end'  }} onPress={() => { this.oninfoClick() }}><Image source={info} style={{ width: 22, height: 22, }} /></Button>
                                        </View>
                                    </View>
                                </ListItem>
                                <ListItem style={{ paddingLeft: 0, marginLeft: 0, borderColor: '#193654' }}>
                                    <View style={{ flex: 1, justifyContent: 'center', paddingLeft: 15, flexDirection: 'row', alignItems: 'center' }}>
                                        <Text style={styles.sectionHeadingBlueText}>
                                            Email:
                                    </Text>
                                        <View style={{ justifyContent: 'flex-end', alignItems: 'center', flex: 1 }}>
                                            <TextInput
                                                ref="email"
                                                onChangeText={(email) => this.setState({ email })} value={this.state.email}
                                                style={[styles.textInput, { marginLeft: 10, alignSelf: 'flex-end', width: '95%', height: 45 }]}
                                                placeholder="Email Address"
                                                placeholderTextColor="#95959a"
                                                editable={false}
                                            />
                                        </View>
                                    </View>
                                </ListItem>
                                <ListItem style={{ paddingLeft: 0, marginLeft: 0, borderColor: '#193654' }}>
                                    <View style={{ flex: 1, justifyContent: 'center', paddingLeft: 15, flexDirection: 'row', alignItems: 'center' }}>
                                        <Text style={styles.sectionHeadingBlueText}>
                                            Mobile:
                                    </Text>
                                        <View style={{ justifyContent: 'flex-end', alignItems: 'center', flex: 1, padding: 0 }}>
                                            <TextInput
                                                ref="mobile"
                                                onChangeText={(mobile) => this.setState({ mobile })} value={this.state.mobile}
                                                style={[styles.textInput, { margin: 0, marginLeft: 10, width: '95%', alignSelf: 'flex-end', height: 45 }]}
                                                placeholder="Mobile Number"
                                                placeholderTextColor="#95959a"
                                                keyboardType="phone-pad"
                                                editable={this.state.isEditable}
                                            />
                                        </View>
                                    </View>
                                </ListItem>
                                <ListItem style={{ paddingLeft: 0, marginLeft: 0, borderColor: '#193654' }}>
                                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', paddingLeft: 15, flexDirection: 'row' }}>
                                        <Text style={styles.sectionHeadingBlueText}>
                                            DOB:
                                    </Text>
                                        <View style={{ justifyContent: 'flex-end', alignItems: 'center', flex: 1 }}>
                                            <TouchableOpacity transparent style={{ alignSelf: 'flex-end', justifyContent: 'center', flex: 1, width: '95%', height: 45 }} disabled={!this.state.isEditable} onPress={() => this._showDateTimePicker()}><Text style={[styles.textInput, { textAlign: 'right' }]}>{this.state.DOB}</Text></TouchableOpacity>
                                        </View>
                                    </View>
                                </ListItem>
                                <ListItem style={{ paddingLeft: 0, marginLeft: 0, borderColor: '#193654' }}>
                                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', paddingLeft: 15, flexDirection: 'row' }}>
                                        <Text style={styles.sectionHeadingBlueText}>
                                            Gender:
                                    </Text>
                                        <View style={{ justifyContent: 'flex-end', alignItems: 'center', flex: 1, flexDirection: 'row', height: 45 }}>
                                            <TouchableOpacity transparent style={{ alignSelf: 'flex-end' }} disabled={!this.state.isEditable} onPress={() => this.onGenderMaleClick()}>
                                                <View style={{ alignItems: 'center', justifyContent: 'flex-end', alignSelf: 'center', flex: 1, flexDirection: 'row' }}>
                                                    <Image source={this.state.gender == 1 ? checkbox_checked : checkbox} style={{ width: 20, height: 20 }} />
                                                    <Text style={{ marginLeft: 10, marginRight: 10, color: 'white' }}>Male</Text>
                                                </View>
                                            </TouchableOpacity>
                                            <TouchableOpacity transparent style={{ alignSelf: 'flex-end', }} disabled={!this.state.isEditable} onPress={() => this.onGenderFemaleClick()}>
                                                <View style={{ alignItems: 'center', alignSelf: 'flex-end', flex: 1, flexDirection: 'row' }}>
                                                    <Image source={this.state.gender == 2 ? checkbox_checked : checkbox} style={{ width: 20, height: 20 }} />
                                                    <Text style={{ marginLeft: 10, marginRight: 10, color: 'white' }}>Female</Text>
                                                </View>
                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                </ListItem>
                                <ListItem style={{ paddingLeft: 0, marginLeft: 0, borderColor: '#193654' }}>
                                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', paddingLeft: 15, flexDirection: 'row' }}>
                                        <Text style={styles.sectionHeadingBlueText}>
                                            Height:
                                    </Text>
                                        <View style={{ alignItems: 'center', justifyContent: 'center', flex: 1, flexDirection: 'row', height: 45 }}>
                                            <TouchableOpacity transparent style={{ alignSelf: 'center', justifyContent: 'flex-end', alignItems: 'center', flex: 1, width: '95%', flexDirection: 'row' }} disabled={!this.state.isEditable} onPress={() => this.onHeightClick()}><Text style={[styles.textInput, { textAlign: 'right' }]}>{this.state.height}</Text></TouchableOpacity>
                                            <Text style={[styles.textInput]}> </Text>
                                        </View>
                                    </View>
                                </ListItem>
                                <ListItem style={{ paddingLeft: 0, marginLeft: 0, borderColor: '#193654' }}>
                                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', paddingLeft: 15, flexDirection: 'row' }}>
                                        <Text style={styles.sectionHeadingBlueText}>
                                            Weight:
                                    </Text>
                                        <View style={{ alignItems: 'center', justifyContent: 'center', flex: 1, flexDirection: 'row', height: 45 }}>
                                            <TouchableOpacity transparent style={{ alignSelf: 'center', justifyContent: 'flex-end', alignItems: 'center', flex: 1, width: '95%', flexDirection: 'row' }} disabled={!this.state.isEditable} onPress={() => this.onWeightClick()}><Text style={[styles.textInput, { textAlign: 'right' }]}>{this.state.weight}</Text></TouchableOpacity>
                                            <Text style={[styles.textInput]}> Lbs</Text>
                                        </View>
                                    </View>
                                </ListItem>

                            </Content>
                        </BackgroundImage>
                        <DateTimePicker
                            isVisible={this.state.isDateTimePickerVisible}
                            onConfirm={this._handleDatePicked}
                            onCancel={this._hideDateTimePicker}
                            mode='date'
                            date={defaultdate}

                        />
                    </Container>
                </HandleBack>
            );
        }
    }

}

export default Profile;

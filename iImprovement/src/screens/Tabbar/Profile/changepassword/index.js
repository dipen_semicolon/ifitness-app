import React, { Component } from "react";
import { SafeAreaView, TextInput, Image, Dimensions, StatusBar, Platform, Alert } from "react-native";
import { Container, View, Text, Content, Button, Header, Left, Right } from "native-base";
import styles from "./style";
import ValidationComponent from 'react-native-form-validator';
import * as Pref from '../../../../Pref/Pref';
import ToastSucess from "../../../Default/ToastSucess"


const email = require("../../../../../assets/images/lock.png");
const backWhite = require("../../../../../assets/images/back.png");
const topIcon = require("../../../../../assets/images/topIcon.png");

class ChangePassword extends ValidationComponent {

  constructor(props) {
    super(props);
    this.state = { password: "", confirmpassword: "", loading: true };
  }

  static navigationOptions = ({ navigation }) => {
    theNavigation = navigation;

    return {
      header: (
        <Header style={styles.headerAndroidnav}>
          <StatusBar barStyle="light-content" backgroundColor="#051f3a" />
          <Left style={{ flex: 0.1, alignItems: 'center', justifyContent: 'center' }} >
            <Button transparent onPress={() => navigation.goBack()} style={{ alignSelf: 'center', width: 44, height: 44 }} >
              <Image source={backWhite} style={{ width: 22, height: 22, tintColor: "white" }} />
            </Button>
          </Left>
          <View style={{ alignSelf: 'center', justifyContent: 'center', flexDirection: 'row', flex: 1 }}>
            {/* <Text style={styles.headerTitle} >{theNavigation.getParam('title').toUpperCase()}</Text> */}
            <Text style={styles.headerTitle} >{'Change Password'.toUpperCase()}</Text>
          </View>
          <Right style={{ alignSelf: 'center', justifyContent: 'center', flex: 0.1 }}>
            {/* <Button transparent style={{ alignSelf: 'baseline', width: 44, height: 44 }} onPress={() => { subthis.toggleFilterModal() }}><Image source={filter} style={{ width: 22, height: 22, }} /></Button> */}
          </Right>
        </Header>
      )
    }

  }

  componentWillMount() {

  }

  onResetpasswordClick = () => {

    if (this.validtion()) {
      this.wscall();
    }
  }

  validtion = () => {
    this.validate({
      password: { minlength: 5, required: true },
      confirmpassword: { minlength: 5, required: true },
    });
    if (this.isFieldInError('password')) {
      this.refs.toast.show(this.getErrorsInField('password'), 500, () => {
      });
      return false
    } else if (this.isFieldInError('confirmpassword')) {
      this.refs.toast.show(this.getErrorsInField('confirmpassword'), 500, () => {
      });
      return false
    } else if (this.state.password != this.state.confirmpassword) {
      this.refs.toast.show("New password and confirm password not same.", 500, () => {
      });
      return false
    }
    return true
  }

  wscall = async () => {
    let httpMethod = 'POST'
    let strURL = changepassword
    let headers = {
      'Content-Type': 'application/json',
      'token': token,
      'userid': userid,

    }
    let params = {
      'password': this.state.password,
    }

    APICall.callWebService(httpMethod, strURL, headers, params, (resJSON) => {
      console.log('Login Screen -> ');
      console.log(resJSON)
      if (resJSON.status == 200) {

        Alert.alert(
          'Change Password',
          'Your password is Change.',
          [
            { text: 'Ok', onPress: () => this.props.navigation.goBack() },
          ],
          { cancelable: false }
        )
        return
      } else {
        this.refs.toast.show(resJSON.message);
      }
    })
  }


  render() {

    return (
      <Container flex-direction={"row"} style={{ flex: 1 }} >
        <Toastt ref="toast"></Toastt>
        <ToastSucess ref="toastsucess"></ToastSucess>

        <BackgroundImage >
          <SafeAreaView />
          <Content style={{ paddingLeft: 33, paddingRight: 33 }}>
            <View>
              <View style={[styles.textInputView, { marginTop: 57, flexDirection: 'row', alignItems: 'center' }]}>
                <Image source={email} style={{ height: 22, width: 16, marginLeft: 20 }} />
                <TextInput
                  ref="password"
                  onChangeText={(password) => this.setState({ password })} value={this.state.password}
                  style={[styles.textInput, { marginLeft: 13, width: '80%', }]}
                  placeholder="New Password"
                  placeholderTextColor="#95959a"
                  autoCorrect={false}
                  secureTextEntry={true}
                />
              </View>
              <View style={[styles.textInputView, { marginTop: 10, flexDirection: 'row', alignItems: 'center' }]}>
                <Image source={email} style={{ height: 22, width: 16, marginLeft: 20 }} />
                <TextInput
                  ref="confirmpassword"
                  onChangeText={(confirmpassword) => this.setState({ confirmpassword })} value={this.state.confirmpassword}
                  style={[styles.textInput, { marginLeft: 13, width: '80%', }]}
                  placeholder="Confirm Password"
                  placeholderTextColor="#95959a"
                  autoCorrect={false}
                  secureTextEntry={true}
                />
              </View>
              <View style={{ justifyContent: 'center', alignItems: 'center', flex: 1, marginTop: 40 }}>
                <Button style={{ flex:1, height: 50, borderRadius: 30, backgroundColor: '#2265B3', paddingLeft: 30, paddingRight: 30, alignSelf: 'center' }} onPress={() => this.onResetpasswordClick()} ><Text style={{ fontSize: 20 }}>RESET PASSWORD</Text>
                </Button>
              </View>

            </View>
          </Content>
        </BackgroundImage>
      </Container>
    );
  }
}

export default ChangePassword;

const React = require("react-native");
const { Dimensions, Platform } = React;
let deviceWidth = Dimensions.get('window').width

export default {
  headerAndroidnav: {
    backgroundColor: '#2265B3',
    shadowOpacity: 0,
    borderBottomWidth: 0,
    height: (deviceWidth * 64) / 320,
  },
  headerTitle: {
    fontFamily: font_SFProDisplay_Bold,
    color: 'white',
    textAlign: 'center'
  },
  imgView: {
    width: deviceWidth * 0.26,
    height: deviceWidth * 0.26,
    borderRadius: deviceWidth * 0.13,
  },
  activityIndicatorStyle: {
    width: 100,
    height: 100,
    backgroundColor: 'transparent'
  },
  username: {
    fontFamily: font_SFProDisplay_Bold,
    color: 'white',
    textAlign: 'center',
    fontSize: 22,
    marginTop: 8,
  },
  sectionHeadingWhiteText: {
    fontFamily: font_MyriadPro_Regular,
    color: 'white',
    fontSize: 18,
  },
  sectionHeadingBlueText: {
    fontFamily: font_MyriadPro_Regular,
    color: '#164B87',
    marginTop:5,
    fontSize: 16,
  }, 
  textInput: {
    color: 'white',
    fontSize: 16,
    fontFamily: font_SFProDisplay_Regular,
    textAlign:'right',
  },

};

import React, { Component } from "react";
import { SafeAreaView, TextInput, Image, Dimensions, StatusBar, ActivityIndicator, FlatList } from "react-native";
import { Container, View, Text, Content, Button, Header, Left, Right, Textarea, } from "native-base";
import styles from "./style";
import ValidationComponent from 'react-native-form-validator';
import * as Pref from '../../../../Pref/Pref';
import ToastSucess from "../../../Default/ToastSucess"
import imageCacheHoc from 'react-native-image-cache-hoc';


const email = require("../../../../../assets/images/lock.png");
const backWhite = require("../../../../../assets/images/back.png");
const award = require("../../../../../assets/images/award.png");
const info = require("../../../../../assets/images/info.png");
const CacheableImage = imageCacheHoc(Image, {
    fileHostWhitelist: ['100.25.250.56'],
    validProtocols: ['http', 'https'],
    defaultPlaceholder: {
        component: ActivityIndicator,
        props: {
            style: styles.activityIndicatorStyle
        }
    }
});
var subthis;
var userinfoObj;
class pointsList extends ValidationComponent {

    constructor(props) {
        super(props);
        subthis = this
        this.state = { page: 0, points: 0, Data: [] };
    }

    oninfoClick = () => {
        this.props.navigation.push("contentPage", {
            id: '6'
        })
    }

    onpointClick = () => {
        this.setState({ page: 0 })
    }

    onredeeemClick = () => {
        this.setState({ page: 1 })
        if (this.state.Data.length >= 1) {
            this.wscallBG()
        } else {
            this.wscall()
        }
    }

    static navigationOptions = ({ navigation }) => {
        theNavigation = navigation;
        return {
            header: (
                <Header style={styles.headerAndroidnav}>
                    <StatusBar barStyle="light-content" backgroundColor="#051f3a" />
                    <Left style={{ flex: 0.1, alignItems: 'center', justifyContent: 'center' }} >
                        <Button transparent onPress={() => navigation.goBack()} style={{ alignSelf: 'center', width: 44, height: 44 }} >
                            <Image source={backWhite} style={{ width: 22, height: 22, tintColor: "white" }} />
                        </Button>
                    </Left>
                    <View style={{ alignSelf: 'center', justifyContent: 'center', flexDirection: 'row', flex: 1 }}>
                        <Text style={styles.headerTitle} >{'Loyalty Points'.toUpperCase()}</Text>
                    </View>
                    <Right style={{ alignSelf: 'center', justifyContent: 'center', flex: 0.1 }}>
                        <Button transparent style={{ alignSelf: 'baseline', width: 44, height: 44, marginRight: 10 }} onPress={() => { subthis.oninfoClick() }}><Image source={info} style={{ width: 22, height: 22, }} /></Button>
                    </Right>
                </Header>
            )
        }

    }

    componentWillMount() {
        Pref.getData(Pref.kUserInfo, (userinfo) => {
            var obj = userinfo;
            userinfoObj = userinfo
            console.log(obj);
            this.setState({ points: obj.points })
        })
    }
    componentDidMount(){
        console.log('theNavigation1',theNavigation.getParam('page'))
        if(theNavigation.getParam('page') && (theNavigation.getParam('page') == 1)){
            subthis.onredeeemClick()
        }
    }

    wscallBG = async () => {
        let httpMethod = 'POST'
        let strURL = redemList
        let headers = {
            'Content-Type': 'application/json',
            'token': token,
            'userid': userid,
        }
        let params = {
        }

        APICall.callWebServiceBackground(httpMethod, strURL, headers, params, (resJSON) => {
            console.log('Login Screen -> ');
            console.log(resJSON)
            if (resJSON.status == 200) {
                this.setState({
                    Data: resJSON.data,
                    page: 1
                })
                return
            } else {
                this.refs.toast.show(resJSON.message);
            }
        })
    }

    wscall = async () => {
        let httpMethod = 'POST'
        let strURL = redemList
        let headers = {
            'Content-Type': 'application/json',
            'token': token,
            'userid': userid,
        }
        let params = {
        }

        APICall.callWebService(httpMethod, strURL, headers, params, (resJSON) => {
            console.log('Login Screen -> ');
            console.log(resJSON)
            if (resJSON.status == 200) {
                this.setState({
                    Data: resJSON.data,
                    page: 1
                })
                return
            } else {
                this.refs.toast.show(resJSON.message);
            }
        })
    }

    wscallRedeem = async (id, points, index) => {
        let httpMethod = 'POST'
        let strURL = redeemed
        let headers = {
            'Content-Type': 'application/json',
            'token': token,
            'userid': userid,
        }
        let params = {
            'id': id,
            'points': points,
        }



        APICall.callWebServiceBackground(httpMethod, strURL, headers, params, (resJSON) => {
            console.log('wsFavcall Screen -> ');
            console.log(resJSON)
            if (resJSON.status == 200) {
                fulldata = this.state.Data
                newData = fulldata[index]
                receipeData = newData[index]
                newData.is_redeem = true
                fulldata[index] = newData
                this.setState({ Data: fulldata })
                this.refs.toast.show('Your redeem sucessfull.');
                userinfoObj.points = userinfoObj.points - newData.points
                this.setState({ points: userinfoObj.points })
                Pref.setData(Pref.kUserInfo, userinfoObj)

                return
            } else {
                this.refs.toast.show(resJSON.message);
            }

        })
    }
    render() {
        console.log(this.state)
        return (
            <Container flex-direction={"row"} style={{ flex: 1 }} >
                <Toastt ref="toast"></Toastt>
                <ToastSucess ref="toastsucess"></ToastSucess>

                <BackgroundImage >
                    <SafeAreaView />
                    {
                        this.state.page == 0 ?
                            <Content style={{ paddingLeft: 33, paddingRight: 33 }}>
                                <Text style={styles.earnTitle}>{'you have earned'.toUpperCase()}</Text>
                                <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                                    <View style={{ backgroundColor: '#2265B3', alignItems: 'center', justifyContent: 'center', margin: 30, padding: 15, height: 100, borderRadius: 50, width: 100 }}>
                                        <Text style={{ color: 'white', fontSize: 35, fontFamily: font_SFProDisplay_Regular }}>{this.state.points}</Text>
                                        <Text>Points</Text>
                                    </View>
                                </View>
                                <View style={{ alignItems: 'center', justifyContent: 'center', marginTop: 20 }}>
                                    <Image source={award} styl={{ width: 180, height: 180, alignItems: 'center', alignSelf: 'center', tintColor: 'white' }} />
                                </View>
                            </Content>
                            :
                            <Content>
                                <FlatList
                                    style={{ flex: 1, borderTopColor: '#193654', borderTopWidth: 2, }}
                                    data={this.state.Data}
                                    renderItem={
                                        ({ item, index }) => this.loadTableCells(item, index)
                                    }
                                    keyExtractor={(index) => index.toString()}
                                />
                            </Content>
                    }

                    <View style={{ flexDirection: 'row' }}>
                        <Button style={[styles.bottomViewButton, { backgroundColor: '#091A2E', borderRadius: 0, flex: 0.5 }]}
                            onPress={() => this.onpointClick()}
                        >
                            <Text style={{ color: 'white' }}>{'Points'.toUpperCase()}</Text>
                        </Button>
                        <Button style={[styles.bottomViewButton, { backgroundColor: '#0D3058', borderRadius: 0, flex: 0.5 }]}
                            onPress={() => this.onredeeemClick()}
                        >
                            <Text style={{ color: 'white' }}>{'redeem'.toUpperCase()}</Text>
                        </Button>
                    </View>
                </BackgroundImage>
            </Container>
        );
    }

    loadTableCells(item, index) {
        return (this.cellVideoList(item, index));
    }


    cellVideoList = (facet, rowID) => {
        sectionID = 0;
        if (this.state.points >= facet.points) {
            return (
                <View style={{ flexDirection: 'row', borderBottomColor: '#193654', borderBottomWidth: 2, paddingBottom: 10, flex: 1, paddingLeft: 20, paddingRight: 20, marginTop: 10, }}>
                    <View style={{ justifyContent: 'center', alignItems: 'center', width: 80, height: 60 }}>
                        {
                            facet.photo.length >= 1 ?
                                <CacheableImage source={{ uri: facet.photo, cache: 'only-if-cached', }} style={{ width: 80, height: 60 }} permanent={true}>
                                </CacheableImage>
                                :
                                <CacheableImage source={{ uri: 'http://100.25.250.56:8081/images/internet.png', cache: 'only-if-cached', }} style={{ width: 80, height: 60, resizeMode: 'contain' }} permanent={true}>
                                </CacheableImage>
                        }
                    </View>
                    <View style={{ flex: 1, paddingLeft: 10, }}>
                        <Text style={styles.videoTitle}>{facet.title}</Text>
                        <Text style={styles.pointTitle}>{facet.points} Points</Text>
                    </View>
                    <View style={{ justifyContent: 'center', alignItems: 'flex-end', flex: 1 }}>
                        {facet.is_redeem == 1 ?
                            <Button disabled style={{ borderRadius: 5, height: 25, borderRadius: 15, backgroundColor: '#2265B3', alignSelf: 'flex-end' }}>
                                <Text style={{ fontSize: 12 }}>REDEEMED</Text>
                            </Button>
                            :
                            <Button style={{ borderRadius: 5, height: 25, borderRadius: 15, backgroundColor: '#2265B3', alignSelf: 'flex-end' }} onPress={() => { this.wscallRedeem(facet.id, facet.points, rowID) }}>
                                <Text style={{ fontSize: 12 }}>REDEEM</Text>
                            </Button>
                        }
                    </View>

                </View>
            );
        } else {
            return (
                <View>
                    <View style={{ flexDirection: 'row', borderBottomColor: '#193654', borderBottomWidth: 2, paddingBottom: 10, flex: 1, paddingLeft: 20, paddingRight: 20, marginTop: 10, }}>
                        <View style={{ justifyContent: 'center', alignItems: 'center', width: 80, height: 60 }}>
                            {
                                facet.photo.length >= 1 ?
                                    <CacheableImage source={{ uri: facet.photo, cache: 'only-if-cached', }} style={{ width: 80, height: 60,  }} permanent={true}>
                                    </CacheableImage>
                                    :
                                    <CacheableImage source={{ uri: 'http://100.25.250.56:8081/images/internet.png', cache: 'only-if-cached', }} style={{ width: 80, height: 60, resizeMode: 'contain', }} permanent={true}>
                                    </CacheableImage>
                            }
                        </View>
                        <View style={{ flex: 1, paddingLeft: 10, }}>
                            <Text style={styles.videoTitle}>{facet.title}</Text>
                            <Text style={styles.pointTitle}>{facet.points} Points</Text>
                        </View>
                        {facet.is_redeem == 1 ?
                            <View style={{ justifyContent: 'center', alignItems: 'flex-end', flex: 1 }}>
                                <Button disabled style={{ borderRadius: 5, height: 25, borderRadius: 15, backgroundColor: '#2265B3', alignSelf: 'flex-end' }}>
                                    <Text style={{ fontSize: 12 }}>REDEEMED</Text>
                                </Button>
                            </View>
                            : null
                        }

                    </View>
                </View>

            );
        }

    }

}

export default pointsList;

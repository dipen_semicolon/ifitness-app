const React = require("react-native");
const { Dimensions, Platform } = React;
const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;

export default {

    headerAndroidnav: {
        backgroundColor: '#2265B3',
        shadowOpacity: 0,
        borderBottomWidth: 0,
        height: (deviceWidth * 64) / 320,
    },
    headerTitle: {
        fontFamily: font_SFProDisplay_Bold,
        color: 'white',
        textAlign: 'center'
    },
    bottomViewButton: {
        height: 50,
        width: '100%',
        justifyContent: "center",
        alignSelf: "stretch",
        textAlignVertical: "center",
        fontFamily: font_SFProDisplay_Medium,
        fontSize: 20,
    },
    earnTitle: {
        fontFamily: font_SFProDisplay_Regular,
        color: 'white',
        textAlign: 'center',
        fontSize: 25,
        marginTop: 100,
    },
    activityIndicatorStyle: {
        width: 80,
        height: 60,
        backgroundColor: 'transparent'
    },
    videoTitle: {
        color: '#8CA9C7',
        fontSize: 18,
        marginBottom:8
    },
    pointTitle: {
        color: 'white',
        fontSize: 18,
    },
    overlay: {
        flex: 1,
        position: 'absolute',
        left: 0,
        top: 0,
        opacity: 0.5,
        backgroundColor: 'rgba(0, 0, 0, 0.2)',
        width: '100%'
      }
};

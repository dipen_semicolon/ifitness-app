import React, { Component } from 'react';
import { Image, ImageBackground } from "react-native";
import { createStackNavigator, TabNavigator } from 'react-navigation';
import Profile from './myProfile'
import Setting from './setting'
import changepassword from './changepassword'
import contactus from './contactus'
import loyalityPoints from './loyalityPoints'
import contentPage from '../../contentPage'


import {
  Container, Header, Left, Body, Right, Button, Icon, Title, Text, View
} from "native-base";


const RootStack = createStackNavigator(
  {
    Profile: {
      screen: Profile, navigationOptions: () => ({
        header:null,
      })
    },
    Setting: {
      screen: Setting, navigationOptions: () => ({
      })
    },
    changepassword: {
      screen: changepassword, navigationOptions: () => ({
      })
    },
    contactus: {
      screen: contactus, navigationOptions: () => ({
      })
    },
    loyalityPoints: {
      screen: loyalityPoints, navigationOptions: () => ({
      })
    },
    contentPage: {
      screen: contentPage, navigationOptions: () => ({
        header:null,
      })
    },
  },
  {
    initialRouteName: 'Profile',
    headerMode: 'screen',
    orientation: 'portrait',
    navBarBackgroundColor: 'transparent',
    headerStyle: {
    },
  },

);

RootStack.navigationOptions = ({ navigation }) => {

  let tabBarVisible = true;
  if (navigation.state.index > 0) {
      tabBarVisible = false
  }
  return {
      tabBarVisible,
  };

};
export default RootStack;

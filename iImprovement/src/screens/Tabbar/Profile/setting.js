import React, { Component } from "react";
import { SafeAreaView, TextInput, alert, Dimensions, StatusBar, Image, TouchableOpacity, Linking, Share, Alert } from "react-native";
import { Container, View, Text, Content, Button, Header, Left, Right, List, ListItem, Icon } from "native-base";
import styles from "./style";
import ValidationComponent from 'react-native-form-validator';
import * as RNIap from 'react-native-iap';
import * as Pref from '../../../Pref/Pref';
import RNProgressHUB from 'react-native-progresshub';
import Modal from "react-native-modal";
import InAppPurchase from "../../inapp"

const backWhite = require("../../../../assets/images/back.png");
const rightArrow = require("../../../../assets/images/right-arrow.png");
const premium = require("../../../../assets/images/protection.png");

var subthis;
class Setting extends ValidationComponent {

    constructor(props) {
        super(props);
        subthis = this
        this.state = {
            code: '',
            isModelVisible: false,
        };        
    }

    componentDidMount() {
        //CacheableImage.flush()
        Pref.getData(Pref.kUserInfo, (userinfo) => {
            var obj = userinfo;
            this.setState({
                code: obj.code,

            })
        })
    }



    wscall = async () => {
        let httpMethod = 'POST'
        let strURL = userlogout
        let headers = {
            'Content-Type': 'application/json',
            'token': token,
            'userid': userid,
        }
        let params = {
        }

        console.log('callWebService');
        APICall.callWebService(httpMethod, strURL, headers, params, (resJSON) => {
            console.log(resJSON)
            if (resJSON.status == 200) {
                Pref.setData(Pref.kUserId, 0)
                is_premium = '0'
                this.props.navigation.push('Home')
                return
            } else {
                this.refs.toast.show(resJSON.message);
            }
        })
    }

    onLogoutClick = () => {
        Pref.setData(Pref.kIsInAppShow,null);
        this.wscall()
    }





    onChangepasswordClick = () => {
        console.log('onChangepasswordClick')
        this.props.navigation.push('changepassword')
    }

    onReferFrined = () => {

        Share.share({
            message: 'Sign up using my Code ' + this.state.code + ' on iImprove Fitness App and get 30 reward points. Download the app using- ',
            url: 'https://itunes.apple.com/in/app/whatsapp-messenger/id310633997?mt=8',
            title: 'Refer more and reward more!'
        }, {
                // Android only:
                dialogTitle: 'Refer more and reward more!',
                subject: 'Refer more and reward more!',
                // iOS only:
                // excludedActivityTypes: [
                //     'com.apple.UIKit.activity.PostToTwitter'
                // ]
            })
    }

    onShopClick = () => {
        console.log('Open shop link.')
        Linking.openURL("http://iimprove.net/");
    }

    oncontactusClick = () => {
        console.log('oncontactusClick')
        this.props.navigation.push('contactus')
    }

    onRestoreClick = async () => {
        console.log('restore click');
        try {
            RNProgressHUB.showSpinIndeterminate();
            const purchases = await RNIap.getAvailablePurchases();
            console.log(purchases)
            if (purchases.length >= 1) {
                this.checkReceiptWithServer()
            } else {
                Alert.alert('Restore Fail', 'There is no previous purchase found for given account');

            }
            RNProgressHUB.dismiss();
        } catch (err) {
            RNProgressHUB.dismiss();
            console.warn(err); // standardized err.code and err.message available
            Alert.alert(err.message);
        }
    }



    checkReceiptWithServer = async () => {
        let httpMethod = 'POST'
        let strURL = checkReceipt
        let headers = {
            'Content-Type': 'application/json',
            'token': token,
            'userid': userid,
        }
        let params = {
        }

        console.log('callWebService');
        APICall.callWebService(httpMethod, strURL, headers, params, (resJSON) => {
            console.log(resJSON)
            if (resJSON.status == 200) {
                is_premium = "1"
                Pref.getData(Pref.kUserInfo, (userinfo) => {
                    var obj = userinfo;
                    is_premium = '1'
                    Pref.setData(Pref.kUserInfo, obj)
                    Alert.alert('Restore Successfully', 'Successfully restored your previous purchases, Now you can enjoy unlimited access of Workout videos and Nutrition recipes.');
                    this.props.navigation.setParams({ "reload": "1" });
                })
                return
            } else {
                Alert.alert('Restore Fail', 'your last is purchase expire.');
                // this.refs.toast.show(resJSON.message);
            }
        })
    }
    static navigationOptions = ({ navigation }) => {
        theNavigation = navigation;

        return {
            header: (
                <Header style={styles.headerAndroidnav}>
                    <StatusBar barStyle="light-content" backgroundColor="#051f3a" />
                    <Left style={{ flex: 0.1, alignItems: 'center', justifyContent: 'center' }} >
                        <Button transparent onPress={() => {
                         navigation.goBack()
                         navigation.state.params.onGoBack()
                        }} style={{ alignSelf: 'center', width: 44, height: 44 }} >
                            <Image source={backWhite} style={{ width: 22, height: 22, tintColor: "white" }} />
                        </Button>
                    </Left>
                    <View style={{ alignSelf: 'center', justifyContent: 'center', flexDirection: 'row', flex: 1 }}>
                        {/* <Text style={styles.headerTitle} >{theNavigation.getParam('title').toUpperCase()}</Text> */}
                        <Text style={styles.headerTitle} >{'Settings'.toUpperCase()}</Text>
                    </View>
                    <Right style={{ alignSelf: 'center', justifyContent: 'center', flex: 0.1 }}>
                        {
                            is_premium == "0" ?
                                <Button transparent onPress={() => {
                                    subthis.setState({ isModelVisible: true })
                                }}
                                    style={{ alignSelf: 'center', width: 44, height: 44, marginRight: 25, }} >
                                    <Image source={premium} style={{ width: 32, height: 32, tintColor: "white" }} />
                                </Button>
                                : null
                        }
                    </Right>
                </Header>
            )
        }
    }


    refresh() {
        console.log('refresh call')
        Pref.getData(Pref.kUserInfo, (userinfo) => {
            var obj = userinfo;
            this.setState({
                code: obj.code,
            })
            this.props.navigation.setParams({ "reload": "1" });
        })

    }

    togglePopupPromocode = () => {
        this.setState({ isModelVisible: !this.state.isModelVisible });
        this.refresh()
    }

    onloyalityClick = () => {
        this.props.navigation.push('loyalityPoints', {
            page: 0
        })
    }

    render() {
        console.log('render', this.state)
        return (
            <Container flex-direction={"row"} style={{ flex: 1 }} >
                <Toastt ref="toast"></Toastt>

                <BackgroundImage >
                    <SafeAreaView />
                    <Content style={{}}>
                        <List>
                            <ListItem style={{ paddingLeft: 0, marginLeft: 0, height: 50, borderBottomColor: '#193654' }}>
                                <TouchableOpacity onPress={() => this.onloyalityClick()} style={{ flexDirection: 'row' }}>
                                    <Left >
                                        <Text style={{ color: 'white', marginLeft: 15 }}>Loyalty Points</Text>
                                    </Left>
                                    <Right>
                                        <Image source={rightArrow} style={{ width: 24, height: 24 }} />
                                    </Right>
                                </TouchableOpacity>
                            </ListItem>
                            <ListItem style={{ paddingLeft: 0, marginLeft: 0, height: 50, borderBottomColor: '#193654' }}>
                                <TouchableOpacity onPress={() => this.onChangepasswordClick()} style={{ flexDirection: 'row' }}>
                                    <Left >
                                        <Text style={{ color: 'white', marginLeft: 15 }}>Change Password</Text>
                                    </Left>
                                    <Right>
                                        <Image source={rightArrow} style={{ width: 24, height: 24 }} />
                                    </Right>
                                </TouchableOpacity>
                            </ListItem>
                            <ListItem style={{ paddingLeft: 0, marginLeft: 0, height: 50, borderBottomColor: '#193654' }}>
                                <TouchableOpacity onPress={() => this.onReferFrined()} style={{ flexDirection: 'row' }}>
                                    <Left >
                                        <Text style={{ color: 'white', marginLeft: 15 }}>Refer a friend</Text>
                                    </Left>
                                    <Right>
                                        <Image source={rightArrow} style={{ width: 24, height: 24 }} />
                                    </Right>
                                </TouchableOpacity>
                            </ListItem>
                            <ListItem style={{ paddingLeft: 0, marginLeft: 0, height: 50, borderBottomColor: '#193654' }} >
                                <TouchableOpacity onPress={() => this.onShopClick()} style={{ flexDirection: 'row' }}>
                                    <Left >
                                        <Text style={{ color: 'white', marginLeft: 15 }}>iImprove Shops</Text>
                                    </Left>
                                    <Right >
                                        <Image source={rightArrow} style={{ width: 24, height: 24 }} />
                                    </Right>
                                </TouchableOpacity>
                            </ListItem>
                            <ListItem style={{ paddingLeft: 0, marginLeft: 0, height: 50, borderBottomColor: '#193654' }}>
                                <TouchableOpacity onPress={() => this.oncontactusClick()} style={{ flexDirection: 'row' }}>
                                    <Left >
                                        <Text style={{ color: 'white', marginLeft: 15 }}>Contact a trainer</Text>
                                    </Left>
                                    <Right>
                                        <Image source={rightArrow} style={{ width: 24, height: 24 }} />
                                    </Right>
                                </TouchableOpacity>
                            </ListItem>
                            {
                                is_premium == "1" ? null :
                                    <ListItem style={{ paddingLeft: 0, marginLeft: 0, borderColor: '#193654' }}>
                                        <View style={{ flex: 1, justifyContent: 'center', paddingLeft: 15, flexDirection: 'row' }}>
                                            <Text style={styles.sectionHeadingWhiteText}>
                                                Previous Purchases
                                    </Text>
                                            <View style={{ justifyContent: 'flex-end', alignItems: 'flex-end', flex: 1 }}><Button style={{ borderRadius: 5, height: 30, borderRadius: 15, backgroundColor: '#2265B3', alignSelf: 'flex-end' }} onPress={() => { this.onRestoreClick() }}><Text style={{ fontSize: 12 }}>RESTORE</Text></Button></View>
                                        </View>
                                    </ListItem>
                            }
                        </List>
                        <View style={{ justifyContent: 'center', alignItems: 'center', flex: 1, marginTop: 60 }}>
                            <Button style={{ height: 50, borderRadius: 30, backgroundColor: '#2265B3', paddingLeft: 30, paddingRight: 30, alignSelf: 'center' }} onPress={() => this.onLogoutClick()}><Text style={{ fontSize: 20 }}>LOGOUT</Text>
                            </Button>
                        </View>
                    </Content>
                    <Modal isVisible={this.state.isModelVisible} style={{ margin: 0, padding: 0 }} >
                        <InAppPurchase
                            hidePopup={() => this.togglePopupPromocode()}
                            closeBtnPressed={() => this.togglePopupPromocode()}
                        />
                    </Modal>
                </BackgroundImage>

            </Container>
        );
    }


}

export default Setting;

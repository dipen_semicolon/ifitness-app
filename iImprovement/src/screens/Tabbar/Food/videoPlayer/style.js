const React = require("react-native");
const { Platform, Dimensions } = React;

const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;

export default {
    headerAndroidnav: {
        backgroundColor: '#2265B3',
        shadowOpacity: 0,
        borderBottomWidth: 0,
        height: (deviceWidth * 64) / 320,
    },
    headerTitle: {
        fontFamily: font_SFProDisplay_Bold,
        color: 'white',
        textAlign: 'center'
    },
    viewBase: {
        marginTop:30,
        alignItems:'center',
        justifyContent: 'center',
    },
    sectionHeadingText: {
        fontSize: 14,
        color: 'white',
        fontFamily: font_MyriadPro_Regular,
    },
    contentText: {
        fontFamily: font_MyriadPro_Regular,
        fontSize: 14,
        color: 'white',
        marginLeft: 15,
        width: '85%'
    },

    Cell_Image: {
        marginLeft: 5,
    },
    btnNext: {
        alignSelf: 'center',
        justifyContent: 'center',
        width: (deviceWidth * 0.55),
        marginTop: 15,
        marginBottom: 15,
        backgroundColor: '#0F66C7',
    },

}
import React, { Component } from "react";
import { Image, ScrollView, SafeAreaView, TouchableOpacity, Dimensions } from "react-native";
import { Container, View, Text, Button, Header, Left, Right, ListItem, } from "native-base";
import styles from "./style";
const close = require("../../../../../assets/images/close.png");
//import Player from 'react-native-video-player';
import Video from 'react-native-af-video-player'

let deviceWidth = Dimensions.get('window').width
let deviceHeight = Dimensions.get('window').height

class VideoPlayer extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
        }
    }
    static navigationOptions = ({ navigation }) => {
        theNavigation = navigation;
    }

    componentDidMount() {
        this.setState({
            isLoading: false,
        })
    }

    render() {
        console.log('render', this.state)
        if (this.state.isLoading) {
            return (<BackgroundImage></BackgroundImage>)
        } else {
            return (
                <Container>
                    <BackgroundImage>
                        <SafeAreaView />
                        <View style={{ flex: 1 }}>
                            {/* <Player
                                endWithThumbnail
                                thumbnail={{ uri: theNavigation.getParam('images') }}
                                video={{ uri: theNavigation.getParam('video') }}
                                duration={0}
                                disableControlsAutoHide={true}
                                ref={r => this.player = r}
                                videoWidth={deviceWidth}
                                videoHeight={deviceHeight - 20}
                                autoplay={true}
                            /> */}

                            <Video url={theNavigation.getParam('video')}
                                fullScreenOnly
                                autoPlay
                            />
                        </View>
                        <View style={{ position: 'absolute', marginTop: 20 }}>
                            <Button transparent onPress={() => { theNavigation.goBack() }}>
                                <Image source={close}></Image>
                            </Button>
                        </View>

                    </BackgroundImage>
                </Container>
            );
        }
    }

}

export default VideoPlayer;

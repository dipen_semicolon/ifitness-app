import React, { Component } from "react";
import { SafeAreaView, Image, ScrollView, TouchableHighlight, ImageBackground, Dimensions, StatusBar } from "react-native";
import { Container, View, Text, Content, Button, Header, Left, Right } from "native-base";
import styles from "./style";
import ValidationComponent from 'react-native-form-validator';
import HTMLView from 'react-native-htmlview';
import PageControl from 'react-native-page-control';
//import ImageSlider from 'react-native-image-slider';
import ImageSlider from 'react-native-image-slider';

import Modal from "react-native-modal";
import VideoPlayer from "../videoPlayer";


const backWhite = require("../../../../../assets/images/back.png");
const fav = require("../../../../../assets/images/fav.png");
const fav_selected = require("../../../../../assets/images/fav_selected.png");

const clock = require("../../../../../assets/images/clock.png");
const serving = require("../../../../../assets/images/serving.png");
const food_logo_header = require("../../../../../assets/images/food_logo_header.png");


var theNavigation;
var subthis;
let deviceWidth = Dimensions.get('window').width

class FoodDetail extends ValidationComponent {

    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            page: 0,
            image: [],
            isFilterVisible: false,
            FromFavorite: false,
            index: -1,

        };
        subthis = this
    }

    static navigationOptions = ({ navigation }) => {
        theNavigation = navigation;
        data = theNavigation.getParam('data')
        console.log('navigation data', data);
        return {
            header: (
                <Header style={styles.headerAndroidnav}>
                    <StatusBar barStyle="light-content" backgroundColor="#051f3a" />
                    <Left style={{ flex: 0.1,  }} >
                        <Button transparent onPress={() => navigation.goBack()} style={{ alignSelf: 'center', width: 44, height: 44 }} >
                            <Image source={backWhite} style={{ width: 22, height: 22, tintColor: "white" }} />
                        </Button>
                    </Left>
                    <View style={{ alignSelf: 'center', justifyContent: 'center', flexDirection: 'row', flex: 1 }}>
                    <Image source={food_logo_header} style= {{width:150,height:76}} />
                    </View>
                    <Right style={{ alignItems: 'center', justifyContent: 'center', flex: 0.1 }}>
                        <Button transparent style={{ alignSelf: 'baseline', width: 44, height: 44 }} onPress={() => { subthis.wscall(data.id, data.fav_status == 1 ? 0 : 1) }}><Image source={data.fav_status == 1 ? fav_selected : fav} style={{ width: 22, height: 22, tintColor: 'white' }} /></Button>
                    </Right>
                </Header>
            )
        }

    }


    componentDidMount() {

        var image = []
        theNavigation.getParam('data').images.map((img, i) => {
            image.push(img.photo)
        })

        if (theNavigation.getParam('data').images.length == 0) {
            image.push('http://100.25.250.56:8081/images/food_placeholder.png')
        }

        this.setState({
            data: theNavigation.getParam('data'),
            title: theNavigation.getParam('title'),
            isLoading: false,
            images: image,
            FromFavorite: theNavigation.getParam('FromFavorite') != undefined ? theNavigation.getParam('FromFavorite') : false,
            index: theNavigation.getParam('index') != undefined ? theNavigation.getParam('index') : -1,


        })
        //this.wscall(theNavigation.getParam('selectedId'))
    }


    wscall = async (receipe_id, status) => {
        let httpMethod = 'POST'
        let strURL = favReceipe
        let headers = {
            'Content-Type': 'application/json',
            'token': token,
            'userid': userid,
        }
        let params = {
            'receipe_id': receipe_id,
            'status': status,
        }

        newdata = this.state.data
        newdata.fav_status = status
        console.log('newdata', newdata)
        theNavigation.setParams({ data: newdata })

        APICall.callWebServiceBackground(httpMethod, strURL, headers, params, (resJSON) => {
            console.log('Favorite video Screen -> ');
            console.log(resJSON)
            if (resJSON.status == 200) {
                if (status == 0) {
                    //this.refs.toast.show('Unfavorite sucessfull.');
                } else {
                    //this.refs.toast.show('favorite sucessfull.');
                }
                // newdata = this.state.data
                // newdata.fav_status = status
                // console.log('newdata', newdata)
                // theNavigation.setParams({ data: newdata })
                console.log(this.state)
                if (this.state.FromFavorite) {
                    theNavigation.goBack()
                    //theNavigation.state.params.onGoBack(this.state.index)
                }
                return
            } else {
                this.refs.toast.show(resJSON.message);
            }

        })
    }


    toggleFilterModal = () => {
        //this.setState({ isFilterVisible: !this.state.isFilterVisible });
        //console.log(this.state.isFilterVisible);
        theNavigation.push('videoPlayer', {
            video : this.state.data.video,
            images : this.state.images[0] 
        })

    }

    toggleHideModal = (filterOptions) => {
        this.setState({ isFilterVisible: false });
        console.log('filterOptions', filterOptions);
    }

    render() {
        if (this.state.isLoading) {
            return (<View></View>)
        } else {
            console.log('food detail ', this.state);
            return (
                <Container flex-direction={"row"} style={{ flex: 1, }} >
                    <Toastt ref="toast"></Toastt>
                    <BackgroundImage  >
                        <SafeAreaView />
                        <Content >
                            <View style={{ backgroundColor: 'red', flex: 1, height: 180 }}>
                                <ImageSlider images={this.state.images} />
                            </View>
                            <View style={{
                                paddingLeft: 20,
                                paddingTop: 20,
                            }}>
                                <View>
                                    <Text style={styles.headerTitle} >{this.state.data.title}</Text>
                                    <View style={{ flexDirection: 'row', marginTop: 10 }}>
                                        <Image source={clock} style={{ width: 21, height: 21, resizeMode: 'cover', tintColor: '#2f4b6d' }} />
                                        <Text style={styles.durationTitle}>{this.state.data.duration} Minutes</Text>
                                        <Image source={serving} style={{ width: 20, height: 21, marginLeft: 20, resizeMode: 'cover', tintColor: '#2f4b6d' }} />
                                        <Text style={styles.durationTitle} >{this.state.data.serving} Surving</Text>
                                        {
                                            this.state.data.video.length >= 1 ?
                                                <View style={{ justifyContent: 'flex-end', alignItems: 'flex-end', height: 21, flex: 1 }}><Button style={{ borderRadius: 5, height: 25, borderRadius: 15, backgroundColor: '#2265B3', alignSelf: 'flex-end', marginRight: 5 }} onPress={() => this.toggleFilterModal()}><Text style={{ fontSize: 12 }}>Watch Recipe</Text></Button></View>
                                                : null
                                        }
                                    </View>
                                </View>

                            </View>
                            <View style={{ paddingTop: 15, paddingBottom: 5 }}>
                                <View style={{ height: 1, backgroundColor: '#2f4b6d' }}></View>
                            </View>
                            <View style={{
                                paddingTop: 10, paddingLeft: 20,
                                paddingRight: 20,
                            }}>
                                <HTMLView
                                    value={this.state.data.descriptions}
                                    stylesheet={styles}
                                    addLineBreaks={false}
                                />
                                <Text style={[styles.headersubTitle, { marginTop: -15 }]}>Ingredients</Text>
                                <HTMLView
                                    value={this.state.data.ingredients}
                                    stylesheet={styles}
                                    style={{ marginLeft: -15, marginTop: -15 }}
                                    addLineBreaks={false}
                                />
                                <Text style={[styles.headersubTitle, { marginTop: -40 }]}>Method / Ingredients</Text>
                                <HTMLView
                                    value={this.state.data.directions}
                                    stylesheet={styles}
                                    style={{ marginLeft: 0, marginTop: 10 }}
                                    addLineBreaks={false}
                                />
                            </View>
                        </Content>
                        <Modal isVisible={this.state.isFilterVisible} style={{ margin: 0 }}>
                            <VideoPlayer
                                hidePopup={(filter) => this.toggleHideModal(filter)}
                                closePopup={() => this.toggleFilterModal()}
                                video={this.state.data.video}
                                images={this.state.images[0]}

                            />
                        </Modal>
                    </BackgroundImage>
                </Container>
            );
        }
    }
}
export default FoodDetail;

const React = require("react-native");
const { Platform, Dimensions } = React;
let deviceWidth = Dimensions.get('window').width

export default {
  headerAndroidnav: {
    backgroundColor: '#2265B3',
    shadowOpacity: 0,
    borderBottomWidth: 0,
    height: (deviceWidth * 100) / 320,
  },
  headerTitle: {
    fontFamily: font_SFProDisplay_Regular,
    color: 'white',
    fontSize: 20,

  },
  videoDescription: {
    color: 'white',
    fontSize: 18,
  },
  videoDuration: {
    color: 'white',
    fontSize: 16,
    marginTop: 3,
  },
  durationTitle: {
    color: '#2f4b6d',
    fontSize: 16,
    marginLeft: 5,
  },
  p: {
    color: 'white', // make links coloured pink
  },
  headersubTitle: {
    fontFamily: font_SFProDisplay_Regular,
    color: 'white',
    fontSize: 18,
  },
  li: {
    color: 'white', // make links coloured pink
    fontSize: 16,
  },
  ul: {
    color: '#2265B3', // make links coloured pink
    fontSize: 18,
    padding: 5,
  },
};




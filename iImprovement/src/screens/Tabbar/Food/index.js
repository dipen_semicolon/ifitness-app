import React, { Component } from 'react';
import { Image, ImageBackground } from "react-native";
import { createStackNavigator, TabNavigator } from 'react-navigation';
import FoodList from './FoodList'
import FoodDetail from './FoodDetail'
import videoPlayer from './videoPlayer'

import {
  Container, Header, Left, Body, Right, Button, Icon, Title, Text, View
} from "native-base";


const RootStack = createStackNavigator(
  {
    FoodList: {
      screen: FoodList, navigationOptions: () => ({
      })
    },
    FoodDetail: {
      screen: FoodDetail, navigationOptions: () => ({
      })
    },
    videoPlayer: {
      screen: videoPlayer, navigationOptions: () => ({
        header:null,
      })
    },
  },
  {
    initialRouteName: 'FoodList',
    headerMode: 'screen',
    orientation: 'portrait',
    navBarBackgroundColor: 'transparent',
    headerStyle: {

    },
  },

);

RootStack.navigationOptions = ({ navigation }) => {

  let tabBarVisible = true;
  if (navigation.state.index > 0) {
      tabBarVisible = false
  }
  return {
      tabBarVisible,
  };
};
export default RootStack;

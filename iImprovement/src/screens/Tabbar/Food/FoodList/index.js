import React, { Component } from "react";
import { SafeAreaView, Image, ScrollView, TouchableHighlight, ImageBackground, Animated, StatusBar, TouchableOpacity, ActivityIndicator, Dimensions, BackHandler } from "react-native";
import { Container, View, Text, Content, Button, Header, Left, Right } from "native-base";
import styles from "./style";
import ValidationComponent from 'react-native-form-validator';
import ScrollableTabView from 'react-native-scrollable-tab-view';
import TabBar from "react-native-underline-tabbar";
import imageCacheHoc from 'react-native-image-cache-hoc';
import Modal from "react-native-modal";
import FilterPopup from "../../Workout/filterpopup";
import InAppPurchase from "../../../inapp"


const topIcon = require("../../../../../assets/images/topIcon.png");
const fav = require("../../../../../assets/images/fav.png");
const fav_selected = require("../../../../../assets/images/fav_selected.png");
const sort = require("../../../../../assets/images/sort.png");

const star = require("../../../../../assets/images/food.png");
const food_placeholder = require("../../../../../assets/images/food_placeholder.png");
const padlock = require("../../../../../assets/images/padlock.png");


let deviceHeight = Dimensions.get('window').height - 350

var theNavigation;
var subthis;
const CacheableImage = imageCacheHoc(ImageBackground, {
  fileHostWhitelist: ['100.25.250.56'],
  validProtocols: ['http', 'https'],
  defaultPlaceholder: {
    component: Image,
    props: {
      style: styles.activityIndicatorStyle,
      source: food_placeholder
    }
  }
});

const Page = ({ label, data, videoLoad }) => (
  <Content>
    <View style={[styles.container]}>
      {videoLoad == true ? renderReceipe(data) : null}
    </View>
  </Content>
);

const Tab = ({ tab, page, isTabActive, onPressHandler, onTabLayout, styles }) => {
  const { label, icon } = tab;
  const style = {
    borderRightWidth: (subthis.state.catData.length - 1) == page ? 0 : 2,
    borderRightColor: '#092441',
  };
  const containerStyle = {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: styles.backgroundColor,
    opacity: styles.opacity,
    transform: [{ scale: styles.opacity }],
    paddingHorizontal: 20,
    paddingVertical: 10,
  };
  const textStyle = {
    color: styles.textColor,
    fontSize: 18,
    fontFamily: font_MyriadPro_Regular,
  };

  return (
    <TouchableOpacity style={style} onPress={onPressHandler} onLayout={onTabLayout} key={page}>
      <Animated.View style={containerStyle}>
        <Animated.Text style={textStyle}>{label}</Animated.Text>
      </Animated.View>
    </TouchableOpacity>
  );
};

const renderReceipe = (receipeData) => {
  console.log(receipeData);
  if (receipeData != null) {
    var facetsArray = receipeData;
    var facetListViewArray = [];
    facetsArray = facetsArray.map((facet, i) => {
      facetListViewArray.push(
        <TouchableHighlight style={{ flexDirection: 'row' }}
          onPress={() => {
            if (facet.paid == '1' && is_premium == '0') {
              //theNavigation.push('inapp')
              subthis.setState({isIAPVisible:true}) 
            } else {
              theNavigation.push('FoodDetail', {
                title: facet.title,
                data: facet
              })
            }
          }}
          underlayColor="transparent"
          activeOpacity={1}
        >
          <View style={{ flex: 1, padding: 10, }}>
            <CacheableImage source={{ uri: facet.images.length >= 1 ? facet.images[0].photo : 'http://100.25.250.56:8081/images/food_placeholder.png' }} resizeMode='cover' style={{ width: '100%', height: 180 }} permanent={true}>
              <View style={{
                paddingLeft: 10, paddingBottom: 10, flexDirection: 'row',
                justifyContent: 'flex-end',
                alignItems: 'center',
              }}>
                {
                  facet.paid == '1' && is_premium == '0' ?
                    <Image source={padlock} style={{ justifyContent: 'flex-end', marginRight: 5, marginTop: 5, paddingRight: 5, width: 25, height: 25 }} />
                    :
                    <Button transparent style={{ justifyContent: 'flex-end', paddingRight: 5, width: 44, height: 44 }} onPress={() => subthis.wsFavcall(facet.id, facet.fav_status == 1 ? 0 : 1, i)} ><Image source={facet.fav_status == 1 ? fav_selected : fav} style={{ width: 22, height: 22, tintColor: 'white' }} /></Button>
                }

              </View>
              <View style={{ paddingLeft: 10, paddingBottom: 10, alignItems: 'flex-start', justifyContent: 'flex-end', flex: 1 }}>
                <View style={[styles.videoTitleBG]}>
                  <Text style={styles.videoTitle}>{facet.title}</Text>
                </View>
              </View>
            </CacheableImage>
          </View>
        </TouchableHighlight>
      )
    })
    if (facetListViewArray.length == 0) {
      return (<View style={{ flex: 1, height: deviceHeight, alignItems: 'center', justifyContent: 'center' }}>
        <Image source={star} style={{ width: 36, height: 36 }} />
        <Text style={{ color: 'white', fontSize: 20, fontFamily: font_SFProDisplay_Bold, marginTop: 10 }}>{'Receipes'.toUpperCase()}</Text>
        <Text style={{ color: 'white', fontSize: 16, fontFamily: font_SFProDisplay_Regular, marginTop: 10, marginLeft: 20, marginRight: 20, textAlign: 'center' }}> No receipes for this categories.</Text>
      </View>)
    }
    return (facetListViewArray)

  }
  return (<View style={{ flex: 1, height: deviceHeight, alignItems: 'center', justifyContent: 'center' }}>
    <Image source={star} style={{ width: 36, height: 36 }} />
    <Text style={{ color: 'white', fontSize: 20, fontFamily: font_SFProDisplay_Bold, marginTop: 10 }}>{'Receipes'.toUpperCase()}</Text>
    <Text style={{ color: 'white', fontSize: 16, fontFamily: font_SFProDisplay_Regular, marginTop: 10, marginLeft: 20, marginRight: 20, textAlign: 'center' }}> No receipes for this categories.</Text>
  </View>)
}

class foodList extends ValidationComponent {

  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      catData: null,
      page: 0,
      isReceipeDataLoad: false,
      isFilterVisible: false,
      titlesort: 0,
      durationsort: 0,
      selectedId: 0,
      sortName: 'SORT',
      isIAPVisible:false,
    };
    subthis = this
  }

  static navigationOptions = ({ navigation }) => {
    theNavigation = navigation;

    return {
      header: (
        <Header style={styles.headerAndroidnav}>
          <StatusBar barStyle="light-content" backgroundColor="#051f3a" />

          <Left style={{ alignSelf: 'flex-start', flex: 0.1 }} >
          </Left>
          <View style={{ alignSelf: 'flex-start', flex: 1 }}>
            <View style={{ alignItems: 'center', justifyContent: 'center', flex: 1, marginTop: 15, marginBottom: 15 }}>
              <Image source={topIcon} style={[styles.topIcon]} />
            </View>
          </View>
          <Right style={{ alignSelf: 'flex-start', flex: 0.1 }}>
            {/* <Button transparent style={{ alignSelf: 'baseline', width: 44, height: 44 }} ><Image source={search} style={{ width: 22, height: 22, tintColor: 'black' }} /></Button> */}
          </Right>
        </Header>
      )
    }
  }


  componentDidMount() {
    this.setState({
      catData: theNavigation.getParam('catData'),
      page: theNavigation.getParam('currentIndex'),
      selectedId: theNavigation.getParam('selectedId'),
      title: theNavigation.getParam('title'),
    })
    this.wscall(theNavigation.getParam('selectedId'), this.state.titlesort, this.state.durationsort)
  }
  oncatChange = (value) => {
    console.log(this.state.catData[value.i].id)
    var selectId = this.state.catData[value.i].id
    this.setState({
      // 0isReceipeDataLoad: false,
      selectedId: selectId
    })
    this.wsReceiprcall(selectId, this.state.titlesort, this.state.durationsort)
  }

  wscall = async () => {
    let httpMethod = 'POST'
    let strURL = nutistionCategories
    let headers = {
      'Content-Type': 'application/json',
    }
    let params = {
    }

    APICall.callWebService(httpMethod, strURL, headers, params, (resJSON) => {
      console.log('wscall Screen -> ');
      console.log(resJSON)
      if (resJSON.status == 200) {
        this.setState({
          catData: resJSON.data,
        })
        if (resJSON.data.length > 0) {
          this.setState({ selectedId: resJSON.data[0].id })
          this.wsReceiprcall(resJSON.data[0].id, this.state.titlesort, this.state.durationsort)
        }
        return
      } else {
        this.refs.toast.show(resJSON.message);
      }

    })
  }


  wsReceiprcall = async (cat_id, titlesort, durationsort) => {
    let httpMethod = 'POST'
    let strURL = receipeList
    let headers = {
      'Content-Type': 'application/json',
      'token': token,
      'userid': userid,
    }
    let params = {
      'cat_id': cat_id,
      'title': titlesort,
      'duration': durationsort,

    }

    APICall.callWebService(httpMethod, strURL, headers, params, (resJSON) => {
      console.log('receipeList Screen -> ');
      console.log(resJSON)
      if (resJSON.status == 200) {

        var newobj = this.state.receipeData
        if (newobj == undefined) {
          newobj = {}
        }
        newobj[cat_id] = resJSON.data
        this.setState({
          isLoading: false,
          receipeData: newobj,
          isReceipeDataLoad: true,

        })
        return
      } else {
        this.refs.toast.show(resJSON.message);
      }

    })
  }

  wsFavcall = async (receipe_id, status, index) => {
    let httpMethod = 'POST'
    let strURL = favReceipe
    let headers = {
      'Content-Type': 'application/json',
      'token': token,
      'userid': userid,
    }
    let params = {
      'receipe_id': receipe_id,
      'status': status,
    }

    fulldata = this.state.receipeData
    newData = fulldata[this.state.selectedId]
    receipeData = newData[index]
    receipeData.fav_status = status
    fulldata[this.state.selectedId] = newData

    console.log('newdata', receipeData)
    subthis.setState({ receipeData: fulldata })


    APICall.callWebServiceBackground(httpMethod, strURL, headers, params, (resJSON) => {
      console.log('wsFavcall Screen -> ');
      console.log(resJSON)
      if (resJSON.status == 200) {
        if (status == 0) {
          // this.refs.toast.show('Unfavorite sucessfull.');
        } else {
          //this.refs.toast.show('favorite sucessfull.');
        }
        // newData =  this.state.receipeData
        // receipeData = newData[index]
        // receipeData.fav_status = status
        // console.log('newdata', receipeData)
        // subthis.setState({receipeData:newData})
        return
      } else {
        this.refs.toast.show(resJSON.message);
      }

    })
  }
  renderPage() {
    if (this.state.catData != null) {
      var facetsArray = this.state.catData;
      var facetListViewArray = [];
      facetsArray = facetsArray.map((facet, i) => {
        facetListViewArray.push(
          <Page tabLabel={{ label: facet.title }} data={this.state.receipeData[facet.id]} videoLoad={this.state.isReceipeDataLoad} />
        )
      })
      return (facetListViewArray)
    }

    return (<Page tabLabel={{ label: "" }} />)
  }




  _scrollX = new Animated.Value(0);
  // 6 is a quantity of tabs
  interpolators = Array.from({ length: 6 }, (_, i) => i).map(idx => ({
    // scale: this._scrollX.interpolate({
    //   inputRange: [idx - 1, idx, idx + 1],
    //   outputRange: [1, 1.2, 1],
    //   extrapolate: 'clamp',
    // }),
    opacity: this._scrollX.interpolate({
      inputRange: [idx - 1, idx, idx + 1],
      outputRange: [0.9, 1, 0.9],
      extrapolate: 'clamp',
    }),
    textColor: '#ffffff',
    // textColor: this._scrollX.interpolate({
    //   inputRange: [idx - 1, idx, idx + 1],
    //   outputRange: ['#000', '#ffffff', '#000'],
    // }),
    backgroundColor: this._scrollX.interpolate({
      inputRange: [idx - 1, idx, idx + 1],
      outputRange: ['#0F2A47', '#092441', '#0F2A47'],
      extrapolate: 'clamp',
    }),
  }));

  toggleFilterModal = () => {
    this.setState({ isFilterVisible: !this.state.isFilterVisible });
    console.log(this.state.isFilterVisible);
  }

  toggleHideModal = (filterOptions) => {
    console.log('filterOptions', filterOptions);
    var title = 0
    var duration = 0
    var sortName = 'SORT'
    if (filterOptions.dictSearchSetting.duration[0].isSelected) {
      //sortest video
      duration = 1
      sortName = 'Shortest'
    } else if (filterOptions.dictSearchSetting.duration[1].isSelected) {
      //longest video
      duration = 2
      sortName = 'Longest'
    } else if (filterOptions.dictSearchSetting.duration[2].isSelected) {
      //longest video
      title = 1
      sortName = 'A-Z'
    }

    this.setState({
      isFilterVisible: false,
      titlesort: title,
      durationsort: duration,
      sortName: sortName
    });
    this.wsReceiprcall(this.state.selectedId, title, duration)
  }


  onBack = () => {
    Alert.alert(
      "Are you sure you want to exit?",
      [
        { text: "Cancel", onPress: () => { }, style: "cancel" },
        { text: "Ok", onPress: () => BackHandler.exitApp() },
      ],
      { cancelable: false },
    );
    return true;
  };


  togglePopupPromocode = () => {
    this.setState({ isIAPVisible: !this.state.isIAPVisible });
  }

  render() {
    if (this.state.isLoading) {
      return (<BackgroundImage></BackgroundImage>)
    } else {
      return (
        <HandleBack onBack={this.onBack}>
          <Container flex-direction={"row"} style={{ flex: 1, }} >
            <Toastt ref="toast"></Toastt>
            <BackgroundImage  >
              <SafeAreaView />

              <ScrollableTabView
                tabBarActiveTextColor="white"
                tabBarInactiveTextColor='white'
                tabBarBackgroundColor='#0F2A47'
                //renderTabBar={() => <TabBar underlineColor="transparent" tabBarTextStyle={{ fontSize: 18, fontFamily: font_MyriadPro_Regular, }} tabBarStyle={{ borderBottomWidth: 0, marginTop: 0,height:50,alignItems:'center'}} />}
                initialPage={this.state.page}
                Page={this.state.page}
                onChangeTab={(value) => this.oncatChange(value)}
                renderTabBar={() => (
                  <TabBar
                    underlineColor="#0F2A47"
                    underlineHeight={0}
                    tabBarStyle={{
                      backgroundColor: "#0F2A47",
                      borderBottomWidth: 0, marginTop: 0
                    }}
                    renderTab={(tab, page, isTabActive, onPressHandler, onTabLayout) => (
                      <Tab
                        key={this.state.page}
                        tab={tab}
                        page={page}
                        isTabActive={isTabActive}
                        onPressHandler={onPressHandler}
                        onTabLayout={onTabLayout}
                        styles={this.interpolators[page]}
                      />
                    )}
                  />
                )}
                onScroll={(x) => this._scrollX.setValue(x)}
              >
                {this.renderPage()}

              </ScrollableTabView>
              {/* <View style={{ backgroundColor: '#2265B3', height: 50, alignItems: 'center', justifyContent: 'center' }}>
              <Button transparent style={{ alignSelf: 'center', justifyContent: 'center', alignItems: 'center', flex: 1, width: '100%' }}
                onPress={() => { subthis.toggleFilterModal() }}
              >
                <Image source={sort} style={{ width: 24, height: 24 }} />
                <Text style={{ color: 'white', fontSize: 20, fontFamily: font_MyriadPro_Bold, marginTop: 10 }}>{this.state.sortName}</Text>
              </Button>
            </View> */}
              <Modal isVisible={this.state.isFilterVisible} style={{ margin: 0 }}>
                <FilterPopup
                  hidePopup={(filter) => this.toggleHideModal(filter)}
                  closePopup={() => this.toggleFilterModal()}
                  nameTitle='Receipe Name'
                  titlesort={this.state.titlesort}
                  durationsort={this.state.durationsort}

                />
              </Modal>
              <Modal isVisible={this.state.isIAPVisible} style={{ margin: 0, padding: 0 }} >
              <InAppPurchase
                hidePopup={() => this.togglePopupPromocode()}
                closeBtnPressed={() => this.togglePopupPromocode()}
              />
            </Modal>
            </BackgroundImage>
          </Container>
        </HandleBack>
      );
    }
  }
}
export default foodList;

const React = require("react-native");
const { Dimensions } = React;

global.DEVICE_HEIGHT = Dimensions.get("window").height;
global.DEVICE_WIDTH = Dimensions.get("window").width;

global.font_SFProDisplay_Bold = "SFProDisplay-Bold";
global.font_SFProDisplay_Light = "SFProDisplay-Light";
global.font_SFProDisplay_Medium = "SFProDisplay-Medium";
global.font_SFProDisplay_Regular = "SFProDisplay-Regular";
global.font_SFProDisplay_Black = "SFProDisplay-Black";
global.font_SFProDisplay_SemiBold = "SFProDisplay-SemiBold";
global.font_SFProDisplay_Thin = "SFProDisplay-Thin";
global.font_MyriadPro_Regular = "MyriadPro-Regular";
global.font_MyriadPro_Bold = "MyriadPro-Bold";
global.font_MyriadPro_BoldSemiExt = "MyriadPro-BoldSemiExt";
global.font_MyriadPro_BoldSemiExt = "MyriadPro-BoldSemiExt";

//api urls
global.baseURL = "http://100.25.250.56:8081/api/"

global.userRegister = baseURL+ "userRegister";
global.userlogin = baseURL+ "userlogin";
global.verifyEmail = baseURL+ "verifyEmail";
global.resendVerifyEmail = baseURL+ "resendVerifyEmail";


global.home = baseURL+ "home";
global.userlogout = baseURL+ "userlogout";
global.userforgotpassword = baseURL+ "userforgotpassword";
global.workoutCategories = baseURL+ "workoutCategories";
global.workoutVideos = baseURL+ "workoutVideos";
global.favVideo = baseURL+ "favVideo";
global.favVideoList = baseURL+ "favVideoList";


global.nutistionCategories = baseURL+ "nutistionCategories";
global.receipeList = baseURL+ "receipeList";
global.favReceipe = baseURL+ "favReceipe";
global.favReceipeList = baseURL+ "favReceipeList";


global.pages = baseURL+ "pages";
global.checkfb = baseURL+ "checkfb";
global.editUserProfile = baseURL+ "editUserProfile";
global.messageList = baseURL+ "messageList";
global.deletemessage = baseURL+ "deletemessage";

global.singleVideo = baseURL+ "singleVideo";
global.singleReceipe = baseURL+ "singleReceipe";

global.contact_us = baseURL+ "contact_us";
global.changepassword = baseURL+ "changepassword";
global.redemList = baseURL+ "redemList";
global.redeemed = baseURL+ "redeemed";
global.userinfo = baseURL+ "userinfo";
global.checkCode = baseURL+ "checkCode";
global.inapplist = baseURL+ "inapplist";
global.buyproduct = baseURL+ "buyproduct";
global.checkReceipt = baseURL+ "checkReceipt";



//constant variable
global.userid = 0;
global.token = '';
global.deviceToken = '';
global.is_premium = '0';




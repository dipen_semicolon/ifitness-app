import React, { Component } from "react";
import { SafeAreaView, TextInput, Image, Dimensions, StatusBar, Platform } from "react-native";
import { Container, View, Text, Content, Button, Header, Left, Right } from "native-base";
import styles from "./style";
import ValidationComponent from 'react-native-form-validator';
import ToastSucess from "../Default/ToastSucess"
import * as Pref from '../../Pref/Pref';


let deviceWidth = Dimensions.get('window').width
const lock = require("../../../assets/images/lock.png");
const backWhite = require("../../../assets/images/back.png");
const topIcon = require("../../../assets/images/topIcon.png");

class OTPVerify extends ValidationComponent {

  constructor(props) {
    super(props);
    this.state = { otp: "", loading: true,code:'',id:'' };
  }

  static navigationOptions = ({ navigation }) => {
    theNavigation = navigation;

    return {
      header: (
        <Header style={styles.headerAndroidnav}>
          <StatusBar barStyle="light-content" backgroundColor="#051f3a" />

          <Left style={{ alignSelf: 'flex-start', flex: 0.1 }} >
            <Button transparent onPress={() => navigation.goBack()} style={{ alignSelf: 'baseline', width: 44, height: 44 }} >
              <Image source={backWhite} style={{ width: 22, height: 22, tintColor: "white" }} />
            </Button>
          </Left>
          <View style={{ alignSelf: 'flex-start', flex: 1 }}>
            <Text style={styles.headerTitle} >{'VERIFY OTP'.toUpperCase()}</Text>
          </View>
          <Right style={{ alignSelf: 'flex-start', flex: 0.1 }}>
            {/* <Button transparent style={{ alignSelf: 'baseline', width: 44, height: 44 }} ><Image source={search} style={{ width: 22, height: 22, tintColor: 'black' }} /></Button> */}
          </Right>
        </Header>
      )
    }

  }
  componentDidMount() {
    this.setState({
      data: theNavigation.getParam('data'),
    })
}


  onResetpasswordClick = () => {

    if (this.validtion()) {
      this.wscall(this.state.data.id);
    }
    
  }

  validtion = () => {
    this.validate({
      otp: {  required: true },
    });
    if (this.isFieldInError('otp')) {
      this.refs.toast.show(this.getErrorsInField('otp'), 500, () => {
      });
      return false
    }else if(this.state.data.code != this.state.otp){
      this.refs.toast.show('Verification code is not match');
      return false
    }
    return true
  }

  wscall = async (id) => {
    let httpMethod = 'POST'
    let strURL = verifyEmail
    let headers = {
      'Content-Type': 'application/json',
    }
    let params = {
      'id': id,
      'status': 1,
    }

    APICall.callWebService(httpMethod, strURL, headers, params, (resJSON) => {
      console.log('Login Screen -> ');
      console.log(resJSON)
      if (resJSON.status == 200) {
        Pref.setData(Pref.kUserInfo, this.state.data)
        Pref.setData(Pref.kUserId, this.state.data.id)
        Pref.setData(Pref.kaccessToken, this.state.data.token)
        userid = this.state.data.id
        token = this.state.data.token
        this.props.navigation.push('Tabbar')
        return
      } else {
        this.refs.toast.show(resJSON.message);
      }
    })
  }


  render() {

    return (
      <Container flex-direction={"row"} style={{ flex: 1 }} >
        <Toastt ref="toast"></Toastt>
        <ToastSucess ref="toastsucess"></ToastSucess>

        <BackgroundImage >
          <SafeAreaView />
          <Content style={{ paddingLeft: 33, paddingRight: 33 }}>
            <View style={{ alignItems: 'center', justifyContent: 'center', marginTop: 60, flex: 1 }}>
              <Image source={topIcon} style={{ width: 170, height: 100, resizeMode: 'cover' }} />
            </View>
            <View>
              <View style={[styles.textInputView, { marginTop: 57, flexDirection: 'row', alignItems: 'center' }]}>
                <Image source={lock} style={{ height: 22, width: 16, marginLeft: 20 }} />
                <TextInput
                  ref="otp"
                  onChangeText={(otp) => this.setState({ otp })} value={this.state.otp}
                  style={[styles.textInput, { marginLeft: 10 }]}
                  placeholder="Enter OTP"
                  placeholderTextColor="#95959a"
                  autoCorrect={false}
                />
              </View>
            </View>
            <View style={{ marginTop: 20 }}>

              <Button style={[styles.bottomViewButton, { backgroundColor: '#0F66C7', marginTop: 5, borderRadius: 0, }]}
                onPress={() => this.onResetpasswordClick()}
              >
                <Text style={{ color: 'white' }}>VERIFY OTP</Text>
              </Button>
            </View>
          </Content>
        </BackgroundImage>
      </Container>
    );
  }
}

export default OTPVerify;

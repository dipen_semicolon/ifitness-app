import React, { Component } from "react";
import { SafeAreaView, TextInput, Image, Dimensions, StatusBar, Platform, Alert } from "react-native";
import { Container, View, Text, Content, Button, Header, Left, Right } from "native-base";
import styles from "./style";
import ValidationComponent from 'react-native-form-validator';
import * as Pref from '../../Pref/Pref';


let deviceWidth = Dimensions.get('window').width

const email = require("../../../assets/images/email.png");
const lock = require("../../../assets/images/lock.png");
const topIcon = require("../../../assets/images/topIcon.png");
const backWhite = require("../../../assets/images/back.png");
const fbIcon = require("../../../assets/images/facebook.png");
const FBSDK = require('react-native-fbsdk');
const {
  LoginManager,
  GraphRequest,
  GraphRequestManager,
  AccessToken,
} = FBSDK;

const infoRequest = new GraphRequest(
  '/me',
  null,
  this._responseInfoCallback,
);


_responseInfoCallback = (error, result) => {
  console.log('test');
  if (error) {
    alert('Error fetching data: ' + error.toString());
  } else {
    alert('Success fetching data: ' + result.toString());
  }
}



class Login extends ValidationComponent {
  constructor(props) {
    super(props);
    this.state = { email: "", password: "", loading: true };
  }

  static navigationOptions = ({ navigation }) => {
    theNavigation = navigation;

    return {
      header: (
        <Header style={styles.headerAndroidnav}>
          <StatusBar barStyle="light-content" backgroundColor="#051f3a" />

          <Left style={{ alignSelf: 'flex-start', flex: 0.1 }} >
            <Button transparent onPress={() => navigation.goBack()} style={{ alignSelf: 'baseline', width: 44, height: 44 }} >
              <Image source={backWhite} style={{ width: 22, height: 22, tintColor: "white" }} />
            </Button>
          </Left>
          <View style={{ alignSelf: 'flex-start', flex: 1 }}>
            <Text style={styles.headerTitle} >{'Login'.toUpperCase()}</Text>
          </View>
          <Right style={{ alignSelf: 'flex-start', flex: 0.1 }}>
            {/* <Button transparent style={{ alignSelf: 'baseline', width: 44, height: 44 }} ><Image source={search} style={{ width: 22, height: 22, tintColor: 'black' }} /></Button> */}
          </Right>
        </Header>
      )
    }

  }
  componentWillMount() {

  }

  onForgotPassword = (page) => {
    this.props.navigation.push('ForgotPassword')
  }

  onSignInClick = () => {

    if (this.validtion()) {
      this.wscall(this.state.email, this.state.password); //2 for student
    }
  }


  FbLoginButton = () => {
    LoginManager
      .logInWithReadPermissions(['public_profile', 'email'])
      .then(function (result) {
        if (result.isCancelled) {
          console.log('Login cancelled');
        } else {
          AccessToken
            .getCurrentAccessToken()
            .then((data) => {
              let accessToken = data.accessToken
              console.log(accessToken.toString())

              const responseInfoCallback = (error, result) => {
                if (error) {
                  console.log(error)
                } else {
                  console.log(result)
                  checkfbFunc(result.id, result.email, result.name, result.picture.data.url)
                }
              }

              const infoRequest = new GraphRequest(
                '/me?fields=email,name,picture',
                null,
                responseInfoCallback,
              );
              new GraphRequestManager().addRequest(infoRequest).start();


              // const infoRequest = new GraphRequest('/me', {
              //   accessToken: accessToken,
              //   parameters: {
              //     fields: {
              //       string: 'email,name,picture'
              //     }
              //   }
              // }, responseInfoCallback);

              // // Start the graph request.
              // new GraphRequestManager()
              //   .addRequest(infoRequest)
              //   .start()

            })
        }
      }, function (error) {
        console.log('Login fail with error: ' + error);
      });

    checkfbFunc = (facebook_id, email, name, photo) => {
      if (email.length >= 1) {
        this.verifyFbIdws(facebook_id, email, name, photo)
      } else {
        this.props.navigation.push("subRegistration", {
          'facebook_id': facebook_id,
          'email': email,
          'name': name,
          'photo': photo,
          'code': this.state.validCode
        })
      }
    }
  }



  verifyFbIdws = async (facebook_id, email, name, photo) => {
    let httpMethod = 'POST'
    let strURL = checkfb
    let headers = {
      'Content-Type': 'application/json',
      'device_token': deviceToken,
      'device_type': Platform.OS === 'ios' ? 1 : 2,
      'build_version': '1.0'
    }
    let params = {
      'fbid': facebook_id,
      'name': name,
      'email': email,
      'photo': photo,
    }

    APICall.callWebService(httpMethod, strURL, headers, params, (resJSON) => {
      console.log('Login Screen -> ');
      console.log(resJSON)
      if (resJSON.status == 200) {
        let data = resJSON.data
        console.log(data);
        Pref.setData(Pref.kUserInfo, data)
        Pref.setData(Pref.kUserId, data.id)
        Pref.setData(Pref.kaccessToken, data.token)
        userid = data.id;
        token = data.token;
        this.props.navigation.push('Tabbar')
        return
      } else {
        this.refs.toast.show(resJSON.message);
      }
    })
  }

  validtion = () => {
    this.validate({
      password: { minlength: 5, required: true },
      email: { email: true },
    });
    if (this.isFieldInError('email')) {
      this.refs.toast.show(this.getErrorsInField('email'), 500, () => {
      });
      return false
    } else if (this.isFieldInError('password')) {
      this.refs.toast.show(this.getErrorsInField('password'), 500, () => {
      });
      return false
    }
    return true
  }

  wscall = async (email, password) => {
    let httpMethod = 'POST'
    let strURL = userlogin
    let headers = {
      'Content-Type': 'application/json',
      'device_token': deviceToken,
      'device_type': Platform.OS === 'ios' ? 1 : 2,
      'build_version': '1.0',
    }
    let params = {
      'email': email,
      'password': password,
    }

    APICall.callWebService(httpMethod, strURL, headers, params, (resJSON) => {
      console.log('Login Screen -> ');
      console.log(resJSON)
      if (resJSON.status == 200) {
        let data = resJSON.data
        console.log(data);
        if (data.is_active == 0) {

          Alert.alert(
            'Account not verify',
            'your email is not verify.are you want to send OTP again?',
            [
              { text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
              { text: 'Resend', onPress: () => this.wsresndEamilcall(data) },
            ],
            { cancelable: false }
          )

        } else {
          Pref.setData(Pref.kUserInfo, data)
          Pref.setData(Pref.kUserId, data.id)
          Pref.setData(Pref.kaccessToken, data.token)
          userid = data.id
          token = data.token
          this.props.navigation.push('Tabbar')
        }
        return
      } else {
        this.refs.toast.show(resJSON.message);
      }

    })
  }


  wsresndEamilcall = async (data) => {
    let httpMethod = 'POST'
    let strURL = resendVerifyEmail
    let headers = {
      'Content-Type': 'application/json',
    }
    let params = {
      'id': data.id,
    }

    APICall.callWebService(httpMethod, strURL, headers, params, (resJSON) => {
      console.log('resendVerifyEmail Screen -> ');
      console.log(resJSON)
      if (resJSON.status == 200) {
        this.props.navigation.push('OTP', {
          data: data,
        })
        return
      } else {
        this.refs.toast.show(resJSON.message);
      }
    })
  }
  render() {

    return (
      <Container flex-direction={"row"} style={{ flex: 1 }} >
        <Toastt ref="toast"></Toastt>
        <BackgroundImage >
          <SafeAreaView />
          <Content style={{ paddingLeft: 33, paddingRight: 33 }}>
            <View style={{ alignItems: 'center', justifyContent: 'center', marginTop: 60, flex: 1 }}>
              <Image source={topIcon} style={{ width: 170, height: 100, resizeMode: 'cover' }} />
            </View>
            <View style={{ flex: 0.5 }}>
              <View style={[styles.textInputView, { marginTop: 57, flexDirection: 'row', alignItems: 'center' }]}>
                <Image source={email} style={{ height: 22, width: 22, marginLeft: 15 }} />
                <TextInput
                  ref="email"
                  onChangeText={(email) => this.setState({ email })} value={this.state.email}
                  style={[styles.textInput, { marginLeft: 10, width: '80%', }]}
                  placeholder="Email Address"
                  placeholderTextColor="#95959a"
                  keyboardType="email-address"
                  autoCorrect={false}
                />
              </View>
              <View style={[styles.textInputView, { marginTop: 20, flexDirection: 'row', alignItems: 'center' }]}>
                <Image source={lock} style={{ height: 22, width: 16, marginLeft: 20 }} />
                <TextInput
                  ref="password"
                  onChangeText={(password) => this.setState({ password })} value={this.state.password}
                  style={[styles.textInput, { marginLeft: 13, width: '80%', }]}
                  placeholder="Password"
                  placeholderTextColor="#95959a"
                  secureTextEntry={true}
                />
              </View>
              <View style={{ marginTop: 20 }}>

                <Button style={[styles.bottomViewButton, { backgroundColor: '#0F66C7', marginTop: 5, borderRadius: 0, }]}
                  onPress={() => this.onSignInClick()}
                >

                  <Text style={{ color: 'white' }}>LOGIN</Text>
                </Button>

                <Button style={[styles.bottomViewButton, { backgroundColor: '#3B66C4', marginTop: 15, borderRadius: 0, }]}
                  onPress={() => this.FbLoginButton()}
                >
                  <Image source={fbIcon} style={{ marginRight: 10, marginTop: -2 }} />
                  <Text style={{ color: 'white' }}>Login with Facebook</Text>
                </Button>

                {/* 
                  shadowOffset: { height: 0, width: 0 },
                  shadowOpacity: 0,
                  elevation:0

                  it should remove button shadow on android 
                */}

                <Button style={[styles.bottomViewButton, { backgroundColor: 'transparent', marginTop: 15, borderRadius: 0, shadowOffset: { height: 0, width: 0 }, shadowOpacity: 0, elevation: 0 }]}
                  onPress={() => this.onForgotPassword()}
                >
                  <View style={{ borderBottomWidth: 1, borderColor: 'white' }}>
                    <Text style={{
                      color: 'white', textAlign: 'center',
                      fontSize: 18,
                    }}>Forgot Password?</Text>
                  </View>

                </Button>

              </View>
            </View>
          </Content>
        </BackgroundImage>
      </Container>
    );
  }
}

export default Login;

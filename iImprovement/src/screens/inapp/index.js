import React, { Component } from "react";
import { SafeAreaView, TextInput, alert, Dimensions, StatusBar, Image, Platform, TouchableOpacity, ImageBackground, Alert } from "react-native";
import { Container, View, Text, Content, Button, Header, Left, Right } from "native-base";
import styles from "./style";
import ValidationComponent from 'react-native-form-validator';
import * as RNIap from 'react-native-iap';
import RNProgressHUB from 'react-native-progresshub';

import * as Pref from '../../Pref/Pref';

const backWhite = require("../../../assets/images/back.png");
const check = require("../../../assets/images/check.png");
const inApp_Banner = require("../../../assets/images/inApp_Banner.jpg");
const close = require("../../../assets/images/close.png");

var subthis;
class InAppPurchase extends ValidationComponent {

  constructor(props) {
    super(props);
    this.state = { description: "", loading: true, title: '', id: '4' };
    subthis = this
  }

  componentDidMount() {
    this.wscall()
  }

  componentWillUnmount() {
    RNIap.endConnection();
  }

  static navigationOptions = ({ navigation }) => {
    theNavigation = navigation;
  }


  wscall = async (id) => {
    let httpMethod = 'POST'
    let strURL = inapplist
    let headers = {
      'Content-Type': 'application/json',
      'token': token,
      'userid': userid,
    }
    let params = {
    }

    APICall.callWebService(httpMethod, strURL, headers, params, (resJSON) => {
      console.log(resJSON)
      if (resJSON.status == 200) {
        let data = resJSON.data
        var iosIds = [];
        var androidIds = [];
        for (let item of data) {
          iosIds.push(item.ios_id)
          androidIds.push(item.android_id)

        }
        const itemSkus = Platform.select({
          ios: iosIds,
          android: androidIds
        });
        subthis.FetchPreoduct(itemSkus)

        return
      } else {
        this.refs.toast.show(resJSON.message);
      }
    })
  }

  async FetchPreoduct(itemSkus) {
    RNProgressHUB.showSpinIndeterminate();
    try {
      const products = await RNIap.getProducts(itemSkus);
      if (products.length >= 1) {
        this.setState({ product: products[0] })
      }
      RNProgressHUB.dismiss();
    } catch (err) {
      RNProgressHUB.dismiss();
      console.warn(err); // standardized err.code and err.message available
    }
  }

  onBuyPress = (id) => {
    RNProgressHUB.showSpinIndeterminate();
    RNIap.buySubscription(id).then(purchase => {
      console.log(purchase)
      RNProgressHUB.dismiss();
      subthis.wsBuycall(purchase.productId, purchase.transactionId, purchase.transactionReceipt)
    }).catch(err => {
      // resetting UI
      console.warn(err); // standardized err.code and err.message available
      RNProgressHUB.dismiss();
    })
  }

  wsBuycall = async (productId, transactionId, transaction, expires_date) => {
    let httpMethod = 'POST'
    let strURL = buyproduct
    let headers = {
      'Content-Type': 'application/json',
      'token': token,
      'userid': userid,
    }
    let params = {
      'productId': productId,
      'transactionId': transactionId,
      'transaction': transaction,
      'expires_date': '0000-00-00',
      'device_type': Platform.OS === 'ios' ? 1 : 2,
    }

    console.log(strURL)
    console.log(headers)
    console.log(params)

    APICall.callWebService(httpMethod, strURL, headers, params, (resJSON) => {
      console.log(resJSON)
      if (resJSON.status == 200) {
        console.log('sucess response')

        Pref.getData(Pref.kUserInfo, (userinfo) => {
          var obj = userinfo;
          console.log(obj)
          is_premium = '1'
          Pref.setData(Pref.kUserInfo, obj)
          Alert.alert(
            'Congratulation!', 'Now you are premium user.',
            [
              { text: 'OK', onPress: () =>  this.props.hidePopup() },
            ],
          );
          RNIap.endConnection();

        })
        return
      } else {
        //this.refs.toast.show(resJSON.message);
      }
    })
  }


  render() {
    console.log('state', this.state)
    if (this.state.isLoading) {
      return (<View></View>)
    } else {
      return (
        <Container flex-direction={"row"} style={{ flex: 1 }} >
          <Toastt ref="toast"></Toastt>
          <Header style={styles.headerAndroidnav}>
            <StatusBar barStyle="light-content" backgroundColor="#051f3a" />
            <Left style={{ alignSelf: 'flex-start', flex: 0.1 }} >
              <Button transparent onPress={() => this.props.hidePopup()} style={{ alignSelf: 'baseline', width: 44, height: 44 }} >
                <Image source={close} style={{ width: 22, height: 22, tintColor: "white" }} />
              </Button>
            </Left>
            <View style={{ alignSelf: 'flex-start', flex: 1 }}>
              <Text style={styles.headerTitle} >{'Go Premium'.toUpperCase()}</Text>
            </View>
          </Header>
          <BackgroundImage >
            <SafeAreaView />
            <View style={{ alignContent: 'center', alignItems: 'center' }}>
              <ImageBackground source={inApp_Banner} style={{ height: 200, width: '100%', alignItems: 'center', justifyContent: 'center' }}>
              </ImageBackground>
            </View>
            <Text style={{ color: 'white', fontSize: 28, fontFamily: font_SFProDisplay_Bold, textAlign: 'center',marginTop:10 }} >Start your fitness journey</Text>
            <Text style={{ color: 'white', fontSize: 32, fontFamily: font_SFProDisplay_Bold, marginTop: 5, textAlign: 'center' }}>Now</Text>
            {
              this.state.product != undefined ?
                <View>
                  <View style={{ margin: 20, marginBottom: 10, borderColor: 'gray', borderWidth: 2, borderRadius: 30, padding: 10, paddingLeft: 20 }}>

                    <TouchableOpacity onPress={() => this.onBuyPress(this.state.product.productId)}>
                      <Text style={{ color: 'white', fontSize: 18, fontFamily: font_SFProDisplay_Bold }} >{'7 days free trial included'.toUpperCase()}</Text>
                      <Text style={{ color: 'white', fontSize: 15, fontFamily: font_SFProDisplay_Regular }} >{this.state.product.localizedPrice} per month after 7 day trial</Text>
                    </TouchableOpacity>
                  </View>
                  <View style={{ marginLeft: 30, marginTop: 20 }}>
                    <View style={{ flexDirection: 'row', marginBottom: 10 }}>
                      <Image source={check} style={{ width: 22, height: 22, marginRight: 10 }} />
                      <Text style={{ color: 'white', fontSize: 20, fontFamily: font_SFProDisplay_Regular }}>Unlimited workout videos</Text>
                    </View>
                    <View style={{ flexDirection: 'row' }}>
                      <Image source={check} style={{ width: 22, height: 22, marginRight: 10,marginBottom:10 }} />
                      <Text style={{ color: 'white', fontSize: 20, fontFamily: font_SFProDisplay_Regular }} >Unlimited nutrition recipes</Text>
                    </View>
                    <View style={{ flexDirection: 'row' }}>
                      <Image source={check} style={{ width: 22, height: 22, marginRight: 10 ,marginBottom:10}} />
                      <Text style={{ color: 'white', fontSize: 20, fontFamily: font_SFProDisplay_Regular }} >New videos added monthly</Text>
                    </View>
                    <View style={{ flexDirection: 'row' }}>
                      <Image source={check} style={{ width: 22, height: 22, marginRight: 10 }} />
                      <Text style={{ color: 'white', fontSize: 20, fontFamily: font_SFProDisplay_Regular }} >Rewards program</Text>
                    </View>
                  </View>
                  {/* <View style={{ justifyContent: 'center', alignItems: 'center', margin: 20 }}>
                    <Button style={{ height: 50, borderRadius: 30, backgroundColor: '#2265B3', paddingLeft: 30, paddingRight: 30, alignItems: 'center', justifyContent: 'center', width: '100%' }} onPress={() => { }}>
                      <Text style={{ fontSize: 20, textAlign: 'center' }}>CONTINUE</Text>
                    </Button>
                  </View> */}
                </View>
                : null
            }
          </BackgroundImage>
        </Container>
      );
    }
  }

}

export default InAppPurchase;

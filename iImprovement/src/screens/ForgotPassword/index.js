import React, { Component } from "react";
import { SafeAreaView, TextInput, Image, Dimensions, StatusBar, Platform,Alert } from "react-native";
import { Container, View, Text, Content, Button, Header, Left, Right } from "native-base";
import styles from "./style";
import ValidationComponent from 'react-native-form-validator';
import ToastSucess from "../Default/ToastSucess"
import * as Pref from '../../Pref/Pref';


let deviceWidth = Dimensions.get('window').width
const email = require("../../../assets/images/email.png");
const backWhite = require("../../../assets/images/back.png");
const topIcon = require("../../../assets/images/topIcon.png");

class ForgotPassword extends ValidationComponent {

  constructor(props) {
    super(props);
    this.state = { email: "", loading: true };
  }

  static navigationOptions = ({ navigation }) => {
    theNavigation = navigation;

    return {
      header: (
        <Header style={styles.headerAndroidnav}>
          <StatusBar barStyle="light-content" backgroundColor="#051f3a" />

          <Left style={{ alignSelf: 'flex-start', flex: 0.1 }} >
            <Button transparent onPress={() => navigation.goBack()} style={{ alignSelf: 'baseline', width: 44, height: 44 }} >
              <Image source={backWhite} style={{ width: 22, height: 22, tintColor: "white" }} />
            </Button>
          </Left>
          <View style={{ alignSelf: 'flex-start', flex: 1 }}>
            <Text style={styles.headerTitle} >{'Forgot Password'.toUpperCase()}</Text>
          </View>
          <Right style={{ alignSelf: 'flex-start', flex: 0.1 }}>
            {/* <Button transparent style={{ alignSelf: 'baseline', width: 44, height: 44 }} ><Image source={search} style={{ width: 22, height: 22, tintColor: 'black' }} /></Button> */}
          </Right>
        </Header>
      )
    }

  }

  componentWillMount() {

  }

  onResetpasswordClick = () => {

    if (this.validtion()) {
      this.wscall(this.state.email);
    }
  }

  validtion = () => {
    this.validate({
      email: { email: true },
    });
    if (this.isFieldInError('email')) {
      this.refs.toast.show(this.getErrorsInField('email'), 500, () => {
      });
      return false
    }
    return true
  }

  wscall = async (email) => {
    let httpMethod = 'POST'
    let strURL = userforgotpassword
    let headers = {
      'Content-Type': 'application/json',
      'device_token': deviceToken,
      'device_type': Platform.OS === 'ios' ? 1 : 2,
      'build_version': '1.0',
    }
    let params = {
      'email': email,
    }

    APICall.callWebService(httpMethod, strURL, headers, params, (resJSON) => {
      console.log('Login Screen -> ');
      console.log(resJSON)
      if (resJSON.status == 200) {

        Alert.alert(
          'Reset password',
          'Your password is reset please check email.',
          [
            {text: 'Ok', onPress: () => this.props.navigation.goBack()},
          ],
          { cancelable: false }
        )
        return
      } else {
        this.refs.toast.show(resJSON.message);
      }
    })
  }


  render() {

    return (
      <Container flex-direction={"row"} style={{ flex: 1 }} >
        <Toastt ref="toast"></Toastt>
        <ToastSucess ref="toastsucess"></ToastSucess>

        <BackgroundImage >
          <SafeAreaView />
          <Content style={{ paddingLeft: 33, paddingRight: 33 }}>
            <View style={{ alignItems: 'center', justifyContent: 'center', marginTop: 60, flex: 1 }}>
              <Image source={topIcon} style={{ width: 170, height: 100, resizeMode: 'cover' }} />
            </View>
            <View>
              <View style={[styles.textInputView, { marginTop: 57, flexDirection: 'row', alignItems: 'center' }]}>
                <Image source={email} style={{ height: 22, width: 22, marginLeft: 15 }} />
                <TextInput
                  ref="email"
                  onChangeText={(email) => this.setState({ email })} value={this.state.email}
                  style={[styles.textInput, { marginLeft: 10 }]}
                  placeholder="Email Address"
                  placeholderTextColor="#95959a"
                  keyboardType="email-address"
                  autoCorrect={false}
                />
              </View>
            </View>
            <View style={{ marginTop: 20 }}>

              <Button style={[styles.bottomViewButton, { backgroundColor: '#0F66C7', marginTop: 5, borderRadius: 0, }]}
                onPress={() => this.onResetpasswordClick()}
              >
                <Text style={{ color: 'white' }}>RESET PASSWORD</Text>
              </Button>
            </View>
          </Content>
        </BackgroundImage>
      </Container>
    );
  }
}

export default ForgotPassword;

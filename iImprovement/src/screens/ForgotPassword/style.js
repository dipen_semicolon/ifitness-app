const React = require("react-native");
const { Dimensions, Platform } = React;
const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;

export default {

  headerAndroidnav: {
   
    backgroundColor: '#2265B3',
    shadowOpacity:0,
    borderBottomWidth:0,
},
headerTitle: {
    fontFamily: font_SFProDisplay_Bold,
    marginTop: 15,
    color: 'white',
    textAlign: 'center'
},

//message listing
Title: {
    fontFamily: font_SFProDisplay_Bold,
    fontSize:13,
    alignSelf: 'flex-start',
    flex: 1,
    color: '#1A1918'
},
  welcome: {
    fontSize: 34.5,
    color: 'white',
    fontFamily: font_SFProDisplay_Bold,
    marginTop:83,
  },
  appName: {
    fontSize: 34.5,
    color: '#ff9b00',
    fontFamily: font_SFProDisplay_Bold,
  },
  bottomView: {
    paddingTop: 0,
    backgroundColor: 'transparent',
  },

  bottomViewButton: {
    height: 50,
    width: '100%',
    justifyContent: "center",
    alignSelf: "stretch",
    textAlignVertical: "center",
    fontFamily: font_SFProDisplay_Medium,
    fontSize: 20,
    },

    textInputView: {
      color: '#95959a',
      height:45,
      borderWidth:1,
      borderColor:'#95959a',
    },
  
    textInput: {
      color: 'white',
      fontSize: 16,
      fontFamily: font_SFProDisplay_Regular,
    },

};

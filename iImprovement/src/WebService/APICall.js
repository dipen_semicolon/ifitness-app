// import React, { Component } from 'react';
// import { TouchableOpacity, View, ActivityIndicator, Text, Alert } from 'react-native';
import RNProgressHUB from 'react-native-progresshub';

export default class APICall {

    static callWebService = (method, strURL, headers, params, callback = (response1) => { }) => {
        console.log(method)
        console.log(strURL)
        console.log(headers)
        console.log(JSON.stringify(params))
        RNProgressHUB.showSpinIndeterminate();
        fetch(strURL, {
            method: method,
            headers: headers,
            body: JSON.stringify(params)
        })
            .then((response) => response.json())
            .then((responseJson) => {
                RNProgressHUB.dismiss();
                if (responseJson.status == 200) {
                    callback(responseJson)
                }else if(responseJson.status == 401){
                    logoutForcefully()
                }else{
                    callback(responseJson)
                }
            })
            .catch((error) => {
                RNProgressHUB.dismiss();
                callback("Server Error.")
            });
    }

    static callWebServiceBackground = (method, strURL, headers, params, callback = (response1) => { }) => {
        console.log(method)
        console.log(strURL)
        console.log(headers)
        console.log(JSON.stringify(params))
        fetch(strURL, {
            method: method,
            headers: headers,
            body: JSON.stringify(params)
        })
            .then((response) => response.json())
            .then((responseJson) => {
                callback(responseJson)
            })
            .catch((error) => {
                callback("Server Error.")
            });
    }
    
    static uploadPhoto = (photoKeyword, photoPath, photoType, photoName, strURL, headers, params, callback = (response1) => { }) => {
        var formData = new FormData();
        const uriParts = photoPath.split('.');
        const fileType = uriParts[uriParts.length - 1];
        formData.append(photoKeyword, {
            uri: photoPath,
            name: `photo.${fileType}`,
            type: photoType
        });

        Object.entries(params).map(([key, value]) => {
            formData.append(key, value)
        })

        const config = {
            method: 'POST',
            headers: headers,
            body: formData
        };
        RNProgressHUB.showSpinIndeterminate();
        fetch(strURL, config)
            .then((response) => response.json())
            .then((responseJson) => {
                RNProgressHUB.dismiss();
                callback(responseJson)
            })
            .catch((error) => {
                RNProgressHUB.dismiss();
                console.error(error);
            });
    }
}